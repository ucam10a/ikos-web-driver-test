package tkss.report;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The custom annotation for Handsontable column
 * 
 * @author Yung Long Li
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
// on field level
public @interface HandsonInfo {

    /** column title */
    String title() default "";

    /** column width */
    int width() default 80;

    /** column type, ex. text, date, checkbox, numeric, html, dropdown */
    String type()  default "text";

    /** source values for dropdown */
    String[] source() default {}; 
    
}