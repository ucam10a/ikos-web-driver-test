package tkss.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * Simple i18N Message Utility <br>
 * properties name should be like 'application_[locale].properties' <br>
 * for example: application_en.properties, application_zh_tw.properties
 * 
 * @author Yung Long Li
 *
 */
public class MessageUtil {

    private static final String resource = "application";
    private static Map<String, String> msgMap = new HashMap<String, String>();
    
    private static Set<String> keys = new HashSet<String>();
    
    public static Locale en;
    public static Locale zh;
    
    static {
        try {
            ClassLoader loader = MessageUtil.class.getClassLoader();
            Properties prop = new Properties();
            prop.load(loader.getResourceAsStream(resource + ".properties"));
            en = new Locale("en");
            zh = new Locale("zh");
            for (Object key : prop.keySet()) {
                keys.add(key.toString());
                //System.out.println("key: " + key);
                getMessage(en, key.toString());
                getMessage(zh, key.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    
    public static Set<String> getKeys() {
        return keys;
    }
    
    /**
     * get i18N message
     * 
     * @param locale language locale
     * @param key message key
     * @return message
     */
    public static String getMessage(Locale locale, String key) {
        String mapKey = locale + "" + key;
        if (msgMap.get(mapKey) != null) {
            return msgMap.get(mapKey);
        }
        ResourceBundle messages = ResourceBundle.getBundle(resource, locale);
        if (messages == null) {
            throw new RuntimeException("ResourceBundle " + resource + ", " + locale + " not found!");
        }
        String msg = messages.getString(key);
        msgMap.put(mapKey, msg);
        return msg;
    }
    
    /**
     * get i18N map by locale
     * 
     * @param locale locale
     * @return i18N map
     */
    public static Map<String, String> geti18NMap(Locale locale) {
        Map<String, String> map = new HashMap<String, String>();
        for (String key : keys) {
            map.put(key, getMessage(locale, key));
        }
        return map;
    }
    
    /**
     * get en message
     * 
     * @param key message key
     * @return message
     */
    public static String getEnMessage(String key) {
        return getMessage(en, key);
    }
    
    public static String uppercaseKey(String key) {
        if (key == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        boolean upper = false;
        for (int i = 0; i < key.length(); i++) {
            String c = "" + key.charAt(i);
            if (".".equals(c)) {
                upper = true;
                continue;
            } else {
                if (upper) {
                    sb.append(c.toUpperCase());
                } else {
                    sb.append(c);
                }
                upper = false;
            }
        }
        return sb.toString();
    }
    
    /**
     * get zh message
     * 
     * @param key message key
     * @return message
     */
    public static String getZhMessage(String key) {
        return getMessage(zh, key);
    }
    
    /**
     * get i18N message by system[JVM server] locale
     * 
     * @param key message key
     * @return message
     */
    public static String getMessage(String key) {
        Locale currentLocale = new Locale(System.getProperty("user.language"), System.getProperty("user.country"));
        return getMessage(currentLocale, key);
    }
    
}