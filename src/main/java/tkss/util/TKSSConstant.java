package tkss.util;

/**
 * The class is used to define the constant value
 * 
 * @author Yung Long
 * 
 */
public class TKSSConstant {

    /** wiki constant value - 'wiki' */
    public static final String WIKI_TYPE = "wiki";

    /** event constant value - 'event' */
    public static final String EVENT_TYPE = "event";

    /** concept constant value - 'concept' */
    public static final String CONCEPT_MAP_TYPE = "concept";
    
    /** book constant value - 'book' */
    public static final String BOOK_TYPE = "book";

    /** keyword annotation prefix for non-english */
    public static final String LANGUAGE_PREFIX = "@lang:";

    /** title annotation prefix for non-english */
    public static final String LZSTRING_PREFIX = "@lz:";
    
    /** encoding */
    public static final String ENCODING = "UTF-8";

    /** the public level constant - 'public' means the public database */
    public static final String LEVEL_PUBLIC = "public";

    /** the local level constant - 'local' means the local/class database */
    public static final String LEVEL_LOCAL = "local";

    /** the teacher account type constant - 'teacher' */
    public static final String ACCOUNT_TEACHER = "teacher";

    /** the student account type constant - 'student' */
    public static final String ACCOUNT_STUDENT = "student";

    /** the administrator account type constant - 'admin' */
    public static final String ACCOUNT_ADMIN = "admin";

    /** the action constant of teacher/student to left message - 'leftMessage' */
    public static final String ACTION_LEFT_MESSAGE = "leftMessage";

    /**
     * the action constant of teacher/student to add someone into co-edit group
     * - 'addToGroup'
     */
    public static final String ACTION_ADD_TO_GROUP = "addToGroup";

    /**
     * the action constant of teacher to accept student to open his entry to
     * class - 'acceptOpen'
     */
    public static final String ACTION_ACCEPT_OPEN = "acceptOpen";

    /**
     * the action constant of teacher to decline student to open his entry to
     * class - 'declineOpen'
     */
    public static final String ACTION_DECLINE_OPEN = "declineOpen";

    /**
     * the action constant of teacher/student to approve other's update his
     * entry to class - 'acceptUpdate'
     */
    public static final String ACTION_ACCEPT_UPDATE = "acceptUpdate";

    /**
     * the action constant of teacher/student to decline other's update his
     * entry to class - 'declineUpdate'
     */
    public static final String ACTION_DECLINE_UPDATE = "declineUpdate";

    /**
     * the action constant of teacher/student to accept someone to join his
     * editing group - 'leftMessage'
     */
    public static final String ACTION_ACCEPT_JOIN = "acceptJoin";

    /**
     * the action constant of teacher/student to reject someone to join his
     * editing group - 'leftMessage'
     */
    public static final String ACTION_DECLINE_JOIN = "declineJoin";

    /**
     * the action constant of student to submit to open his entry to class -
     * 'applyOpen'
     */
    public static final String ACTION_APPLY_OPEN = "applyOpen";

    /**
     * the action constant of student to cancel to open his entry to class -
     * 'cancelOpen'
     */
    public static final String ACTION_CANCEL_APPLY_OPEN = "cancelOpen";

    /**
     * the action constant of teacher/student to request to join someone's
     * editing group - 'applyJoin'
     */
    public static final String ACTION_APPLY_JOIN = "applyJoin";

    /**
     * the action constant of teacher/student to rate the concept map entry -
     * 'rateConcept'
     */
    public static final String ACTION_RATE_CONCEPT = "rateConcept";

    /**
     * the action constant of teacher/student to rate the event entry -
     * 'rateEvent'
     */
    public static final String ACTION_RATE_EVENT = "rateEvent";

    /**
     * the action constant of teacher/student to rate the wiki entry -
     * 'rateWiki'
     */
    public static final String ACTION_RATE_WIKI = "rateWiki";
    
    /**
     * the action constant of teacher/student to rate the book entry -
     * 'rateBook'
     */
    public static final String ACTION_RATE_BOOK = "rateBook";

    /**
     * the action constant of teacher to add a public level entry to his class -
     * 'addToClass'
     */
    public static final String ACTION_ADD_TO_CLASS = "addToClass";

    /**
     * the action constant of teacher/student to create concept map entry -
     * 'createConcept'
     */
    public static final String ACTION_CREATE_CONCEPT = "createConcept";

    /**
     * the action constant of teacher/student to create book entry -
     * 'createBook'
     */
    public static final String ACTION_CREATE_BOOK = "createBook";
    
    /**
     * the action constant of teacher/student to delete book entry -
     * 'deleteBook'
     */
    public static final String ACTION_DELETE_BOOK = "deleteBook";
    
    /**
     * the action constant of teacher/student to delete concept map entry -
     * 'deleteConcept'
     */
    public static final String ACTION_DELETE_CONCEPT = "deleteConcept";

    /**
     * the action constant of teacher/student to create event entry -
     * 'createEvent'
     */
    public static final String ACTION_CREATE_EVENT = "createEvent";

    /**
     * the action constant of teacher/student to delete event entry -
     * 'deleteEvent'
     */
    public static final String ACTION_DELETE_EVENT = "deleteEvent";

    /**
     * the action constant of teacher/student to create wiki entry -
     * 'createWiki'
     */
    public static final String ACTION_CREATE_WIKI = "createWiki";

    /**
     * the action constant of teacher/student to delete wiki entry -
     * 'deleteWiki'
     */
    public static final String ACTION_DELETE_WIKI = "deleteWiki";

    /**
     * the action constant of teacher to change the status of the entry in his
     * class ex. open-to-class - 'switchPublicize'
     */
    public static final String ACTION_SWITCH_PUBLICIZE = "switchPublicize";

    /**
     * the action constant of teacher to submit the good quality entry to public
     * level database - 'applyToPublic'
     */
    public static final String ACTION_APPLY_TO_PUBLICDB = "applyToPublic";

    /**
     * the action constant of administrator to accept the good quality entry to
     * public level database - 'acceptPublic'
     */
    public static final String ACTION_ACCEPT_TO_PUBLIC_DB = "acceptPublic";

    /**
     * the action constant of book author to create new book chapter
     */
    public static final String ACTION_CREATE_BOOK_CHAPTER = "createChapter";
    
    /**
     * the action constant of book author to delete book chapter
     */
    public static final String ACTION_DELETE_BOOK_CHAPTER = "deleteChapter";
    
    /**
     * the action constant of book author to swap book chapter
     */
    public static final String ACTION_SWAP_BOOK_CHAPTER = "swapChapter";
    
    /**
     * the action constant of administrator to decline the good quality entry to
     * public level database - 'declinePublic'
     */
    public static final String ACTION_DECLINE_TO_PUBLIC_DB = "declinePublic";

    /** the multiple problem in quiz */
    public static final String QUIZ_PROBLEM_MULTIPLE = "multiple";

    /** the multiple-pair problem in quiz */
    public static final String QUIZ_PROBLEM_MULTIPLEPAIR = "multiple-pair";

    /** the event problem in quiz */
    public static final String QUIZ_PROBLEM_EVENT = "event";

    /** the concept map problem in quiz */
    public static final String QUIZ_PROBLEM_CONCEPT = "concept";

    /** the limit that direct and indirect relevant link in web page */
    public static final int LINK_LIMIT = 10;

    /** action message type id: team id */
    public static final int ACTION_MSG_TEAMID = 1;

    /** action message type id: entry id */
    public static final int ACTION_MSG_ENTRY_ID = 2;

    /** action message type id: class entry */
    public static final int ACTION_MSG_ENTRY = 3;
    
    /** date format */
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /** IF class field */
    public static final String IF_FIELD = "IF";
    
    /** announcement fields */
    public static final String[] ANNOUNCEMENT_FIELDS = {IF_FIELD};
    
    /** announcement title */
    public static final String ANNOUNCEMENT = "announcement";

    /** forever license */
    public static final String FOREVER_LICENSE = "forever";
    
    /** demo account allow register students */
    public static final int DEMO_ACCOUNT_REGISTER_SIZE = 5;
    
    /** class status open */
    public static final String CLASS_OPEN = "open";

    /** class status close */
    public static final String CLASS_CLOSE = "close";

    /** login error message */
    public static final String MSG_LOGIN_ERROR = "msg.login.error";

    /** successfully added message */
    public static final String MSG_SUCCESSFULLY_ADDED = "msg.successfully.added";

    /** successfully added message */
    public static final String MSG_SUCCESSFULLY_SUBMITTED = "msg.successfully.submitted";

    /** successfully added message */
    public static final String MSG_SUCCESSFULLY_PUBLIC_ADDED = "msg.successfully.public.added";

    /** successfully delete message */
    public static final String MSG_SUCCESSFULLY_DELETED = "msg.successfully.deleted";

    /** add to public application already exist message */
    public static final String MSG_ADD_PUBLIC_EXIST = "msg.public.add.exist";

    /** application not exist message */
    public static final String MSG_APPLICATION_NOT_EXIST = "msg.application.not.exist";

    /** input username message */
    public static final String MSG_INPUT_USERNAME = "msg.input.username";

    /** duplicate username message */
    public static final String MSG_DUPLICATE_USERNAME = "msg.username.exist";
    
    /** duplicate username message */
    public static final String MSG_ACCOUNT_CREATED = "msg.account.created";

    /** password not match username message */
    public static final String MSG_PASSWORD_NOT_MATCH = "msg.password.not.match";

    /** password not match username message */
    public static final String MSG_PASSWORD_EMPTY = "msg.password.empty";

    /** password not match username message */
    public static final String MSG_PASSWORD_TOOSHORT = "msg.password.too.short";

    /** email format error message */
    public static final String MSG_EMAIL_FORMAT_ERROR = "msg.email.format.error";

    /** first name error message */
    public static final String MSG_FIRST_NAME_ERROR = "msg.input.firstname";

    /** last name error message */
    public static final String MSG_LAST_NAME_ERROR = "msg.input.lastname";

    /** set class message */
    public static final String MSG_SET_CALSS = "msg.set.class.first";

    /** no record message */
    public static final String MSG_NO_RECORD = "msg.no.match.word";

    /** no such account message */
    public static final String MSG_NO_ACCOUNT = "msg.no.such.account";

    /** user name and password error message */
    public static final String MSG_PASSWORD_ERROR = "msg.password.not.match";

    /** system error message */
    public static final String MSG_SYSTEM_ERROR = "msg.system.db.error";

    /** not allow to modify message */
    public static final String MSG_NOT_ALLOW_VIEW = "msg.not.allow.view";

    /** not allow to modify message */
    public static final String MSG_NOT_ALLOW_MODIFY = "msg.not.allow.modify";

    /** keyword too long */
    public static final String MSG_KEYWORD_TOOLONG = "msg.keyword.too.long";

    /** title empty */
    public static final String MSG_TITLE_EMPTY = "msg.title.empty";

    /** no current class */
    public static final String MSG_NO_CURRENTCLASS = "msg.set.class.first";

    /** title duplicate */
    public static final String MSG_TITLE_DUPLICATE = "msg.title.duplicate";

    /** user is editing */
    public static final String MSG_USER_EDITING = "msg.user.editting";

    /** has submitted to public database */
    public static final String MSG_PUBLIC_SUBMITTED = "msg.public.submitted";

    /** not exist */
    public static final String MSG_NOT_EXIST = "msg.entry.not.exist";
    
    /** decision made */
    public static final String MSG_DECISION_MADE = "msg.decision.made";

    /** application made */
    public static final String MSG_APPLICATION_MADE = "msg.application.made";

    /** rating made */
    public static final String MSG_RATING_MADE = "msg.ratting.made";

    /** not team member */
    public static final String MSG_NOT_TEAM_MEMBER = "msg.not.team.member";

    /** update not approve */
    public static final String MSG_UPDATE_NOT_APPROVE = "msg.update.not.approve";

    /** new update waiting */
    public static final String MSG_NEW_UPDATE_WAITING = "msg.new.update.waiting";

    /** new member wating to jon */
    public static final String MSG_NEW_MEMBER_WAITING = "msg.new.member.waiting";

    /** update not approve */
    public static final String MSG_OPEN_WAITING = "msg.open.waiting";

    /** database connection */
    public static final String MSG_CONNECTION_ERROR = "msg.connection.error";

    /** not an administrator */
    public static final String MSG_NOT_ADMIN = "msg.not.admin";

    /** not a teacher */
    public static final String MSG_NOT_TEACHER = "msg.not.teacher";

    /** not allow to add to class */
    public static final String MSG_NOT_ALLOW_TO_ADD_TO_CLASS = "msg.not.allow.to.add.class";

    /** not allow to submit to public database */
    public static final String MSG_NOT_ALLOW_TO_SUBMIT_TO_PUBLIC_DATABASE = "msg.not.allow.submit.public.db";

    /** not allow to select this class */
    public static final String MSG_NOT_ALLOW_SELECT_CLASS = "msg.not.allow.select.class";

    /** not allow to select this class */
    public static final String MSG_NOT_ALLOW_UPDATE_ENTRY = "msg.allow.update.entry";

    /** not allow to select this class */
    public static final String MSG_NOT_CLASS_MEMBER = "msg.not.class.member";
        
    /** not an administrator */
    public static final String MSG_NOT_ADMIN_OR_TEACHER = "msg.not.admin.or.teacher";
    
    /** demo account size is over */
    public static final String DEMO_ACCOUNT_OVERSIZE_MSG = "msg.demo.account.oversize";

    /** license expired message */
    public static final String LICENSE_EXPIRE_MSG = "msg.class.license.expired";
    
    /** license hint */
    public static final String LICENSE_HINT = "msg.license.hint";

    /** license expired and not allow to create entry message */
    public static final String LIMIT_CREAT_ENTRY = "msg.limit.create.entry";

    public static final String LICENSE_OVERSIZE_MSG = "msg.license.oversize";

    public static final String MSG_LEFT_MSG_OVERSIZE = "msg.left.msg.oversize";

    public static final String MSG_OPEN_TO_CLASS = "msg.open.to.class";

    public static final String MSG_EDIT_GROUP_UPDATE = "msg.edit.group.update";

    public static final String MSG_OPEN_TO_CLASS_CANCEL = "msg.open.to.class.cancel";

    public static final String MSG_UPDATE_APPROVED = "msg.update.approved";

    public static final String MSG_UPDATE_CANCEL = "msg.update.cancel";

    public static final String MSG_ACCEPT_CO_EDIT = "msg.accept.co.edit";

    public static final String MSG_DECLINE_CO_EDIT = "msg.decline.co.edit";

    public static final String MSG_APPLY_CO_EDIT = "msg.apply.co.edit";

    public static final String MSG_APPLY_OPEN_TO_CLASS = "msg.apply.open.to.class";

}