package tkss.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * type converter to cast or construct to another object
 * 
 * @author Yung Long Li
 *
 */
public class TypeConverter {

    private static Logger logger = Logger.getLogger(TypeConverter.class.getName());

    /**
     * This method will check then cast source object to target object Note:
     * object hash code does not change, so it is still the same object
     * 
     * @param from
     *            source object
     * @param target
     *            target class
     * @return target object
     */
    public static <T> T convert(Object source, Class<T> target) {
        if (target.isAssignableFrom(source.getClass())) {
            return target.cast(source);
        } else {
            throw new RuntimeException("type is incompatible!");
        }
    }

    /**
     * This method will create a new target object then copy all field from
     * source to target if they are the same Note: object hash code changes, so
     * it is a new object
     * 
     * @param source
     *            source object
     * @param target
     *            target class
     * @return new target object
     */
    public static <T> T construct(Object source, Class<T> target) {

        T result = null;
        if (source == null) return null;
        try {
            result = target.newInstance();
            List<String> clsTFieldNames = new ArrayList<String>();
            Class<?> cls = target;
            // print field
            while (cls != null) {
                Field[] fields = cls.getDeclaredFields();
                for (Field f : fields) {
                    clsTFieldNames.add(f.getName());
                }
                cls = cls.getSuperclass();
            }
            for (String name : clsTFieldNames) {
                Object value = runGetter(source, name);
                if (value != null) invokeSetter(result, name, value);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.toString(), e);
        }
        return result;

    }
    
    /**
     * This method will create a new target object then copy all field from
     * map to target if they are the same Note: object hash code changes, so
     * it is a new object
     * 
     * @param map
     *            source map object
     * @param target
     *            target class
     * @return new target object
     */
    public static <T> T construct(Map<String, Object> map, Class<T> target) {

        T result = null;
        if (map == null) return null;
        try {
            result = target.newInstance();
            List<String> clsTFieldNames = new ArrayList<String>();
            Class<?> cls = target;
            // print field
            while (cls != null) {
                Field[] fields = cls.getDeclaredFields();
                for (Field f : fields) {
                    clsTFieldNames.add(f.getName());
                }
                cls = cls.getSuperclass();
            }
            for (String name : clsTFieldNames) {
                Object value = map.get(name);
                if (value != null) invokeSetter(result, name, value);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.toString(), e);
        }
        return result;

    }

    /**
     * 
     * @param source
     * @param sourceType
     * @param targetType
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T, S> Set<T> convertSet(Set<S> source, Class<S> sourceType, Class<T> targetType) {
        if (targetType.isAssignableFrom(sourceType)) {
            return (Set<T>) source;
        }
        throw new RuntimeException("type is incompatible!");
    }

    /**
     * 
     * @param source
     * @param sourceType
     * @param targetType
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T, S> List<T> convertList(List<S> source, Class<S> sourceType, Class<T> targetType) {
        if (targetType.isAssignableFrom(sourceType)) {
            return (List<T>) source;
        }
        throw new RuntimeException("type is incompatible!");
    }

    /**
     * 
     * @param obj
     * @param methodName
     * @return
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    private static Object runGetter(Object obj, String methodName) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        for (Method method : obj.getClass().getMethods()) {
            if ((method.getName().startsWith("get")) && (method.getName().length() == (methodName.length() + 3))) {
                if (method.getName().toLowerCase().endsWith(methodName.toLowerCase())) {
                    return method.invoke(obj);
                }
            }
        }
        return null;
    }

    /**
     * 
     * @param obj
     * @param fieldName
     * @param value
     * @throws Exception
     */
    private static void invokeSetter(Object obj, String fieldName, Object value) throws Exception {
        String methodName = "set" + fieldName;
        for (Method method : obj.getClass().getMethods()) {
            if ((method.getName().toLowerCase().equals(methodName.toLowerCase()) && method.getParameterTypes().length == 1)) {
                Class<?> type = method.getParameterTypes()[0];
                if (type.getName().equalsIgnoreCase(value.getClass().getName())) {
                    method.invoke(obj, value);
                }
                break;
            }
        }
    }

    /**
     * test method
     * 
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {


    }

}