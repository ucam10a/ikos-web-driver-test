package tkss.util.orm;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;

import tkss.util.orm.dao.CommentOBJDAO;

/**
 * The class is used to map the message table in database
 * 
 * @author Yung Long
 * 
 */
@Entity(name="comments")
@EntityListeners(value={CommentOBJDAO.class})
public class CommentOBJ extends BasicEntity {

    /** entry id */
    @Column(name="id")
    @Id
    protected Integer EntryID;

    /** entry type */
    @Column(name="type")
    @Id
    protected String type;

    @Column(name="count_num")
    @Id
    private Integer countNum;
    
    /** message text */
    @Column(name="comments")
    protected String comments;

    /** encode message */
    protected String encodeComment;

    /**
     * the number message of this entry, one entry can only has at most ten
     * message
     */
    protected Integer count;

    /**
     * create time
     */
    @Column(name="createDate")
    protected Timestamp createTsp;
    
    /** the time which message left */
    protected String dateTime;

    /** the username who sent this message */
    @Column(name="from_id")
    protected String from;

    /** the sender's first name */
    protected String firstName;

    /** the sender's last name */
    protected String lastName;

    /** author user id */
    protected String author;

    /** allow to remove */
    protected boolean remove = false;

    /**
     * set the entry type
     * 
     * @param type
     *            entry type
     * 
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * get the entry type
     * 
     * @return the entry type
     */
    public String getType() {
        return type;
    }

    /**
     * set the message text
     * 
     * @param message
     *            message text
     * 
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * get the message text
     * 
     * @return the message text
     */
    public String getComments() {
        return comments;
    }

    /**
     * set the number message of this entry
     * 
     * @param count
     *            number message of this entry
     * 
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * get the number message of this entry
     * 
     * @return the number message of this entry
     */
    public Integer getCount() {
        return count;
    }

    /**
     * set the username who sent the message
     * 
     * @param from
     *            username
     * 
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * get the username who sent the message
     * 
     * @return username
     */
    public String getFrom() {
        return from;
    }

    /**
     * set the current time of sending message
     * 
     * @param dateTime
     *            timestamp of current time
     * 
     */
    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * get the time stamp of current time
     * 
     * @return time stamp of current time
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     * set sender's first name
     * 
     * @param firstName
     *            sender's first name
     * 
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * get sender's first name
     * 
     * @return sender's first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * set sender's last name
     * 
     * @param lastName
     *            sender's last name
     * 
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * get sender's last name
     * 
     * @return sender's last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * set entry id of this message belong
     * 
     * @param entryID
     *            entry id of this message belong
     * 
     */
    public void setEntryID(Integer entryID) {
        EntryID = entryID;
    }

    /**
     * get entry id of this message belong
     * 
     * @return entry id
     */
    public Integer getEntryID() {
        return EntryID;
    }

    /**
     * get author user id
     * 
     * @return author user id
     */
    public String getAuthor() {
        return author;
    }

    /**
     * set author user id
     * 
     * @param author
     *            author user id
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * allow to remove
     * 
     * @return true if it can be remove
     */
    public boolean isRemove() {
        return remove;
    }

    /**
     * set true if allow to remove
     * 
     * @param remove
     *            true if allow to remove
     */
    public void setRemove(boolean remove) {
        this.remove = remove;
    }

    /**
     * get encode comment
     * 
     * @return encode comment
     */
    public String getEncodeComment() {
        return encodeComment;
    }

    /**
     * set encode comment
     * 
     * @param encodeComment
     *            encode comment
     */
    public void setEncodeComment(String encodeComment) {
        this.encodeComment = encodeComment;
    }

    /**
     * set count
     * 
     * @param count
     *            count
     */
    public Integer getCountNum() {
        return countNum;
    }

    /**
     * get count
     * 
     * @param count
     * 
     */
    public void setCountNum(Integer countNum) {
        this.countNum = countNum;
    }

    public Timestamp getCreateTsp() {
        return createTsp;
    }

    public void setCreateTsp(Timestamp createTsp) {
        this.createTsp = createTsp;
    }

}