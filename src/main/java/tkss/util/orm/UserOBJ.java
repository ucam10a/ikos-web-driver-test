package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;

import tkss.util.orm.dao.UserOBJDAO;

/**
 * The class is used to map the user table in database
 * 
 * @author Yung Long
 * 
 */
@Entity(name="users")
@EntityListeners(value={UserOBJDAO.class})
public class UserOBJ extends BasicEntity {

    /** username */
    @Column(name="userID")
    @Id
    protected String userID;

    @Column(name="password")
    protected String password;
    
    /** user account content */
    @Column(name="accountType")
    protected String accountType;

    /** user's email */
    @Column(name="email")
    protected String email;

    /** user first name */
    @Column(name="firstName")
    protected String firstName;

    /** user last name */
    @Column(name="lastName")
    protected String lastName;

    /** user icon image path */
    @Column(name="icon")
    private String icon = "users/icon.gif";
    
    @Column(name="currentClass")
    private Integer currentClass = -1;

    /**
     * set user first name
     * 
     * @param firstName
     *            user first name
     * 
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * get user first name
     * 
     * @return user first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * set user last name
     * 
     * @param lastName
     *            user last name
     * 
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * get the user last name
     * 
     * @return user last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * set the user id
     * 
     * @param userID
     *            username
     * 
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

    /**
     * get user id
     * 
     * @return user id
     */
    public String getUserID() {
        return userID;
    }

    /**
     * set the account type of user
     * 
     * @param accountType
     *            account type ex.(teacher, student, admin)
     * 
     */
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    /**
     * get user account type
     * 
     * @return user account type
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * set user email
     * 
     * @param email
     *            user email
     * 
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * get user email
     * 
     * @return user email
     */
    public String getEmail() {
        return email;
    }

    /**
     * get user icon
     * 
     * @return user icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * set user icon
     * 
     * @param icon
     *            user icon
     * 
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * get current class id
     * 
     * @return current class id
     */
    public Integer getCurrentClass() {
        return currentClass;
    }

    /**
     * set current class id
     * 
     * @param currentClass
     *                   current class id
     */
    public void setCurrentClass(Integer currentClass) {
        this.currentClass = currentClass;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}