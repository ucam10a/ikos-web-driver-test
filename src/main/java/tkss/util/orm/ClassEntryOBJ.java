package tkss.util.orm;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.Column;
//import javax.persistence.FetchType;
import javax.persistence.Id;
//import javax.persistence.JoinColumns;
//import javax.persistence.JoinColumn;
//import javax.persistence.OneToOne;

import tkss.util.TKSSConstant;

/**
 * The class entry object which is used to store the class level entry ex.
 * event, wiki or concept map
 * 
 * @author Yung Long
 * 
 */
public class ClassEntryOBJ extends EntryOBJ {

    /** entry id */
    @Column(name="id")
    protected Integer entryID = -1;

    /** the entry in class is open or not */
    @Column(name="publicize")
    @Id
    protected String publicize;

    /** the entry team leader, it usually means the creator of this entry */
    @Column(name="leader")
    protected String leader;

    /** the class which the entry locates */
    @Column(name="classID")
    @Id
    protected Integer classID;
    
    //@OneToOne(fetch=FetchType.LAZY)
    //@JoinColumns(
    //    @JoinColumn(table="class", name="classID", referencedColumnName="classID")
    //)
    private ClassOBJ classObj;

    /** team id of the entry */
    @Column(name="team")
    protected String team;
    
    /** mark flag for teacher */
    @Column(name="flag")
    protected String flag;
    
    /** entry update user */
    @Column(name="updater")
    protected String updater;

    /** the first name of entry team leader */
    protected String firstNameOfLeader;

    /** the last name of entry team leader */
    protected String lastNameOfLeader;

    /** the list of all team member, it separates by comma */
    protected String teamMember;

    /** the list of all team member */
    protected List<String> teamMemberNames;

    /** check if the client is one of the editor in this edit-group */
    protected boolean inGroup;

    /** team member number */
    protected int teamNum;
    
    /** rate score for this entry */
    protected int[] ratingArray;

    /** entry status */
    protected StatusOBJ status = new StatusOBJ();

    /** the create time string */
    protected String createdate;

    /** the create time stamp */
    @Column(name="createdate")
    protected Timestamp createTsp;

    /** the modify time string */
    protected String modifydate;

    /** the modify time stamp */
    @Column(name="modifydate")
    protected Timestamp modifyTsp;
    
    /** has flag */
    protected boolean hasFlag = false;
    
    /**
     * set the entry publicize status in class, ex. yes or no
     * 
     * @param publicize
     *            entry publicize status in class, ex. yes or no
     */
    public void setPublicize(String publicize) {
        this.publicize = publicize;
    }

    /**
     * get the entry publicize status in class, ex. yes or no
     * 
     * @return the entry publicize status ex. yes or no
     */
    public String getPublicize() {
        return publicize;
    }

    /**
     * set the entry leader username
     * 
     * @param leader
     *            entry leader username
     */
    public void setLeader(String leader) {
        this.leader = leader;
    }

    /**
     * get the entry leader username
     * 
     * @return the entry leader username
     */
    public String getLeader() {
        return leader;
    }

    /**
     * set the class id where this entry locates
     * 
     * @param classID
     *            class id where this entry locates
     */
    public void setClassID(Integer classID) {
        this.classID = classID;
        this.entryLevel = TKSSConstant.LEVEL_LOCAL;
    }

    /**
     * get the class id where this entry locates
     * 
     * @return class id
     */
    public Integer getClassID() {
        return classID;
    }

    /**
     * set the entry the team id
     * 
     * @param team
     *            the team id
     */
    public void setTeam(String team) {
        this.team = team;
    }

    /**
     * get the team id
     * 
     * @return team id
     */
    public String getTeam() {
        return team;
    }

    /**
     * set the first name of the leader
     * 
     * @param firstNameOfLeader
     *            the first name of the leader
     */
    public void setFirstNameOfLeader(String firstNameOfLeader) {
        this.firstNameOfLeader = firstNameOfLeader;
    }

    /**
     * get the first name of the leader
     * 
     * @return the first name of the leader
     */
    public String getFirstNameOfLeader() {
        return firstNameOfLeader;
    }

    /**
     * set the last name of the leader
     * 
     * @param lastNameOfLeader
     *            the last name of the leader
     */
    public void setLastNameOfLeader(String lastNameOfLeader) {
        this.lastNameOfLeader = lastNameOfLeader;
    }

    /**
     * get the last name of the leader
     * 
     * @return the last name of the leader
     */
    public String getLastNameOfLeader() {
        return lastNameOfLeader;
    }

    /**
     * set all team member's name
     * 
     * @param teamMember
     *            all team member's name
     */
    public void setTeamMember(String teamMember) {
        this.teamMember = teamMember;
    }

    /**
     * get all team member's name
     * 
     * @return all team member's name
     */
    public String getTeamMember() {
        return teamMember;
    }

    /**
     * set true if the client is in edit-group
     * 
     * @param inGroup
     *            true if the client is in edit-group
     */
    public void setInGroup(boolean inGroup) {
        this.inGroup = inGroup;
    }

    /**
     * check if the client is in edit-group
     * 
     * @return true if client is in edit-group
     */
    public boolean isInGroup() {
        return inGroup;
    }

    /**
     * set number of team member
     * 
     * @param teamNum
     *            number of team member
     */
    public void setTeamNum(int teamNum) {
        this.teamNum = teamNum;
    }

    /**
     * get number of team member
     * 
     * @return number of team member
     */
    public int getTeamNum() {
        return teamNum;
    }

    /**
     * get entry status
     * 
     * @return entry status
     */
    public StatusOBJ getStatus() {
        return status;
    }

    /**
     * set entry status
     * 
     * @param status
     *            entry status
     */
    public void setStatus(StatusOBJ status) {
        this.status = status;
    }

    /**
     * get the common title for convenient call
     * 
     * @return entry title
     */
    public String getCreatedate() {
        return createdate;
    }

    /**
     * set create time stamp
     * 
     * @param createdate
     *            time stamp
     */
    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    /**
     * set modify time stamp
     * 
     * @return modify time stamp
     */
    public String getModifydate() {
        return modifydate;
    }

    /**
     * set modify time stamp
     * 
     * @param modifydate
     *            time stamp
     */
    public void setModifydate(String modifydate) {
        this.modifydate = modifydate;
    }

    /**
     * get create time stamp
     * 
     * @return create time stamp
     */
    public Timestamp getCreateTsp() {
        return createTsp;
    }

    /**
     * set create time stamp
     * 
     * @param createTsp
     *            create time stamp
     */
    public void setCreateTsp(Timestamp createTsp) {
        this.createTsp = createTsp;
        SimpleDateFormat sdf = new SimpleDateFormat(TKSSConstant.DATE_FORMAT);
        this.createdate = sdf.format(createTsp);
    }

    /**
     * get modify time stamp
     * 
     * @return modify time stamp
     */
    public Timestamp getModifyTsp() {
        return modifyTsp;
    }

    /**
     * set modify time stamp
     * 
     * @param modifyTsp
     *            modify time stamp
     */
    public void setModifyTsp(Timestamp modifyTsp) {
        this.modifyTsp = modifyTsp;
        SimpleDateFormat sdf = new SimpleDateFormat(TKSSConstant.DATE_FORMAT);
        this.modifydate = sdf.format(modifyTsp);
    }

    /**
     * set entry id
     * 
     * @param entryID
     *            entry id
     * 
     */
    public void setEntryID(Integer entryID) {
        this.entryID = entryID;
    }

    /**
     * get entry id
     * 
     * @return entry id
     */
    public Integer getEntryID() {
        return entryID;
    }

    /**
     * get team member names
     * 
     * @return team member names
     */
    public List<String> getTeamMemberNames() {
        return teamMemberNames;
    }

    /**
     * set team member names
     * 
     * @param teamMemberNames
     *            team member names
     */
    public void setTeamMemberNames(List<String> teamMemberNames) {
        this.teamMemberNames = teamMemberNames;
    }

    public ClassOBJ getClassObj() {
        return classObj;
    }

    public void setClassObj(ClassOBJ classObj) {
        this.classObj = classObj;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        if (flag != null && !flag.trim().equals("")) {
            this.hasFlag = true;
        }
        this.flag = flag;
    }

    public String getUpdater() {
        return updater;
    }

    public void setUpdater(String updater) {
        this.updater = updater;
    }

    public boolean getHasFlag() {
        return hasFlag;
    }

    public void setHasFlag(boolean hasFlag) {
        this.hasFlag = hasFlag;
    }

    public int[] getRatingArray() {
        return ratingArray;
    }

    public void setRatingArray(int[] ratingArray) {
        this.ratingArray = ratingArray;
    }

}