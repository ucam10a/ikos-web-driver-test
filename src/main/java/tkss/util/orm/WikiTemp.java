package tkss.util.orm;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import tkss.util.orm.dao.WikiTempDAO;

@Entity(name="wikitemp")
@EntityListeners(value={WikiTempDAO.class})
public class WikiTemp extends TempOBJ {

}