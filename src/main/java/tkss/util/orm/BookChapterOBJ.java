package tkss.util.orm;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import tkss.util.orm.dao.BookChapterOBJDAO;

import com.yung.jpa.BasicEntity;

@Entity(name="bookchapter")
@EntityListeners(value={BookChapterOBJDAO.class})
public class BookChapterOBJ extends BasicEntity {
    
    @Column(name="id")
    private Integer chapterID;
    
    @Column(name="bookId")
    @Id
    private Integer bookID;
    
    @Column(name="type")
    private String type;
    
    @Column(name="entryId")
    private Integer entryID;
    
    @Column(name="historydate")
    private Timestamp wikiHistory;
    
    @Column(name="createdate")
    private Timestamp createDate;
    
    @Column(name="modifydate")
    private Timestamp modifyDate;
    
    @Column(name="sequenceOrder")
    @Id
    private Integer sequenceOrder = -1;
    
    private String title;
    
    private String position = "";
    
    public Integer getChapterID() {
        return chapterID;
    }

    public void setChapterID(Integer chapterID) {
        this.chapterID = chapterID;
    }

    public Integer getBookID() {
        return bookID;
    }

    public void setBookID(Integer bookID) {
        this.bookID = bookID;
    }

    public Timestamp getWikiHistory() {
        return wikiHistory;
    }

    public void setWikiHistory(Timestamp wikiHistory) {
        this.wikiHistory = wikiHistory;
    }

    public Integer getSequenceOrder() {
        return sequenceOrder;
    }

    public void setSequenceOrder(Integer sequenceOrder) {
        this.sequenceOrder = sequenceOrder;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Timestamp modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getEntryID() {
        return entryID;
    }

    public void setEntryID(Integer entryID) {
        this.entryID = entryID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    } 
    
}
