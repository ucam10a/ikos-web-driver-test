package tkss.util.orm;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;

public class LockOBJ extends BasicEntity {

    @Column(name="id")
    @Id
    private Integer id;
    
    @Column(name="timestamp")
    @Id
    private Timestamp datetime;
    
    @Column(name="userID")
    private String userID;

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getDatetime() {
        return datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
    
}