package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import tkss.util.orm.dao.PublicEventKeywordDAO;

@Entity(name="eventkeyword")
@EntityListeners(value={PublicEventKeywordDAO.class})
public class PublicEventKeyword extends PublicEntryKeyword{

    @Column(name="eventWord")
    @Id
    private String eventWord;

    public String getEventWord() {
        return eventWord;
    }

    public void setEventWord(String eventWord) {
        this.eventWord = eventWord;
    }
    
}