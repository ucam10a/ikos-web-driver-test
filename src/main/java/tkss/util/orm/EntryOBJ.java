package tkss.util.orm;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;

import com.yung.jpa.BasicEntity;

import tkss.util.TKSSConstant;
import tkss.util.TKSSUtil;

/**
 * The abstract class is the basic entry class for event, wiki and concept map.
 * It includes all common values which will use in ikos system
 * 
 * @author Yung Long
 * 
 */
public abstract class EntryOBJ extends BasicEntity {

    public static int KEYWORD_SIZE = 140;

    /** entry title */
    protected String entryTitle;

    /** encoded entry title */
    protected String encodeTitle;

    /** entry type */
    protected String entryClass;

    /** entry level */
    protected String entryLevel;

    /** the entry keyword */
    @Column(name="keyword")
    protected String keyword;

    /** entry keyword list */
    protected List<String> keywordNodes;

    /** encode node if it is not english */
    protected List<String> encodeKeywordNodes;

    /** the list of direct relevant event */
    protected List<LinkOBJ> eventDirectLinks;

    /**
     * the list of direct relevant event title, print as string and separates
     * with comma
     */
    protected String eventDirectTiles;

    /** the list of indirect relevant event */
    protected List<LinkOBJ> eventInDirectLinks;

    /**
     * the list of indirect relevant event title, print as string and separates
     * with comma
     */
    protected String eventInDirectTiles;

    /** the list of direct relevant wiki */
    protected List<LinkOBJ> wikiDirectLinks;

    /**
     * the list of direct relevant wiki title, print as string and separates
     * with comma
     */
    protected String wikiDirectTitles;

    /** the list of indirect relevant event */
    protected List<LinkOBJ> wikiInDirectLinks;

    /**
     * the list of indirect relevant wiki title, print as string and separates
     * with comma
     */
    protected String wikiInDirectTitles;

    /** the list of direct relevant concept map */
    protected List<LinkOBJ> conceptDirectLinks;

    /**
     * the list of direct relevant concept map title, print as string and
     * separates with comma
     */
    protected String conceptDirectTitles;

    /** the list of indirect relevant event */
    protected List<LinkOBJ> conceptInDirectLinks;

    /**
     * the list of indirect relevant concept title, print as string and
     * separates with comma
     */
    protected String conceptInDirectTitles;

    /**
     * the list of RespondTagOBJ which is used to pass the alert message. ex.
     * someone want to join edit-group
     */
    protected List<RespondTagOBJ> responds;

    /**
     * set the entry keyword
     * 
     * @param keyword
     *            the entry keyword
     * @throws UnsupportedEncodingException
     */
    public void setKeyword(String keyword) {
        if (keyword == null) keyword = ""; 
        this.keyword = keyword;
        try {
            setKeywordNodes(keyword);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * get the entry keyword
     * 
     * @return entry keyword
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * set list link of other direct relative event entry
     * 
     * @param eventDirectLinks
     *            list link of other direct relative event entry
     */
    public void setEventDirectLinks(List<LinkOBJ> eventDirectLinks) {
        this.eventDirectLinks = eventDirectLinks;
        setEventDirectTiles(TKSSUtil.LinkToString(eventDirectLinks));
    }

    /**
     * get list link of other direct relative event entry
     * 
     * @return list link of other direct relative event entry
     */
    public List<LinkOBJ> getEventDirectLinks() {
        return eventDirectLinks;
    }

    /**
     * set list link of other indirect relative event entry
     * 
     * @param eventInDirectLinks
     *            list link of other indirect relative event entry
     */
    public void setEventInDirectLinks(List<LinkOBJ> eventInDirectLinks) {
        this.eventInDirectLinks = eventInDirectLinks;
        setEventInDirectTiles(TKSSUtil.LinkToString(eventInDirectLinks));
    }

    /**
     * get list link of other indirect relative event entry
     * 
     * @return list link of other indirect relative event entry
     */
    public List<LinkOBJ> getEventInDirectLinks() {
        return eventInDirectLinks;
    }

    /**
     * set list link of other direct relative wiki entry
     * 
     * @param wikiDirectLinks
     *            list link of other direct relative wiki entry
     */
    public void setWikiDirectLinks(List<LinkOBJ> wikiDirectLinks) {
        this.wikiDirectLinks = wikiDirectLinks;
        setWikiDirectTitles(TKSSUtil.LinkToString(wikiDirectLinks));
    }

    /**
     * get list link of other direct relative wiki entry
     * 
     * @return list link of other direct relative wiki entry
     */
    public List<LinkOBJ> getWikiDirectLinks() {
        return wikiDirectLinks;
    }

    /**
     * set list link of other indirect relative wiki entry
     * 
     * @param wikiInDirectLinks
     *            list link of other indirect relative wiki entry
     */
    public void setWikiInDirectLinks(List<LinkOBJ> wikiInDirectLinks) {
        this.wikiInDirectLinks = wikiInDirectLinks;
        setWikiInDirectTitles(TKSSUtil.LinkToString(wikiInDirectLinks));
    }

    /**
     * get list link of other indirect relative wiki entry
     * 
     * @return list link of other indirect relative wiki entry
     */
    public List<LinkOBJ> getWikiInDirectLinks() {
        return wikiInDirectLinks;
    }

    /**
     * set list link of other direct relative concept map entry
     * 
     * @param conceptDirectLinks
     *            list link of other direct relative concept map entry
     */
    public void setConceptDirectLinks(List<LinkOBJ> conceptDirectLinks) {
        this.conceptDirectLinks = conceptDirectLinks;
        setConceptDirectTitles(TKSSUtil.LinkToString(conceptDirectLinks));
    }

    /**
     * get list link of other direct relative concept map entry
     * 
     * @return list link of other direct relative concept map entry
     */
    public List<LinkOBJ> getConceptDirectLinks() {
        return conceptDirectLinks;
    }

    /**
     * set list link of other indirect relative concept map entry
     * 
     * @param conceptInDirectLinks
     *            list link of other indirect relative concept map entry
     * 
     */
    public void setConceptInDirectLinks(List<LinkOBJ> conceptInDirectLinks) {
        this.conceptInDirectLinks = conceptInDirectLinks;
        setConceptInDirectTitles(TKSSUtil.LinkToString(conceptInDirectLinks));
    }

    /**
     * get list link of other indirect relative concept map entry
     * 
     * @return list link of other indirect relative concept map entry
     */
    public List<LinkOBJ> getConceptInDirectLinks() {
        return conceptInDirectLinks;
    }

    /**
     * set all titles of other direct relative event entry
     * 
     * @param eventDirectTiles
     *            all titles of other direct relative event entry
     * 
     */
    public void setEventDirectTiles(String eventDirectTiles) {
        this.eventDirectTiles = eventDirectTiles;
    }

    /**
     * get all titles of other direct relative event entry
     * 
     * @return all titles of other direct relative event entry
     */
    public String getEventDirectTiles() {
        return eventDirectTiles;
    }

    /**
     * set all titles of other indirect relative event entry
     * 
     * @param eventInDirectTiles
     *            all titles of other indirect relative event entry
     * 
     */
    public void setEventInDirectTiles(String eventInDirectTiles) {
        this.eventInDirectTiles = eventInDirectTiles;
    }

    /**
     * get all titles of other indirect relative event entry
     * 
     * @return all titles of other indirect relative event entry
     */
    public String getEventInDirectTiles() {
        return eventInDirectTiles;
    }

    /**
     * set all titles of other direct relative wiki entry
     * 
     * @param wikiDirectTitles
     *            all titles of other direct relative wiki entry
     * 
     */
    public void setWikiDirectTitles(String wikiDirectTitles) {
        this.wikiDirectTitles = wikiDirectTitles;
    }

    /**
     * get all titles of other direct relative wiki entry
     * 
     * @return all titles of other direct relative wiki entry
     */
    public String getWikiDirectTitles() {
        return wikiDirectTitles;
    }

    /**
     * set all titles of other indirect relative wiki entry
     * 
     * @param wikiInDirectTitles
     *            all titles of other indirect relative wiki entry
     * 
     */
    public void setWikiInDirectTitles(String wikiInDirectTitles) {
        this.wikiInDirectTitles = wikiInDirectTitles;
    }

    /**
     * get all titles of other indirect relative wiki entry
     * 
     * @return all titles of other indirect relative wiki entry
     */
    public String getWikiInDirectTitles() {
        return wikiInDirectTitles;
    }

    /**
     * set all titles of other direct relative concept map entry
     * 
     * @param conceptDirectTitles
     *            all titles of other direct relative concept map entry
     * 
     */
    public void setConceptDirectTitles(String conceptDirectTitles) {
        this.conceptDirectTitles = conceptDirectTitles;
    }

    /**
     * get all titles of other direct relative concept map entry
     * 
     * @return all titles of other direct relative concept map entry
     */
    public String getConceptDirectTitles() {
        return conceptDirectTitles;
    }

    /**
     * set all titles of other indirect relative concept map entry
     * 
     * @param conceptInDirectTitles
     *            all titles of other indirect relative concept map entry
     * 
     */
    public void setConceptInDirectTitles(String conceptInDirectTitles) {
        this.conceptInDirectTitles = conceptInDirectTitles;
    }

    /**
     * get all titles of other indirect relative concept map entry
     * 
     * @return all titles of other indirect relative concept map entry
     */
    public String getConceptInDirectTitles() {
        return conceptInDirectTitles;
    }

    /**
     * set all respond tag object in message, permission, rating or team
     * 
     * @param responds
     *            all respond tag object in message, permission, rating or team
     * 
     */
    public void setResponds(List<RespondTagOBJ> responds) {
        this.responds = responds;
    }

    /**
     * get all respond tag object in message, permission, rating or team
     * 
     * @return list of respond tag object
     */
    public List<RespondTagOBJ> getResponds() {
        return responds;
    }

    /**
     * set the common title for convenient call
     * 
     * @param entryTitle
     */
    public void setEntryTitle(String entryTitle) {
        this.entryTitle = entryTitle;
        try {
            this.encodeTitle = URLEncoder.encode(entryTitle, TKSSConstant.ENCODING);
        } catch (UnsupportedEncodingException e) {
            // ignore
        }
    }

    /**
     * get the common title for convenient call
     * 
     * @return entry title
     */
    public String getEntryTitle() {
        return entryTitle;
    }

    /**
     * get entry class(entry type)
     * 
     * @return entry type
     */
    public String getEntryClass() {
        return entryClass;
    }

    /**
     * set entry class(entry type)
     * 
     * @param entryClass
     *            entry type
     */
    public void setEntryClass(String entryClass) {
        this.entryClass = entryClass;
    }

    /**
     * get entry level ex. local class or public
     * 
     * @return entry level
     */
    public String getEntryLevel() {
        return entryLevel;
    }

    /**
     * set entry level
     * 
     * @param entryLevel
     *            entry level
     */
    public void setEntryLevel(String entryLevel) {
        this.entryLevel = entryLevel;
    }

    /**
     * get encoded entry title
     * 
     * @return encoded entry title
     */
    public String getEncodeTitle() {
        return encodeTitle;
    }

    /**
     * set encoded entry title
     * 
     * @param encodeTitle
     *            encoded entry title
     */
    public void setEncodeTitle(String encodeTitle) {
        // ignore
    }

    /**
     * set the list of keywords of this entry, keyword string will be split to a
     * list
     * 
     * @param keywords
     *            the keyword which separates by comma
     * @throws IOException
     * 
     */
    public void setKeywordNodes(String keywords) throws IOException {
        String[] keyword = keywords.split(",");
        ArrayList<String> result = new ArrayList<String>();
        ArrayList<String> encodeResult = new ArrayList<String>();
        for (int i = 0; i < keyword.length; i++) {
            String source = keyword[i].trim();
            if (source.equals("")) continue; 
            if (source.startsWith(TKSSConstant.LANGUAGE_PREFIX)) {
                String word = source.substring(TKSSConstant.LANGUAGE_PREFIX.length(), source.length()).trim();
                String decodeKeyword = TKSSUtil.decodeBase64(word);
                result.add(decodeKeyword);
                encodeResult.add(URLEncoder.encode(decodeKeyword, TKSSConstant.ENCODING));
            } else {
                result.add(source.trim());
                encodeResult.add("");
            }
        }
        this.keywordNodes = result;
        this.encodeKeywordNodes = encodeResult;
    }

    /**
     * get keyword list of this entry
     * 
     * @return keyword list
     */
    public List<String> getKeywordNodes() {
        return keywordNodes;
    }

    public List<String> getEncodeKeywordNodes() {
        return encodeKeywordNodes;
    }

    public void setEncodeKeywordNodes(List<String> encodeKeywordNodes) {
        this.encodeKeywordNodes = encodeKeywordNodes;
    }

}