package tkss.util.orm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import tkss.util.orm.dao.ImageOBJDAO;

import com.yung.jpa.BasicEntity;

@Entity(name="IMAGES")
@EntityListeners(value={ImageOBJDAO.class})
public class ImageOBJ extends BasicEntity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3099679770132789670L;

    @Column(name="ID")
    @Id
    private String id;

    @Column(name="URL")
    private String url;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}