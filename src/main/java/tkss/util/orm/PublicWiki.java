package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import tkss.util.TKSSConstant;
import tkss.util.orm.dao.PublicWikiDAO;

/**
 * The class is used to map the class level wiki entry in database
 * 
 * @author ucam10a
 * 
 */
@XmlRootElement
@Entity(name="wiki")
@EntityListeners(value={PublicWikiDAO.class})
public class PublicWiki extends PublicEntryOBJ {

    /** wiki title */
    @Column(name="wikiWord")
    @Id
    protected String wikiWord;

    /** wiki text */
    @Column(name="text")
    protected String text;

    /** wiki the first picture */
    @Deprecated
    @Column(name="pic1")
    protected String pic1 = " ";

    /** wiki the second picture */
    @Deprecated
    @Column(name="pic2")
    protected String pic2= " ";

    /** wiki the third picture */
    @Deprecated
    @Column(name="pic3")
    protected String pic3 = " ";

    public PublicWiki() {
        setEntryClass(TKSSConstant.WIKI_TYPE);
    }

    /**
     * get the title of wiki
     * 
     * @return the title of wiki
     */
    public String getWikiWord() {
        return wikiWord;
    }

    /**
     * set the title of wiki
     * 
     * @param wikiWord
     *            the title of wikiWord
     */
    public void setWikiWord(String wikiWord) {
        this.wikiWord = wikiWord;
        setEntryTitle(wikiWord);
        setEntryClass(TKSSConstant.WIKI_TYPE);
    }

    /**
     * get wiki text content
     * 
     * @return wiki text content
     */
    public String getText() {
        return text;
    }

    /**
     * set the text content of wiki
     * 
     * @param text
     *            the text content of wiki
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * set the first picture path of wiki
     * 
     * @param pic1
     *            the path of first picture of wiki
     */
    @Deprecated
    public void setPic1(String pic1) {
        this.pic1 = pic1;
    }

    /**
     * get wiki first picture path
     * 
     * @return first picture path
     */
    @Deprecated
    public String getPic1() {
        return pic1;
    }

    /**
     * set the second picture path of wiki
     * 
     * @param pic2
     *            the path of second picture of wiki
     */
    @Deprecated
    public void setPic2(String pic2) {
        this.pic2 = pic2;
    }

    /**
     * get wiki second picture path
     * 
     * @return second picture path
     */
    @Deprecated
    public String getPic2() {
        return pic2;
    }

    /**
     * set the third picture path of wiki
     * 
     * @param pic3
     *            the path of third picture of wiki
     */
    @Deprecated
    public void setPic3(String pic3) {
        this.pic3 = pic3;
    }

    /**
     * get wiki third picture path
     * 
     * @return third picture path
     */
    @Deprecated
    public String getPic3() {
        return pic3;
    }

}