package tkss.util.orm;

/**
 * The class is used to store the state of the entry
 * 
 * @author Yung Long
 * 
 */
public class StatusOBJ {

    /** check if there is a alert message */
    protected boolean hasMessage = false;

    /** check if client is in edit-group */
    protected boolean inGroup = false;

    /** check if this entry is applied to public database */
    protected boolean appliedPublicDataBase = false;

    /** check if this entry has updating request */
    protected boolean hasUpdating = false;

    /** check if this entry has someone want to join edit-group */
    protected boolean hasApplyingJoin = false;

    /** check if this entry is applied to open-to-class */
    protected boolean hasApplyingOpen = false;

    /** check if client has rated this entry before */
    protected boolean hasRated = false;

    /** check if client has applied to join edit-group */
    protected boolean appliedJoin = false;

    /** the respond message */
    protected String respond;

    /** the respond tag location, ex. message, permission, rating, team */
    protected String respondTag;

    /**
     * set true if client is in edit-group
     * 
     * @param inGroup
     *            true if client is in edit-group
     * 
     */
    public void setInGroup(boolean inGroup) {
        this.inGroup = inGroup;
    }

    /**
     * check if client is in edit-group
     * 
     * @return true if client is in edit-group
     */
    public boolean isInGroup() {
        return inGroup;
    }

    /**
     * set true if this entry has someone who is applying to join edit-group
     * 
     * @param hasApplyingJoin
     *            true if this entry has someone who is applying to join
     *            edit-group
     * 
     */
    public void setHasApplyingJoin(boolean hasApplyingJoin) {
        this.hasApplyingJoin = hasApplyingJoin;
        if (hasApplyingJoin == true) setHasMessage(true);
    }

    /**
     * check if this entry has someone who is applying to join edit-group
     * 
     * @return true if this entry has someone who is applying to join edit-group
     */
    public boolean isHasApplyingJoin() {
        return hasApplyingJoin;
    }

    /**
     * set true if this entry is applying to open-to-class
     * 
     * @param hasApplyingOpen
     *            true if this entry is applying to open-to-class
     * 
     */
    public void setHasApplyingOpen(boolean hasApplyingOpen) {
        this.hasApplyingOpen = hasApplyingOpen;
        if (hasApplyingOpen == true) setHasMessage(true);
    }

    /**
     * check if this entry is applying to open-to-class
     * 
     * @return true if this entry is applying to open-to-class
     */
    public boolean isHasApplyingOpen() {
        return hasApplyingOpen;
    }

    /**
     * set true if client has rated this entry before
     * 
     * @param hasRated
     *            true if client has rated this entry before
     * 
     */
    public void setHasRated(boolean hasRated) {
        this.hasRated = hasRated;
    }

    /**
     * check if client has rated this entry before
     * 
     * @return true if client has rated this entry before
     */
    public boolean isHasRated() {
        return hasRated;
    }

    /**
     * set true if this entry has an updating request
     * 
     * @param hasUpdating
     *            true if this entry has an updating request
     * 
     */
    public void setHasUpdating(boolean hasUpdating) {
        this.hasUpdating = hasUpdating;
        if (hasUpdating == true) setHasMessage(true);
    }

    /**
     * check true if this entry has an updating request
     * 
     * @return true if this entry has an updating request
     */
    public boolean isHasUpdating() {
        return hasUpdating;
    }

    /**
     * set respond message
     * 
     * @param respond
     *            respond message
     * 
     */
    public void setRespond(String respond) {
        this.respond = respond;
    }

    /**
     * get respond message
     * 
     * @return respond message
     */
    public String getRespond() {
        return respond;
    }

    /**
     * set respond message tag location, ex. message, permission, rating or team
     * 
     * @param respondTag
     *            location, ex. message, permission, rating or team
     * 
     */
    public void setRespondTag(String respondTag) {
        this.respondTag = respondTag;
    }

    /**
     * get respond tag object
     * 
     * @return respond tag object
     */
    public String getRespondTag() {
        return respondTag;
    }

    /**
     * set true if there is at least one alert message
     * 
     * @param hasMessage
     *            true if there is at least one alert message
     * 
     */
    public void setHasMessage(boolean hasMessage) {
        this.hasMessage = hasMessage;
    }

    /**
     * check true if there is at least one alert message
     * 
     * @return true if there is at least one alert message
     */
    public boolean isHasMessage() {
        return hasMessage;
    }

    /**
     * set true if this entry is applied to public database
     * 
     * @param appliedPublicDataBase
     *            true if this entry is applied to public database
     * 
     */
    public void setAppliedPublicDataBase(boolean appliedPublicDataBase) {
        this.appliedPublicDataBase = appliedPublicDataBase;
    }

    /**
     * check true if this entry is applied to public database
     * 
     * @return true if this entry is applied to public database
     */
    public boolean isAppliedPublicDataBase() {
        return appliedPublicDataBase;
    }

    /**
     * set true if this client has applied to join this edit-group
     * 
     * @param appliedJoin
     *            true if this client has applied to join this edit-group
     * 
     */
    public void setAppliedJoin(boolean appliedJoin) {
        this.appliedJoin = appliedJoin;
    }

    /**
     * check true if this client has applied to join this edit-group
     * 
     * @return true if this client has applied to join this edit-group
     */
    public boolean isAppliedJoin() {
        return appliedJoin;
    }

}
