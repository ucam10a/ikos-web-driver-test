package tkss.util.orm;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;

import tkss.util.orm.dao.TeamOBJDAO;

/**
 * Object/Relational Mapping to team table
 * 
 * @author Yung Long Li
 *
 */
@Entity(name="team")
@EntityListeners(value={TeamOBJDAO.class})
public class TeamOBJ extends BasicEntity {

    /** team id */
    @Column(name="teamID")
    @Id
    protected String teamID;

    @Column(name="member")
    protected String member;
    
    /** member usre id list */
    protected List<String> memberIDs;

    /** application status */
    @Column(name="status")
    protected String status;

    /** entry type */
    @Column(name="type")
    protected String type;

    /** class id */
    @Column(name="classID")
    protected Integer classID;
    
    protected Integer entryID;

    /**
     * get team id
     * 
     * @return team id
     */
    public String getTeamID() {
        return teamID;
    }

    /**
     * set team id
     * 
     * @param teamID
     *            team id
     */
    public void setTeamID(String teamID) {
        this.teamID = teamID;
        if (teamID != null) {
            int index = teamID.indexOf("_");
            if (index > 0) {
                this.entryID = Integer.valueOf(teamID.substring(0, index));
            }
        }
    }

    /**
     * get team member list
     * 
     * @return team member list
     */
    public List<String> getMemberIDs() {
        return memberIDs;
    }

    /**
     * set team member list
     * 
     * @param memberIDs
     *            team member list
     */
    public void setMemberIDs(List<String> memberIDs) {
        this.memberIDs = memberIDs;
    }

    /**
     * get entry type
     * 
     * @return entry type
     */
    public String getType() {
        return type;
    }

    /**
     * set entry type
     * 
     * @param type
     *            entry type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * get application status
     * 
     * @return application status
     */
    public String getStatus() {
        return status;
    }

    /**
     * set application status
     * 
     * @param status
     *            application status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * get class id
     * 
     * @return class id
     */
    public Integer getClassID() {
        return classID;
    }

    /**
     * set class id
     * 
     * @param classID
     *            class id
     */
    public void setClassID(Integer classID) {
        this.classID = classID;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public Integer getEntryID() {
        return entryID;
    }

    public void setEntryID(Integer entryID) {
        this.entryID = entryID;
    }

}