package tkss.util.orm;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import tkss.util.orm.dao.EventLockDAO;

@Entity(name="eventlock")
@EntityListeners(value={EventLockDAO.class})
public class EventLock extends LockOBJ {

}