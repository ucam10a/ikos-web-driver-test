package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;

/**
 * Object/Relational Mapping to rating table
 * 
 * @author Yung Long Li
 *
 */
public class RatingOBJ extends BasicEntity {

    /** entry id */
    @Column(name="id")
    @Id
    protected Integer entryID;

    /** user id */
    @Column(name="userID")
    @Id
    protected String userID;

    /** rating score from 1-5 */
    @Column(name="score")
    protected Integer score;

    /**
     * get target entry id
     * 
     * @return entry id
     */
    public Integer getEntryID() {
        return entryID;
    }

    /**
     * set target entry id
     * 
     * @param entryID
     */
    public void setEntryID(Integer entryID) {
        this.entryID = entryID;
    }

    /**
     * get rating score
     * 
     * @return rating score
     */
    public Integer getScore() {
        return score;
    }

    /**
     * set rating score
     * 
     * @param score
     *            rating score
     */
    public void setScore(Integer score) {
        this.score = score;
    }

    /**
     * get rater user id
     * 
     * @return rater user id
     */
    public String getUserID() {
        return userID;
    }

    /**
     * set rater user id
     * 
     * @param userID
     *            rater user id
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

}