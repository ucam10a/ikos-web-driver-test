package tkss.util.orm;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import tkss.util.orm.dao.ConceptRatingDAO;

@Entity(name="localconceptrating")
@EntityListeners(value={ConceptRatingDAO.class})
public class ConceptRating extends RatingOBJ {

}