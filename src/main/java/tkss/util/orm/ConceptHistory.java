package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import tkss.util.orm.dao.ConceptHistoryDAO;

@Entity(name="concepthistory")
@EntityListeners(value={ConceptHistoryDAO.class})
public class ConceptHistory extends HistoryOBJ {

    @Column(name="xml")
    protected String xml;

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }
    
}
