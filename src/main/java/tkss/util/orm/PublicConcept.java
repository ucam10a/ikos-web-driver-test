package tkss.util.orm;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import tkss.util.TKSSConstant;
import tkss.util.orm.dao.PublicConceptDAO;

/**
 * The class is used to map to the database public level concept map table
 * 
 * @author Yung Long
 * 
 */
@XmlRootElement
@Entity(name="concept")
@EntityListeners(value={PublicConceptDAO.class})
public class PublicConcept extends PublicEntryOBJ {

    /** concept map title */
    @Column(name="conceptWord")
    @Id
    protected String conceptWord;

    /** concept map xml data */
    @Column(name="xml")
    protected String xml;

    /** concept map serialized xml data */
    protected String serializedXml;

    /** concept map picture file path */
    @Deprecated
    protected String conceptPic;

    public PublicConcept() {
        setEntryClass(TKSSConstant.CONCEPT_MAP_TYPE);
    }

    /**
     * get the concept map title
     * 
     * @return concept map title
     */
    public String getConceptWord() {
        return conceptWord;
    }

    /**
     * set the concept map title
     * 
     */
    public void setConceptWord(String conceptWord) {
        this.conceptWord = conceptWord;
        setEntryTitle(conceptWord);
        setEntryClass(TKSSConstant.CONCEPT_MAP_TYPE);
    }

    /**
     * get the concept map xml
     * 
     * @return concept map xml data
     */
    public String getXml() {
        return xml;
    }

    /**
     * set the concept map xml data
     * 
     * 
     */
    public void setXml(String xml) {
        this.xml = xml;
        try {
            this.serializedXml = URLEncoder.encode(xml, TKSSConstant.ENCODING);
        } catch (UnsupportedEncodingException e) {
            // ignore
        }
    }

    /**
     * set the concept map picture file path
     * 
     * 
     */
    @Deprecated
    public void setConceptPic(String conceptPic) {
        this.conceptPic = conceptPic;
    }

    /**
     * get the concept map picture file path
     * 
     * @return concept map picture file path
     */
    @Deprecated
    public String getConceptPic() {
        return conceptPic;
    }

    public String getSerializedXml() {
        return serializedXml;
    }

    public void setSerializedXml(String serializedXml) {
        this.serializedXml = serializedXml;
    }

}