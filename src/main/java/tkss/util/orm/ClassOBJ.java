package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;

import tkss.util.orm.dao.ClassOBJDAO;

/**
 * The class is used to map to the database class table
 * 
 * @author Yung Long
 * 
 */
@Entity(name="class")
@EntityListeners(value={ClassOBJDAO.class})
public class ClassOBJ extends BasicEntity {

    /** class id */
    @Column(name="classID")
    protected Integer classID = -1;

    /** class name */
    @Column(name="className")
    @Id
    protected String className;

    /** class field */
    @Column(name="field")
    @Id
    protected String field;

    /** class status ex. open or close */
    @Column(name="status")
    protected String status;

    /** class teacher username */
    @Column(name="teacher")
    protected String teacher;

    /** class code */
    @Column(name="classCode")
    protected String classCode;

    /** class alias */
    protected String alias;

    /** class teacher first name */
    protected String firstName;

    /** class teacher last name */
    protected String lastName;

    /** alert message for event in this class */
    protected String AlertOfEvent;

    /** alert message for wiki in this class */
    protected String AlertOfWiki;

    /** alert message for concept map in this class */
    protected String AlertOfConcept;
    
    /** alert message for book in this class */
    protected String AlertOfBook;

    /** has alert message */
    protected boolean Alert = false;

    /**
     * set class id
     * 
     * @param classID
     *            means that the class id
     */
    public void setClassID(Integer classID) {
        this.classID = classID;
    }

    /**
     * get the class id
     * 
     * @return class id
     */
    public Integer getClassID() {
        return classID;
    }

    /**
     * set teacher username
     * 
     * @param teacher
     *            the username of teacher
     */
    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    /**
     * get teacher's username
     * 
     * @return teacher username
     */
    public String getTeacher() {
        return teacher;
    }

    /**
     * set class code
     * 
     * @param classCode
     *            class code is used for registering class
     */
    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    /**
     * get the class code for registering class
     * 
     * @return the class code for registering
     */
    public String getClassCode() {
        return classCode;
    }

    /**
     * set field of class ex. biology, physics
     * 
     * @param field
     *            ex. biology, physics
     */
    public void setField(String field) {
        this.field = field;
    }

    /**
     * get the field of the class(ex. biology, phtsics)
     * 
     * @return the filed
     */
    public String getField() {
        return field;
    }

    /**
     * set status of class ex. open or close
     * 
     * @param status
     *            ex. "open" or "close"
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * get the status of the class (ex. open or close)
     * 
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * set teacher first name
     * 
     * @param firstName
     *            teacher's first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * get teacher first name
     * 
     * @return teacher first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * set teacher last name
     * 
     * @param lastName
     *            teacher's last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * get teacher last name
     * 
     * @return teacher last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * set the alert message of event
     * 
     * @param alertOfEvent
     *            alert message of event
     */
    public void setAlertOfEvent(String alertOfEvent) {
        AlertOfEvent = alertOfEvent;
    }

    /**
     * get the alert message of event
     * 
     * @return the alert message of event
     */
    public String getAlertOfEvent() {
        return AlertOfEvent;
    }

    /**
     * set the alert message of wiki
     * 
     * @param alertOfWiki
     *            alert message of wiki
     */
    public void setAlertOfWiki(String alertOfWiki) {
        AlertOfWiki = alertOfWiki;
    }

    /**
     * get the alert message of wiki
     * 
     * @return the alert message of wiki
     */
    public String getAlertOfWiki() {
        return AlertOfWiki;
    }

    /**
     * set the alert message of concept map
     * 
     * @param alertOfConcept
     *            alert message of concept map
     */
    public void setAlertOfConcept(String alertOfConcept) {
        AlertOfConcept = alertOfConcept;
    }

    /**
     * get the alert message of concept map
     * 
     * @return the alert message of concept map
     */
    public String getAlertOfConcept() {
        return AlertOfConcept;
    }

    /**
     * set the class name
     * 
     * @param className
     *            class name
     */
    public void setClassName(String className) {
        this.className = className;
        String[] arr = className.split("_");
        if (arr != null && arr.length == 2) {
            if (this.field == null) {
                this.field = arr[0];
            }
            if (this.alias == null) {
                this.alias = arr[1];
            }
        }
    }

    /**
     * get class name
     * 
     * @return class name
     */
    public String getClassName() {
        return className;
    }

    /**
     * set true if there is a alert
     * 
     * @param alert
     *            true if there is a alert
     */
    public void setAlert(boolean alert) {
        Alert = alert;
    }

    /**
     * get boolean of alert
     * 
     * @return true if it has alert message
     */
    public boolean isAlert() {
        return Alert;
    }

    /**
     * get class alias
     * 
     * @return class alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * set class alias
     * 
     * @param alias
     *            class alias
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * set the alert message of book
     * 
     * @param alertOfBook
     *            alert message of book
     */
    public String getAlertOfBook() {
        return AlertOfBook;
    }

    /**
     * set the alert message of book
     * 
     * @param alertOfBook
     *            alert message of book
     */
    public void setAlertOfBook(String alertOfBook) {
        AlertOfBook = alertOfBook;
    }

}