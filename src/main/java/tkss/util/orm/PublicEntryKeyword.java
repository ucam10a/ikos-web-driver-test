package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;

public class PublicEntryKeyword extends BasicEntity {

    @Column(name="field")
    @Id
    private String field;
    
    @Column(name="keyword")
    @Id
    private String keyword;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    
    
}