package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import tkss.util.orm.dao.WikiHistoryDAO;

@Entity(name="wikihistory")
@EntityListeners(value={WikiHistoryDAO.class})
public class WikiHistory extends HistoryOBJ {
    
    @Column(name="text")
    protected String text;
    
    @Column(name="keyword")
    protected String keyword;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    
}
