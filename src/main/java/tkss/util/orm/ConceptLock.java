package tkss.util.orm;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import tkss.util.orm.dao.ConceptLockDAO;

@Entity(name="conceptlock")
@EntityListeners(value={ConceptLockDAO.class})
public class ConceptLock extends LockOBJ {

}