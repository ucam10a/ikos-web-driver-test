package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Id;

import tkss.util.TKSSConstant;

/**
 * The public entry object which is used to store the public entry ex. event,
 * wiki or concept map
 * 
 * @author Yung Long
 * 
 */
public class PublicEntryOBJ extends EntryOBJ {

    /** store the latest serial number of public entry */
    @Column(name="sqNumber")
    @Id
    protected Integer sqNumber;

    /** the entry field, ex. biology, physics */
    @Column(name="field")
    @Id
    protected String field;

    /** store the public entry author */
    @Column(name="userID")
    protected String authors;

    /** refer to local entry Id */
    protected int referenceId = -1;

    /**
     * set the latest serial number of public entry
     * 
     * @param sqNumber
     *            latest serial number of public entry
     * 
     */
    public void setSqNumber(Integer sqNumber) {
        this.sqNumber = sqNumber;
    }

    /**
     * get the latest serial number of public entry
     * 
     * @return latest serial number of public entry
     */
    public Integer getSqNumber() {
        return sqNumber;
    }

    /**
     * set entry field, ex. biology, physics
     * 
     * @param field
     *            entry field
     * 
     */
    public void setField(String field) {
        this.field = field;
        this.entryLevel = TKSSConstant.LEVEL_PUBLIC;
    }

    /**
     * get entry field ex. biology, physics
     * 
     * @return entry field
     */
    public String getField() {
        return field;
    }

    /**
     * set the public entry author
     * 
     * @param authors
     *            public entry author
     */
    public void setAuthors(String authors) {
        this.authors = authors;
    }

    /**
     * get the public entry author
     * 
     * @return the public entry author
     */
    public String getAuthors() {
        return authors;
    }

    /**
     * get reference Id
     * 
     * @return reference Id
     */
    public int getReferenceId() {
        return referenceId;
    }

    /**
     * set reference Id
     * 
     * @param referenceId
     *            reference Id
     */
    public void setReferenceId(int referenceId) {
        this.referenceId = referenceId;
    }

}