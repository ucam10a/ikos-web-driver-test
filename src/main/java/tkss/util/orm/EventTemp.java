package tkss.util.orm;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import tkss.util.orm.dao.EventTempDAO;

@Entity(name="eventtemp")
@EntityListeners(value={EventTempDAO.class})
public class EventTemp extends TempOBJ {

}