package tkss.util.orm;

import java.util.ArrayList;

/**
 * The class is used to store the statistic information of student
 * 
 * @author Yung Long
 * 
 */
public class StudentOBJ extends UserOBJ {

    /** the list of event entry title which is created by the student */
    protected ArrayList<String> LeadingEvent;

    /** the string of event entry tilte which is created by the student */
    protected String LeadEventTitle;

    /** the list of event entry title which is co-edited by the student */
    protected ArrayList<String> CoEditEvent;

    /** the string of event entry tilte which is co-edited by the student */
    protected String CoEditEventTitle;

    /** the list of wiki entry title which is created by the student */
    protected ArrayList<String> LeadingWiki;

    /** the string of wiki entry title which is created by the student */
    protected String LeadWikiTitle;

    /** the list of wiki entry title which is co-edited by the student */
    protected ArrayList<String> CoEditWiki;

    /** the string of wiki entry title which is co-edited by the student */
    protected String CoEditWikiTitle;

    /** the list of concept map entry title which is created by the student */
    protected ArrayList<String> LeadingConcept;

    /** the string of concept map entry title which is created by the student */
    protected String LeadConceptTitle;

    /** the list of concept map entry title which is co-edited by the student */
    protected ArrayList<String> CoEditConcept;

    /** the string of concept map entry title which is co-edited by the student */
    protected String CoEditConceptTitle;

    /**
     * set the student's creating event title list
     * 
     * @param leadingEvent
     *            the list of creating event entry title
     * 
     */
    public void setLeadingEvent(ArrayList<String> leadingEvent) {
        LeadingEvent = leadingEvent;
        boolean start = true;
        String titles = "";
        for (String title : leadingEvent) {
            if (start == true) {
                titles = title + ", ";
                start = false;
            } else {
                titles = titles + title + ", ";
            }
        }
        if (titles.length() > 2) {
            titles = titles.substring(0, (titles.length() - 2));
        }
        setLeadEventTitle(titles);
    }

    /**
     * get the student's creating event title list
     * 
     * @return the student's creating event title list
     */
    public ArrayList<String> getLeadingEvent() {
        return LeadingEvent;
    }

    /**
     * set the student's co-editing event title list
     * 
     * @param coEditEvent
     *            the list of co-editing event entry title
     * 
     */
    public void setCoEditEvent(ArrayList<String> coEditEvent) {
        CoEditEvent = coEditEvent;
        setCoEditEventTitle(ListToString(coEditEvent));
    }

    /**
     * get the student's co-editing event title list
     * 
     * @return the student's co-editing event title list
     */
    public ArrayList<String> getCoEditEvent() {
        return CoEditEvent;
    }

    /**
     * set the student's creating wiki title list
     * 
     * @param leadingWiki
     *            the list of creating wiki entry title
     * 
     */
    public void setLeadingWiki(ArrayList<String> leadingWiki) {
        LeadingWiki = leadingWiki;
        setLeadWikiTitle(ListToString(leadingWiki));
    }

    /**
     * get the student's creating wiki title list
     * 
     * @return the student's creating wiki title list
     */
    public ArrayList<String> getLeadingWiki() {
        return LeadingWiki;
    }

    /**
     * set the student's co-editing wiki title list
     * 
     * @param coEditWiki
     *            the list of co-editing wiki entry title
     * 
     */
    public void setCoEditWiki(ArrayList<String> coEditWiki) {
        CoEditWiki = coEditWiki;
        setCoEditWikiTitle(ListToString(coEditWiki));
    }

    /**
     * get the student's co-editing wiki title list
     * 
     * @return the student's co-editing wiki title list
     */
    public ArrayList<String> getCoEditWiki() {
        return CoEditWiki;
    }

    /**
     * set the student's creating concept map title list
     * 
     * @param leadingConcept
     *            the list of creating concept map entry title
     * 
     */
    public void setLeadingConcept(ArrayList<String> leadingConcept) {
        LeadingConcept = leadingConcept;
        setLeadConceptTitle(ListToString(leadingConcept));
    }

    /**
     * get the student's creating concept map title list
     * 
     * @return the student's creating concept map title list
     */
    public ArrayList<String> getLeadingConcept() {
        return LeadingConcept;
    }

    /**
     * set the student's co-editing concept map title list
     * 
     * @param coEditConcept
     *            the list of co-editing concept map entry title
     * 
     */
    public void setCoEditConcept(ArrayList<String> coEditConcept) {
        CoEditConcept = coEditConcept;
        setCoEditConceptTitle(ListToString(coEditConcept));
    }

    /**
     * get the student's co-editing concept map title list
     * 
     * @return the student's co-editing concept map title list
     */
    public ArrayList<String> getCoEditConcept() {
        return CoEditConcept;
    }

    /**
     * set the student's creating event title string, ex. title1, title2, title3
     * ..
     * 
     * @param leadEventTitle
     *            creating event title string
     * 
     */
    public void setLeadEventTitle(String leadEventTitle) {
        LeadEventTitle = leadEventTitle;
    }

    /**
     * get the student's creating event title string, ex. title1, title2, title3
     * ..
     * 
     * @return the student's creating event title string
     */
    public String getLeadEventTitle() {
        return LeadEventTitle;
    }

    /**
     * set the student's co-editing event title string, ex. title1, title2,
     * title3 ..
     * 
     * @param coEditEventTitle
     *            co-editing event title string
     * 
     */
    public void setCoEditEventTitle(String coEditEventTitle) {
        CoEditEventTitle = coEditEventTitle;
    }

    /**
     * get the student's co-editing event title string, ex. title1, title2,
     * title3 ..
     * 
     * @return the student's co-editing event title string
     */
    public String getCoEditEventTitle() {
        return CoEditEventTitle;
    }

    /**
     * set the student's creating wiki title string, ex. title1, title2, title3
     * ..
     * 
     * @param leadWikiTitle
     *            creating wiki title string
     * 
     */
    public void setLeadWikiTitle(String leadWikiTitle) {
        LeadWikiTitle = leadWikiTitle;
    }

    /**
     * get the student's creating wiki title string, ex. title1, title2, title3
     * ..
     * 
     * @return the student's creating wiki title string
     */
    public String getLeadWikiTitle() {
        return LeadWikiTitle;
    }

    /**
     * set the student's co-editing wiki title string, ex. title1, title2,
     * title3 ..
     * 
     * @param coEditWikiTitle
     *            co-editing wiki title string
     * 
     */
    public void setCoEditWikiTitle(String coEditWikiTitle) {
        CoEditWikiTitle = coEditWikiTitle;
    }

    /**
     * get the student's co-editing wiki title string, ex. title1, title2,
     * title3 ..
     * 
     * @return the student's co-editing wiki title string
     */
    public String getCoEditWikiTitle() {
        return CoEditWikiTitle;
    }

    /**
     * set the student's creating concept map title string, ex. title1, title2,
     * title3 ..
     * 
     * @param leadConceptTitle
     *            creating concept map title string
     * 
     */
    public void setLeadConceptTitle(String leadConceptTitle) {
        LeadConceptTitle = leadConceptTitle;
    }

    /**
     * get the student's creating concept map title string, ex. title1, title2,
     * title3 ..
     * 
     * @return the student's creating concept map title string
     */
    public String getLeadConceptTitle() {
        return LeadConceptTitle;
    }

    /**
     * set the student's co-editing concept map title string, ex. title1,
     * title2, title3 ..
     * 
     * @param coEditConceptTitle
     *            co-editing concept map title string
     * 
     */
    public void setCoEditConceptTitle(String coEditConceptTitle) {
        CoEditConceptTitle = coEditConceptTitle;
    }

    /**
     * get the student's co-editing concept map title string, ex. title1,
     * title2, title3 ..
     * 
     * @return the student's co-editing concept map title string
     */
    public String getCoEditConceptTitle() {
        return CoEditConceptTitle;
    }

    private String ListToString(ArrayList<String> list) {

        String result = "";
        boolean start = true;
        for (String str : list) {
            if (start) {
                result = result + str;
                start = false;
            } else {
                result = result + ", " + str;
            }

        }
        return result;
    }

}
