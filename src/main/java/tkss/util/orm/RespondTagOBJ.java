package tkss.util.orm;

/**
 * The class is used to show the alert message in mypage jsp
 * 
 * @author Yung Long
 * 
 */
public class RespondTagOBJ {

    /** the tile of respond tag ex. new commer, new joiner, new update etc. */
    protected String title;

    /** the location of respond tag ex. message, permission, rating or team */
    protected String anchor;

    /** the content text */
    protected String content;

    /** the type of show page ex. eventview, eventedit etc */
    protected String showPage;

    /**
     * set the respond tag title
     * 
     * @param title
     *            tile of respond tag ex. new commer, new joiner, new update etc
     * 
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * get the respond tag title
     * 
     * @return the respond tag title
     */
    public String getTitle() {
        return title;
    }

    /**
     * set the respond tag location ex. message, permission, rating or team
     * 
     * @param anchor
     *            the respond tag location ex. message, permission, rating or
     *            team
     * 
     */
    public void setAnchor(String anchor) {
        this.anchor = anchor;
    }

    /**
     * get the respond tag location ex. message, permission, rating or team
     * 
     * @return the respond tag location
     */
    public String getAnchor() {
        return anchor;
    }

    /**
     * set the respond tag content text
     * 
     * @param content
     *            respond tag content text
     * 
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * get the respond tag content text
     * 
     * @return respond tag content text
     */
    public String getContent() {
        return content;
    }

    /**
     * set the type of show page ex. eventview, eventedit etc
     * 
     * @param showPage
     *            the type of show page
     * 
     */
    public void setShowPage(String showPage) {
        this.showPage = showPage;
    }

    /**
     * get the type of show page ex. eventview, eventedit etc
     * 
     * @return the type of show page
     */
    public String getShowPage() {
        return showPage;
    }

}
