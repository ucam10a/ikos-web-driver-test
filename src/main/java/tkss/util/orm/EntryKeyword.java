package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Id;

public class EntryKeyword {

    @Column(name="keyword")
    @Id
    private String keyword;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    
}