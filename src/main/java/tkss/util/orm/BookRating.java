package tkss.util.orm;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import tkss.util.orm.dao.BookRatingDAO;

@Entity(name="localbookrating")
@EntityListeners(value={BookRatingDAO.class})
public class BookRating extends RatingOBJ {

}