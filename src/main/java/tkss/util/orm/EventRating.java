package tkss.util.orm;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import tkss.util.orm.dao.EventRatingDAO;

@Entity(name="localeventrating")
@EntityListeners(value={EventRatingDAO.class})
public class EventRating extends RatingOBJ {

}