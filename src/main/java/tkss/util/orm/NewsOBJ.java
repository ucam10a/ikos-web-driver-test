package tkss.util.orm;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;

import tkss.util.orm.dao.NewsOBJDAO;

/**
 * The class is used to map the news table in database
 * 
 * @author Yung Long
 * 
 */
@Entity(name="news")
@EntityListeners(value={NewsOBJDAO.class})
public class NewsOBJ extends BasicEntity {

    /** news title */
    @Column(name="title")
    protected String title;

    /** the content of news */
    @Column(name="content")
    protected String content;
    
    /** create date */
    @Column(name="createDate")
    @Id
    private Timestamp createDate;

    /**
     * set the news title
     * 
     * @param title
     *            news title
     * 
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * get the title of news
     * 
     * @return the title of news
     */
    public String getTitle() {
        return title;
    }

    /**
     * set the content of news
     * 
     * @param content
     *            content of news
     * 
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * get the content of news
     * 
     * @return the content of news
     */
    public String getContent() {
        return content;
    }

    /**
     * get create date of news
     * 
     * @return get create date
     */
    public Timestamp getCreateDate() {
        return createDate;
    }

    /**
     * set create date of news
     * 
     * @param createDate
     *            create date
     * 
     */
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

}