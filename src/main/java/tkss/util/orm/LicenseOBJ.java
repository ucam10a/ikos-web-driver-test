package tkss.util.orm;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import tkss.report.HandsonInfo;
import tkss.util.TKSSUtil;
import tkss.util.orm.dao.LicenseOBJDAO;

import com.yung.jpa.BasicEntity;

@Entity(name="license")
@EntityListeners(value={LicenseOBJDAO.class})
public class LicenseOBJ extends BasicEntity {

    public static final int DEMO = -1;
    public static final int FOREVER = -2;
    public static final int PERSON = -3;
    
    @HandsonInfo(title="status", width=60, type="html")
    private String msg;
    
    @Column(name="id")
    @HandsonInfo(title="id", width=60, type="numeric")
    private Integer id;

    @HandsonInfo(title="name", width=120)
    private String name;
    
    @Id
    @Column(name="name")
    private String encodeName;
    
    @Column(name="licensecount")
    @HandsonInfo(title="license number", width=100, type="numeric")
    private Integer count;
    
    @Column(name="expiredate")
    private Timestamp expireTsp;
    
    @HandsonInfo(title="expire date", width=150, type="date")
    private String expireDate;
    
    @HandsonInfo(title="total students", width=100, type="numeric")
    private Integer studentCount;
    
    @Column(name="code")
    @HandsonInfo(title="code", width=200)
    private String code;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        name = TKSSUtil.getDecodeTitle(name);
        this.name = name;
        if (this.encodeName == null) {
            this.encodeName = TKSSUtil.getEncodeTitle(name);
        }
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Timestamp getExpireTsp() {
        return expireTsp;
    }

    public void setExpireTsp(Timestamp expireTsp) {
        this.expireTsp = expireTsp;
        if (expireTsp != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            this.expireDate = sdf.format(expireTsp);
        }
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
        if (expireDate != null) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                this.expireTsp = new Timestamp(sdf.parse(expireDate).getTime());
            } catch (ParseException e) {
            }
        }
    }

    public Integer getStudentCount() {
        return studentCount;
    }

    public void setStudentCount(Integer studentCount) {
        this.studentCount = studentCount;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getEncodeName() {
        return encodeName;
    }

    public void setEncodeName(String encodeName) {
        this.encodeName = encodeName;
        if (this.name == null) {
            this.name = TKSSUtil.getDecodeTitle(encodeName);
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
}