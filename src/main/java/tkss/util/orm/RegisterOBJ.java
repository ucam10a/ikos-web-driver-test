package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;

import tkss.util.orm.dao.RegisterOBJDAO;

/**
 * 
 * @author Yung Long Li
 *
 */
@Entity(name="register")
@EntityListeners(value={RegisterOBJDAO.class})
public class RegisterOBJ extends BasicEntity {

    @Column(name="classID")
    @Id
    private Integer classID;
    
    @Column(name="student")
    @Id
    private String student;
    
    @Column(name="grade")
    private String grade;
    
    @Column(name="comments")
    private String comments;

    public Integer getClassID() {
        return classID;
    }

    public void setClassID(Integer classID) {
        this.classID = classID;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
    
}