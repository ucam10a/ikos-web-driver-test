package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import tkss.util.orm.dao.PublicConceptKeywordDAO;

@Entity(name="conceptkeyword")
@EntityListeners(value={PublicConceptKeywordDAO.class})
public class PublicConceptKeyword extends PublicEntryKeyword{

    @Column(name="conceptWord")
    @Id
    private String conceptWord;

    public String getConceptWord() {
        return conceptWord;
    }

    public void setConceptWord(String conceptWord) {
        this.conceptWord = conceptWord;
    }
    
}