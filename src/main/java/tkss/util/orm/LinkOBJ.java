package tkss.util.orm;

/**
 * This object is to represent a link of iKOS entry
 * 
 * @author Yung Long
 * 
 */
public class LinkOBJ {

    /** mean the title of entry */
    protected String Word;

    /**
     * page is used to mean the viewer or editor of entry. ex. eventview or
     * eventedit
     */
    protected String page;

    /**
     * get the title of entry
     * 
     * @return title
     */
    public String getWord() {
        return Word;
    }

    /**
     * set title of entry
     * 
     * @param Word
     *            title of entry
     * 
     */
    public void setWord(String Word) {
        this.Word = Word;
    }

    /**
     * get the viewer or editor of entry ex. eventview or eventedit
     * 
     * @return page
     */
    public String getPage() {
        return page;
    }

    /**
     * set viewer or editor of entry
     * 
     * @param page
     * 
     */
    public void setPage(String page) {
        this.page = page;
    }

}
