package tkss.util.orm;

/**
 * This class is used to represent the youtube
 * 
 * @author Yung long
 * 
 */
public class YoutubeOBJ {

    /** the background container width */
    protected int width = 800;

    /** the background container height */
    protected int height = 600;

    /** the embedded video left padding to background container */
    protected int paddingLeft = 50;

    /** the embedded video top padding to background container */
    protected int paddingTop = 50;

    /** the embedded video width */
    protected int videoWidth = 420;

    /** the embedded video height */
    protected int videoHeight = 315;

    /** youtube video id */
    protected String videoId;

    /**
     * set background container width
     * 
     * @param width
     *            background container width
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * get background container width
     * 
     * @return background container width
     */
    public int getWidth() {
        return width;
    }

    /**
     * set background container height
     * 
     * @param height
     *            background container height
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * get background container height
     * 
     * @return background container height
     */
    public int getHeight() {
        return height;
    }

    /**
     * set embedded video left padding to background container
     * 
     * @param paddingLeft
     *            embedded video left padding to background container
     */
    public void setPaddingLeft(int paddingLeft) {
        this.paddingLeft = paddingLeft;
    }

    /**
     * get embedded video left padding to background container
     * 
     * @return left padding value
     */
    public int getPaddingLeft() {
        return paddingLeft;
    }

    /**
     * set embedded video top padding to background container
     * 
     * @param paddingTop
     *            embedded video top padding to background container
     */
    public void setPaddingTop(int paddingTop) {
        this.paddingTop = paddingTop;
    }

    /**
     * get embedded video top padding to background container
     * 
     * @return embedded video top padding to background container
     */
    public int getPaddingTop() {
        return paddingTop;
    }

    /**
     * set embedded video width
     * 
     * @param videoWidth
     *            embedded video width
     */
    public void setVideoWidth(int videoWidth) {
        this.videoWidth = videoWidth;
    }

    /**
     * get embedded video width
     * 
     * @return embedded video width
     */
    public int getVideoWidth() {
        return videoWidth;
    }

    /**
     * set embedded video height
     * 
     * @param videoHeight
     *            embedded video height
     */
    public void setVideoHeight(int videoHeight) {
        this.videoHeight = videoHeight;
    }

    /**
     * get embedded video height
     * 
     * @return embedded video height
     */
    public int getVideoHeight() {
        return videoHeight;
    }

    /**
     * set youtube video id
     * 
     * @param videoId
     *            youtube video id
     */
    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    /**
     * get youtube video id
     * 
     * @return youtube video id
     */
    public String getVideoId() {
        return videoId;
    }

}
