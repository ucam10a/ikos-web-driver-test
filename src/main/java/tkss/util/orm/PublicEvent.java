package tkss.util.orm;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import tkss.util.TKSSConstant;
import tkss.util.orm.dao.PublicEventDAO;

/**
 * The class is used to map to the database public level event table
 * 
 * @author ucam10a
 * 
 */
@XmlRootElement
@Entity(name="event")
@EntityListeners(value={PublicEventDAO.class})
public class PublicEvent extends PublicEntryOBJ {

    /** event title */
    @Column(name="eventWord")
    @Id
    protected String eventWord;

    /** event xml data */
    @Column(name="xml")
    protected String xml;

    /** concept map serialized xml data */
    protected String serializedXml;

    /** event description text */
    @Column(name="description")
    protected String description;

    /** event background image file path */
    @Column(name="bgImage")
    protected String bgImage;

    /**
     * event background image file's xml. It is used to store image size and
     * location in event
     */
    @Column(name="bgxml")
    protected String bgxml;

    /** event picture file path, this picture is used to show on i-pad */
    @Deprecated
    protected String eventPic;

    /** event description text with different line separator */
    @Deprecated
    protected String description1;

    public PublicEvent() {
        setEntryClass(TKSSConstant.EVENT_TYPE);
    }

    /**
     * get event title
     * 
     * @return event title
     */
    public String getEventWord() {
        return eventWord;
    }

    /**
     * set event title
     * 
     * @param eventWord
     *            event title
     * 
     */
    public void setEventWord(String eventWord) {
        this.eventWord = eventWord;
        setEntryTitle(eventWord);
        setEntryClass(TKSSConstant.EVENT_TYPE);
    }

    /**
     * set event xml
     * 
     * @param xml
     *            event xml data
     * 
     */
    public void setXml(String xml) {
        this.xml = xml;
        try {
            this.serializedXml = URLEncoder.encode(xml, TKSSConstant.ENCODING);
        } catch (UnsupportedEncodingException e) {
            // ignore
        }
    }

    /**
     * get event xml
     * 
     * @return event xml
     */
    public String getXml() {
        return xml;
    }

    /**
     * set event description text
     * 
     * @param description
     *            event description text
     * 
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * get event description text
     * 
     * @return event description text
     */
    public String getDescription() {
        return description;
    }

    /**
     * set event background image file path
     * 
     * @param bgImage
     *            event background image file path
     * 
     */
    public void setBgImage(String bgImage) {
        this.bgImage = bgImage;
    }

    /**
     * get event background image file path
     * 
     * @return event background image file path
     */
    public String getBgImage() {
        return bgImage;
    }

    /**
     * set event picture file path, this picture is used for i-pad
     * 
     * @param eventPic
     *            event picture file path
     * 
     */
    @Deprecated
    public void setEventPic(String eventPic) {
        this.eventPic = eventPic;
    }

    /**
     * get event picture file path, this picture is used for i-pad
     * 
     * @return event picture file path, this picture is used for i-pad
     */
    @Deprecated
    public String getEventPic() {
        return eventPic;
    }

    /**
     * set event description with different line separator, this description is
     * used for i-pad
     * 
     * @param description1
     *            description text
     * 
     */
    @Deprecated
    public void setDescription1(String description1) {
        this.description1 = description1;
    }

    /**
     * get event description with different line separator, this description is
     * used for i-pad
     * 
     * @return event description with different line separator, this description
     *         is used for i-pad
     */
    @Deprecated
    public String getDescription1() {
        return description1;
    }

    /**
     * set event background components xml
     * 
     * @param bgxml
     *            event background components xml
     * 
     */
    public void setBgxml(String bgxml) {
        this.bgxml = bgxml;
    }

    /**
     * get event background components xml
     * 
     * @return event background components xml
     */
    public String getBgxml() {
        return bgxml;
    }

    public String getSerializedXml() {
        return serializedXml;
    }

    public void setSerializedXml(String serializedXml) {
        this.serializedXml = serializedXml;
    }

}