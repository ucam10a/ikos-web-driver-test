package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import tkss.util.orm.dao.LocalWikiKeywordDAO;

@Entity(name="localwikikeyword")
@EntityListeners(value={LocalWikiKeywordDAO.class})
public class LocalWikiKeyword extends LocalEntryKeyword {

    @Column(name="wikiWord")
    @Id
    private String wikiWord;

    public String getWikiWord() {
        return wikiWord;
    }

    public void setWikiWord(String wikiWord) {
        this.wikiWord = wikiWord;
    }
    
}