package tkss.util.orm;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.google.gson.Gson;
import com.yung.jpa.BasicEntity;

import tkss.util.MessageUtil;
import tkss.util.TKSSConstant;
import tkss.util.TKSSUtil;
import tkss.util.orm.dao.MessageOBJDAO;

/**
 * The class is used to map the message table in database
 * 
 * @author Yung Long
 * 
 */
@Entity(name="message")
@EntityListeners(value={MessageOBJDAO.class})
public class MessageOBJ extends BasicEntity {

    public static final String RATING = "rating";
    public static final String COMMENT = "comment";
    public static final String DELETE = "delete";
    public static final String INVITATION_CO_EDIT = "invitation to co-edit";
    public static final String APPLY_TO_OPEN = "apply to open";
    public static final String APPROVE_TO_OPEN = "approve to open";
    public static final String REJECT_TO_OPEN = "reject to open";
    public static final String APPLY_TO_UPATE = "apply to update";
    public static final String APPROVE_TO_UPDATE = "approve to update";
    public static final String REJECT_TO_UPDATE = "reject to update";
    public static final String APPLY_TO_CO_EDIT = "apply to co-edit";
    public static final String APPROVE_TO_CO_EDIT = "approve to co-edit";
    public static final String REJECT_TO_CO_EDIT = "reject to co-edit";

    public static final String NOTIFICATION_TYPE_ACTION = "action";
    public static final String NOTIFICATION_TYPE_NOTIFY = "notify";

    /** entry id */
    @Column(name="entryID")
    @Id
    protected Integer entryID;

    /** entry class id */
    @Column(name="classID")
    protected Integer classID;

    /** entry title */
    protected String entryTitle;

    /** entry type */
    @Column(name="entryType")
    @Id
    protected String type;

    /** entry member user id */
    @Column(name="userID")
    @Id
    protected String userId;

    /** action user name */
    protected String actionUser;

    /** entry author name */
    protected String entryAuthor;

    /** message text */
    @Column(name="messageType")
    @Id
    protected String messageType;

    /** notification type */
    protected String notificationType;

    /** display message */
    protected String displayMessage;

    /** link url */
    protected String linkUrl;

    /** call AJAX function */
    protected String callAjax;
    
    /** json to store data */
    @Column(name="wrapJson")
    protected String json;
    
    private Locale locale;

    private String of;
    
    public MessageOBJ(Locale locale) {
        this.locale = locale;
    }
    
    /**
     * set the entry type
     * 
     * @param type
     *            entry type
     * 
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * get the entry type
     * 
     * @return the entry type
     */
    public String getType() {
        return type;
    }

    /**
     * set the message type
     * 
     * @param message
     *            message type
     * 
     */
    public void setMessageType(String messageType) {
        this.messageType = messageType;
        switch (messageType) {
        case RATING:
            displayMessage = MessageUtil.getMessage(locale, "rated.on");
            linkUrl = getLink(RATING);
            break;
        case COMMENT:
            displayMessage = MessageUtil.getMessage(locale, "comment.on");
            linkUrl = getLink(COMMENT);
            break;
        case DELETE:
            displayMessage = MessageUtil.getMessage(locale, "deleted");
            linkUrl = getLink(DELETE);
            break;
        case INVITATION_CO_EDIT:
            displayMessage = MessageUtil.getMessage(locale, "new.editor.on");
            linkUrl = getLink(INVITATION_CO_EDIT);
            break;
        case APPLY_TO_OPEN:
            displayMessage = MessageUtil.getMessage(locale, "applied.open.class.on");
            linkUrl = getApproveLink(APPLY_TO_OPEN, "applying");
            break;
        case APPROVE_TO_OPEN:
            displayMessage = MessageUtil.getMessage(locale, "approved.open.to.class.on");
            linkUrl = getLink(APPROVE_TO_OPEN);
            break;
        case REJECT_TO_OPEN:
            displayMessage = MessageUtil.getMessage(locale, "change.status.on");
            linkUrl = getLink(REJECT_TO_OPEN);
            break;
        case APPLY_TO_UPATE:
            displayMessage = MessageUtil.getMessage(locale, "apply.update.on");
            linkUrl = getApproveLink(APPLY_TO_UPATE, "open");
            break;
        case APPROVE_TO_UPDATE:
            displayMessage = MessageUtil.getMessage(locale, "approved.update.on");
            linkUrl = getLink(APPROVE_TO_UPDATE);
            break;
        case REJECT_TO_UPDATE:
            displayMessage = MessageUtil.getMessage(locale, "rollback.update.on");
            linkUrl = getLink(REJECT_TO_UPDATE);
            break;
        case APPROVE_TO_CO_EDIT:
            displayMessage = MessageUtil.getMessage(locale, "became.new.editor.on");
            linkUrl = getLink(APPROVE_TO_CO_EDIT);
            break;
        case REJECT_TO_CO_EDIT:
            displayMessage = MessageUtil.getMessage(locale, "declined.co.edit.application.on");
            linkUrl = getLink(REJECT_TO_CO_EDIT);
            break;
        case APPLY_TO_CO_EDIT:
            displayMessage = MessageUtil.getMessage(locale, "apply.co.edit.on");
            linkUrl = getLink(APPLY_TO_CO_EDIT);
            break;
        default:
            displayMessage = "";
            linkUrl = getLink(null);
        }
    }

    private String getLink(String messageType) {
        String link = "";
        if (messageType.equals(DELETE)) {
            link = "javascript:void(null);";
            this.callAjax = "clearDeleteMsg('" + entryID + "', '" + type + "');";
        } else {
            if (TKSSConstant.EVENT_TYPE.equalsIgnoreCase(type)) {
                link = "showLocalEvent?id=" + entryID + "&level=local&page=eventView";
            } else if (TKSSConstant.WIKI_TYPE.equalsIgnoreCase(type)) {
                link = "showLocalWiki?id=" + entryID + "&level=local&page=wikiView";
            } else if (TKSSConstant.CONCEPT_MAP_TYPE.equalsIgnoreCase(type)) {
                link = "showLocalConcept?id=" + entryID + "&level=local&page=conceptView";
            } else if (TKSSConstant.BOOK_TYPE.equalsIgnoreCase(type)) {
                link = "showLocalBook?id=" + entryID + "&level=local&page=bookView";
            } else {
                throw new RuntimeException("Unknown entry type: " + type);
            }
            if (messageType != null) {
                link = link + "&deleteMsgType=" + messageType;
            }
        }
        return link;
    }

    private String getApproveLink(String messageType, String publicize) {
        String link = "";
        if (TKSSConstant.EVENT_TYPE.equalsIgnoreCase(type)) {
            link = "showLocalEvent?id=" + entryID + "&level=local&page=eventCheck";
        } else if (TKSSConstant.WIKI_TYPE.equalsIgnoreCase(type)) {
            link = "showLocalWiki?id=" + entryID + "&level=local&page=wikiCheck";
        } else if (TKSSConstant.CONCEPT_MAP_TYPE.equalsIgnoreCase(type)) {
            link = "showLocalConcept?id=" + entryID + "&level=local&page=conceptCheck";
        } else {
            throw new RuntimeException("Unknown entry type");
        }
        if (publicize.equalsIgnoreCase("applying")) {
            link = link + "&publicize=applying";
        }
        return link;
    }

    /**
     * get the message type
     * 
     * @return the message type
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * set entry id of this message belong
     * 
     * @param entryID
     *            entry id of this message belong
     * 
     */
    public void setEntryID(Integer entryID) {
        this.entryID = entryID;
    }

    /**
     * get entry id of this message belong
     * 
     * @return entry id
     */
    public Integer getEntryID() {
        return entryID;
    }

    /**
     * get entry member user id
     * 
     * @return user id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * set entry member user id
     * 
     * @param userId
     *            user id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * get entry title
     * 
     * @return entry title
     */
    public String getEntryTitle() {
        return entryTitle;
    }

    /**
     * set entry title
     * 
     * @param entryTtile
     *            entry title
     */
    public void setEntryTitle(String entryTitle) {
        entryTitle = TKSSUtil.getDecodeTitle(entryTitle);
        this.entryTitle = entryTitle;
    }

    /**
     * get notification type
     * 
     * @return notification type
     */
    public String getNotificationType() {
        return notificationType;
    }

    /**
     * set notification type
     * 
     * @param notificationType
     *            notification type
     */
    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    /**
     * get display message
     * 
     * @return display message
     */
    public String getDisplayMessage() {
        return displayMessage;
    }

    /**
     * set display message
     * 
     * @param displayMessage
     *            display message
     */
    public void setDisplayMessage(String displayMessage) {
        this.displayMessage = displayMessage;
    }

    /**
     * get class id
     * 
     * @return
     */
    public Integer getClassID() {
        return classID;
    }

    /**
     * set class id
     * 
     * @param classID
     */
    public void setClassID(Integer classID) {
        this.classID = classID;
    }

    /**
     * get link url
     * 
     * @return
     */
    public String getLinkUrl() {
        return linkUrl;
    }

    /**
     * set link url
     * 
     * @param linkUrl
     */
    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    /**
     * get action user name
     * 
     * @return action user name
     */
    public String getActionUser() {
        return actionUser;
    }

    /**
     * set action user name
     * 
     * @param actionUser
     *            action user name
     */
    public void setActionUser(String actionUser) {
        this.actionUser = actionUser;
    }

    /**
     * get entry author name
     * 
     * @return entry author name
     */
    public String getEntryAuthor() {
        return entryAuthor;
    }

    /**
     * set entry author name
     * 
     * @param entryAuthor
     *            entry author name
     */
    public void setEntryAuthor(String entryAuthor) {
        this.entryAuthor = entryAuthor;
    }

    /**
     * get onclick ajax function
     * 
     * @return onclick ajax function name
     */
    public String getCallAjax() {
        return callAjax;
    }

    /**
     * set onclick ajax function
     * 
     * @param callAjax
     *            onclick ajax function
     */
    public void setCallAjax(String callAjax) {
        this.callAjax = callAjax;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
    
    /**
     * wrap entry title, action author and entry author to Json
     * 
     * @param entryTitle
     *            entry title
     * @param actionUser
     *            action user
     * @param entryAuthor
     *            entry author
     * @return warp json
     */
    public static String wrapJson(String entryTitle, String actionUser, String entryAuthor) {
        Map<String, String> gsonMap = new HashMap<String, String>();
        gsonMap.put("entryTitle", entryTitle);
        gsonMap.put("actionUser", actionUser);
        gsonMap.put("entryAuthor", entryAuthor);
        Gson gson = new Gson();
        return gson.toJson(gsonMap);
    }
    
    /**
     * unwrap Json
     * 
     * @param message
     *             message object
     * @param Json
     *             wrap Json
     */
    @SuppressWarnings("unchecked")
    public static void unwrapJson(MessageOBJ message, String Json) {
        Gson gson = new Gson();
        Map<String, String> gsonMap = new HashMap<String, String>();
        gsonMap = (Map<String, String>) gson.fromJson(Json, gsonMap.getClass());
        message.entryTitle = gsonMap.get("entryTitle");
        message.actionUser = gsonMap.get("actionUser");
        message.entryAuthor = gsonMap.get("entryAuthor");
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getOf() {
        this.of = MessageUtil.getMessage(locale, "of");
        return of;
    }

    public void setOf(String of) {
        // ignore
    }

}