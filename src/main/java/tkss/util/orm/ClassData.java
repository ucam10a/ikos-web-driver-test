package tkss.util.orm;

import java.util.List;

/**
 * This is a container to pack all of class raw data not include binary file(ex.
 * image)
 * 
 * @author Yung Long Li
 *
 */
public class ClassData {

    /** teacher */
    protected UserOBJ teacher;

    /** list of student */
    protected List<UserOBJ> students;

    /** list of event */
    protected List<EventOBJ> events;

    /** list of wiki */
    protected List<WikiOBJ> wikis;

    /** list of concept */
    protected List<ConceptOBJ> concepts;

    /** list of comment */
    protected List<CommentOBJ> comments;

    /** list of event rating */
    protected List<RatingOBJ> eventRatings;

    /** list of wiki rating */
    protected List<RatingOBJ> wikiRatings;

    /** list of concept rating */
    protected List<RatingOBJ> conceptRatings;

    /** teamMember map <key> team id <value> lsit of user id */
    protected List<TeamOBJ> teams;

    /**
     * get teacher object
     * 
     * @return teacher object
     */
    public UserOBJ getTeacher() {
        return teacher;
    }

    /**
     * set teacher object
     * 
     * @param teacher
     *            teacher object
     */
    public void setTeacher(UserOBJ teacher) {
        this.teacher = teacher;
    }

    /**
     * set student list
     * 
     * @return student list
     */
    public List<UserOBJ> getStudents() {
        return students;
    }

    /**
     * get student list
     * 
     * @param students
     *            student list
     */
    public void setStudents(List<UserOBJ> students) {
        this.students = students;
    }

    /**
     * get event list
     * 
     * @return event list
     */
    public List<EventOBJ> getEvents() {
        return events;
    }

    /**
     * set event list
     * 
     * @param events
     *            event list
     */
    public void setEvents(List<EventOBJ> events) {
        this.events = events;
    }

    /**
     * get wiki list
     * 
     * @return wiki list
     */
    public List<WikiOBJ> getWikis() {
        return wikis;
    }

    /**
     * set wiki list
     * 
     * @param wikis
     *            wiki list
     */
    public void setWikis(List<WikiOBJ> wikis) {
        this.wikis = wikis;
    }

    /**
     * get comment list
     * 
     * @return comment list
     */
    public List<CommentOBJ> getComments() {
        return comments;
    }

    /**
     * set comment list
     * 
     * @param comments
     *            comment list
     */
    public void setComments(List<CommentOBJ> comments) {
        this.comments = comments;
    }

    /**
     * get concept list
     * 
     * @return concept list
     */
    public List<ConceptOBJ> getConcepts() {
        return concepts;
    }

    /**
     * set concept list
     * 
     * @param concepts
     *            concept list
     */
    public void setConcepts(List<ConceptOBJ> concepts) {
        this.concepts = concepts;
    }

    /**
     * set event rating list
     * 
     * @return event rating list
     */
    public List<RatingOBJ> getEventRatings() {
        return eventRatings;
    }

    /**
     * get event rating list
     * 
     * @param eventRatings
     *            event rating list
     */
    public void setEventRatings(List<RatingOBJ> eventRatings) {
        this.eventRatings = eventRatings;
    }

    /**
     * get wiki rating list
     * 
     * @return wiki rating list
     */
    public List<RatingOBJ> getWikiRatings() {
        return wikiRatings;
    }

    /**
     * set wiki rating list
     * 
     * @param wikiRatings
     *            wiki rating list
     */
    public void setWikiRatings(List<RatingOBJ> wikiRatings) {
        this.wikiRatings = wikiRatings;
    }

    /**
     * get concept map rating list
     * 
     * @return concept map rating list
     */
    public List<RatingOBJ> getConceptRatings() {
        return conceptRatings;
    }

    /**
     * set concept map rating list
     * 
     * @param conceptRatings
     *            concept map rating list
     */
    public void setConceptRatings(List<RatingOBJ> conceptRatings) {
        this.conceptRatings = conceptRatings;
    }

    /**
     * get team list
     * 
     * @return team list
     */
    public List<TeamOBJ> getTeams() {
        return teams;
    }

    /**
     * set team list
     * 
     * @param teams
     *            team list
     */
    public void setTeams(List<TeamOBJ> teams) {
        this.teams = teams;
    }

}
