package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;

public class TempOBJ extends BasicEntity {

    @Column(name="id")
    @Id
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
}