package tkss.util.orm;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import tkss.util.orm.dao.BookOBJDAO;

@Entity(name="localbook")
@EntityListeners(value={BookOBJDAO.class})
public class BookOBJ extends ClassEntryOBJ {

    /** book title */
    @Column(name="bookWord")
    @Id
    protected String bookWord;
    
    protected List<BookChapterOBJ> bookChapters;
    
    public String getBookWord() {
        return bookWord;
    }

    public void setBookWord(String bookWord) {
        this.bookWord = bookWord;
        setEntryTitle(bookWord);
    }

    public List<BookChapterOBJ> getBookChapters() {
        return bookChapters;
    }

    public void setBookChapters(List<BookChapterOBJ> bookChapters) {
        this.bookChapters = bookChapters;
    }

}