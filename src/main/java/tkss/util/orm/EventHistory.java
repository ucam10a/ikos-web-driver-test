package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import tkss.util.orm.dao.EventHistoryDAO;

@Entity(name="eventhistory")
@EntityListeners(value={EventHistoryDAO.class})
public class EventHistory extends HistoryOBJ {

    @Column(name="xml")
    protected String xml;
    
    @Column(name="bgxml")
    protected String bgXml;

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public String getBgXml() {
        return bgXml;
    }

    public void setBgXml(String bgXml) {
        this.bgXml = bgXml;
    }
    
}
