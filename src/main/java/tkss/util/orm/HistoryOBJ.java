package tkss.util.orm;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;

public class HistoryOBJ extends BasicEntity {

    @Column(name="id")
    @Id
    protected Integer entryID;
    
    @Column(name="modifydate")
    @Id
    protected Timestamp modifyTsp;
    
    public Integer getEntryID() {
        return entryID;
    }

    public void setEntryID(Integer entryID) {
        this.entryID = entryID;
    }

    public Timestamp getModifyTsp() {
        return modifyTsp;
    }

    public void setModifyTsp(Timestamp modifyTsp) {
        this.modifyTsp = modifyTsp;
    }    
}
