package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;

public class LocalEntryKeyword extends BasicEntity {

    @Column(name="classID")
    @Id
    private Integer classID;
    
    @Column(name="keyword")
    @Id
    private String keyword;
    
    public Integer getClassID() {
        return classID;
    }

    public void setClassID(Integer classID) {
        this.classID = classID;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    
}