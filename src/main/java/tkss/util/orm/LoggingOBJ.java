package tkss.util.orm;

import java.sql.Timestamp;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import tkss.util.orm.dao.LoggingOBJDAO;

import com.yung.jpa.BasicEntity;

@Entity(name="logging")
@EntityListeners(value={LoggingOBJDAO.class})
public class LoggingOBJ extends BasicEntity {
    
    public static final String CLICK_EMOJI = "Click emoji";
    public static final String CLICK_ENTRY_TITLE = "Click entry title";
    public static final String CLICK_SAVE = "Click save/leave";
    public static final String CLICK_MODIFY = "Click modify";
    public static final String LOGIN = "Log in";
    public static final String LOGOUT = "Log out";
    public static final String CLICK_VIEW_COMMENT = "Click view comment";
    public static final String CLICK_SUBMIT_COMMENT = "Click submit comment";
    public static final String CLICK_MESSAGE_BOARD = "Click message board";
    public static final String CLICK_APPLY_JOIN = "Click apply join to co-edit";
    public static final String CLICK_APPLY_TO_OPEN = "Click apply to open class";
    public static final String CLICK_ADD_COAUTHOR = "Click add coauthor";
    public static final String CLICK_ACCEPT_UPDATE = "Click accept entry to update";
    public static final String CLICK_CREATE = "Click create entry";
    public static final String CLICK_CREATE_CHAPTER = "Click create chapter";
    public static final String CLICK_CHANHGE_CHAPTER = "Click change chapter";
    
    /** id */
    @Column(name="seqence")
    @Id
    private Integer seqence;
    
    /** action user */
    @Column(name="userID")
    private String user;
    
    /** class id */
    @Column(name="class")
    private Integer classId = -1;
    
    /** entry id */
    @Column(name="entryID")
    private Integer entryID = -1;
    
    /** entry type */
    @Column(name="entryType")
    private String entryType;
    
    /** entry action */
    @Column(name="entryAction")
    private String entryAction;
    
    /** action time */
    @Column(name="time")
    private Timestamp time;
    
    /** action content, JSON type */
    @Column(name="actionContent")
    private String actionContent;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Integer getEntryID() {
        return entryID;
    }

    public void setEntryID(Integer entryID) {
        this.entryID = entryID;
    }

    public String getEntryType() {
        return entryType;
    }

    public void setEntryType(String entryType) {
        this.entryType = entryType;
    }

    public String getEntryAction() {
        return entryAction;
    }

    public void setEntryAction(String entryAction) {
        this.entryAction = entryAction;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public String getActionContent() {
        return actionContent;
    }

    public void setActionContent(String actionContent) {
        this.actionContent = actionContent;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getSeqence() {
        return seqence;
    }

    public void setSeqence(Integer seqence) {
        this.seqence = seqence;
    }

    public static String getContent(Map<String, Object> dataMap) {
        if (dataMap == null || dataMap.size() == 0) {
            return "";
        } else {
            StringBuilder sb = new StringBuilder();
            for (String key : dataMap.keySet()) {
                sb.append(key + ":" + dataMap.get(key));
            }
            return sb.toString();
        }
    }
    
    
}
