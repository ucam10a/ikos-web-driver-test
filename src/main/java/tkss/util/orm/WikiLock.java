package tkss.util.orm;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import tkss.util.orm.dao.WikiLockDAO;

@Entity(name="wikilock")
@EntityListeners(value={WikiLockDAO.class})
public class WikiLock extends LockOBJ {

}