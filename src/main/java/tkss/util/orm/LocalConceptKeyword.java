package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import tkss.util.orm.dao.LocalConceptKeywordDAO;

@Entity(name="localconceptkeyword")
@EntityListeners(value={LocalConceptKeywordDAO.class})
public class LocalConceptKeyword extends LocalEntryKeyword{

    @Column(name="conceptWord")
    @Id
    private String conceptWord;

    public String getConceptWord() {
        return conceptWord;
    }

    public void setConceptWord(String conceptWord) {
        this.conceptWord = conceptWord;
    }
    
}