package tkss.util.orm;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import tkss.util.orm.dao.WikiRatingDAO;

@Entity(name="localwikirating")
@EntityListeners(value={WikiRatingDAO.class})
public class WikiRating extends RatingOBJ {

}