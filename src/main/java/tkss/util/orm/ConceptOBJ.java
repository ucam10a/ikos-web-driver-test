package tkss.util.orm;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
//import java.util.ArrayList;
//import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
//import javax.persistence.FetchType;
import javax.persistence.Id;
//import javax.persistence.JoinColumns;
//import javax.persistence.JoinColumn;
//import javax.persistence.OneToMany;

import tkss.util.TKSSConstant;
import tkss.util.orm.dao.ConceptOBJDAO;

/**
 * The class is used to map to the database class level concept map table
 * 
 * @author Yung Long
 * 
 */
@Entity(name="localconcept")
@EntityListeners(value={ConceptOBJDAO.class})
public class ConceptOBJ extends ClassEntryOBJ {

    /** concept map title */
    @Column(name="conceptWord")
    @Id
    protected String conceptWord;

    /** concept map xml data */
    @Column(name="xml")
    protected String xml;

    /** concept map serialized xml data */
    protected String serializedXml;

    /** concept map picture file path */
    @Deprecated
    protected String conceptPic;
    
    //@OneToMany(fetch=FetchType.LAZY)
    //@JoinColumns(
    //    @JoinColumn(table="localconceptrating", name="id", referencedColumnName="id")
    //)
    //protected List<RatingOBJ> ratings = new ArrayList<RatingOBJ>();

    public ConceptOBJ() {
        setEntryClass(TKSSConstant.CONCEPT_MAP_TYPE);
    }

    /**
     * get the concept map title
     * 
     * @return concept map title
     */
    public String getConceptWord() {
        return conceptWord;
    }

    /**
     * set the concept map title
     * 
     */
    public void setConceptWord(String conceptWord) {
        this.conceptWord = conceptWord;
        setEntryTitle(conceptWord);
    }

    /**
     * get the concept map xml
     * 
     * @return concept map xml data
     */
    public String getXml() {
        return xml;
    }

    /**
     * set the concept map xml data
     * 
     */
    public void setXml(String xml) {
        this.xml = xml;
        try {
            this.serializedXml = URLEncoder.encode(xml, TKSSConstant.ENCODING);
        } catch (UnsupportedEncodingException e) {
            // ignore
        }
    }

    /**
     * get the concept map picture file path
     * 
     * @param conceptPic
     *            concept map picture file path
     */
    @Deprecated
    public void setConceptPic(String conceptPic) {
        this.conceptPic = conceptPic;
    }

    /**
     * set the concept map picture file path
     * 
     */
    @Deprecated
    public String getConceptPic() {
        return conceptPic;
    }

    /**
     * get serialized xml
     * 
     * @return serialized xml
     */
    public String getSerializedXml() {
        return serializedXml;
    }

    /**
     * set serialized xml
     * 
     * @param serializedXml
     *            serialized xml
     */
    public void setSerializedXml(String serializedXml) {
        // ignore
    }

    //public List<RatingOBJ> getRatings() {
    //    return ratings;
    //}

    //public void setRatings(List<RatingOBJ> ratings) {
    //    this.ratings = ratings;
    //}

}