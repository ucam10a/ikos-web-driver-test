package tkss.util.orm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import tkss.util.orm.dao.LocalBookKeywordDAO;

@Entity(name="localbookkeyword")
@EntityListeners(value={LocalBookKeywordDAO.class})
public class LocalBookKeyword extends LocalEntryKeyword {

    @Column(name="bookWord")
    @Id
    private String bookWord;

    public String getBookWord() {
        return bookWord;
    }

    public void setBookWord(String bookWord) {
        this.bookWord = bookWord;
    }

}