package tkss.util.orm;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import tkss.util.orm.dao.ConceptTempDAO;

@Entity(name="concepttemp")
@EntityListeners(value={ConceptTempDAO.class})
public class ConceptTemp extends TempOBJ {

}