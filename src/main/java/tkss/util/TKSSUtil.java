package tkss.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import tkss.util.orm.EntryOBJ;
import tkss.util.orm.LinkOBJ;
import tkss.util.orm.PublicConcept;
import tkss.util.orm.PublicEvent;
import tkss.util.orm.PublicWiki;

import com.google.gson.Gson;
import com.yung.web.test.FileUtil;

/**
 * The common function for all type of entry
 * 
 * @author Yung Long
 * 
 */
public class TKSSUtil {

    /** the stop words */
    protected static TreeSet<String> stopWords = new TreeSet<String>();

    private static Logger logger = Logger.getLogger(TKSSUtil.class.getName());

    private static CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder();

    /**
     * concatenate the keyword to a string with ","
     * 
     * @param words
     *            a set of word
     * @return a string of all keyword
     */
    public static String concatenateWord(Collection<String> words) {

        // Concatenate keyword
        StringBuffer keywordBuf = new StringBuffer("");
        boolean start = true;
        for (String word : words) {
            if (start) {
                keywordBuf.append(word.trim());
                start = false;
            } else {
                if (!(word.equals(""))) {
                    keywordBuf.append(", " + word.trim());
                }
            }
        }
        return keywordBuf.toString();
    }

    /**
     * concatenate the keyword to a string with ","
     * 
     * @param keywords
     *            a set of keyword
     * @return a string of all keyword
     */
    public String concatenateKeyword(Collection<String> keywords) {

        // Concatenate keyword
        StringBuffer keywordBuf = new StringBuffer("");
        boolean start = true;
        for (String word : keywords) {
            if (start) {
                keywordBuf.append(word.trim());
                start = false;
            } else {
                if (!(word.equals(""))) {
                    keywordBuf.append(", " + word.trim());
                }
            }
        }
        return keywordBuf.toString();
    }

    /**
     * tokenize the word into SQL Query words list. ex, 'word1', 'word2',
     * 'word3'
     * 
     * @param words
     *            the list of words
     * @return the string of words
     */
    public static String tokenize(Collection<String> words) {
        String result = "";
        boolean start = true;
        for (String word : words) {
            if (word.contains("'")) word = word.replace("'", "\\'");
            if (start == true) {
                result = word.trim();
                start = false;
            } else {
                result = result + ", " + word.trim();
            }
        }
        return result;
    }

    /**
     * tokenize the word into SQL Query words list. ex, 'word1', 'word2',
     * 'word3'
     * 
     * @param words
     *            the list of words
     * @return the string of words
     */
    public static String tokenize(String[] words) {
        String result = "";
        boolean start = true;
        for (String word : words) {
            if (word.contains("'")) word = word.replace("'", "\\'");
            if (start == true) {
                result = "'" + word.trim() + "'";
                start = false;
            } else {
                result = result + ", '" + word.trim() + "'";
            }
        }
        return result;
    }

    /**
     * check the set of keywords is oversize
     * 
     * @param keywords
     *            a set of keywords
     * @return any keyword is oversize
     */
    public Set<String> checkKeywordLength(Set<String> keywords) {
        TreeSet<String> oversizeKeywords = new TreeSet<String>();
        for (String word : keywords) {
            if (word.length() > EntryOBJ.KEYWORD_SIZE) oversizeKeywords.add(word);
        }
        return oversizeKeywords;
    }
    
    /**
     * check email format
     * 
     * @param email
     *            input email
     * @return validate or not
     */
    public static boolean isEmailValid(String email) {
        boolean isValid = false;
        // Initialize reg ex for email.
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        // Make the comparison case-insensitive.
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    /**
     * convert the json object to PublicEvent object
     * 
     * @param json
     *            input json object
     * @return PublicEvent object
     * @throws JSONException
     */
    public static PublicEvent parseJSONEvent(String json) {

        Gson gson = new Gson();
        PublicEvent event = gson.fromJson(json, PublicEvent.class);
        return event;

    }

    /**
     * convert the json object to PublicWiki object
     * 
     * @param json
     *            input json object
     * @return PublicWiki object
     * @throws JSONException
     */
    public static PublicWiki parseJSONWiki(String json) {

        Gson gson = new Gson();
        PublicWiki wiki = gson.fromJson(json, PublicWiki.class);
        return wiki;

    }

    /**
     * convert the json object to PublicConceot object
     * 
     * @param json
     *            input json object
     * @return PublicConcept object
     * @throws JSONException
     */
    public static PublicConcept parseJSONConcept(String json) {

        Gson gson = new Gson();
        PublicConcept concept = gson.fromJson(json, PublicConcept.class);
        return concept;

    }

    /**
     * copy file
     * 
     * @param srcFile
     *            source file
     * @param desFile
     *            target file path
     * @throws IOException
     * @throws URISyntaxException
     */
    public void fileCopy(File srcFile, String desFile) throws IOException, URISyntaxException {
        InputStream in = new FileInputStream(srcFile);
        File file = null;
        if (desFile.startsWith("file:")) {
            file = FileUtil.getFile(desFile);
        } else {
            file = FileUtil.getFile(desFile);
        }
        OutputStream out = new FileOutputStream(file);
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    /**
     * convert the list link object to a string of all titles
     * 
     * @return all titles of the list link
     */
    public static String LinkToString(List<LinkOBJ> links) {
        String result = "";
        boolean start = true;
        if (links != null) {
            for (LinkOBJ link : links) {
                if (start) {
                    result = result + link.getWord();
                    start = false;
                } else {
                    result = result + ", " + link.getWord();
                }
            }
        }
        return result;
    }

    /**
     * get sort attribute from session
     * 
     * @return sort attribute
     */
    public static String getDefaultSortAttribute(String entryType) {
        String sortAttr = null;
        if (entryType.equalsIgnoreCase(TKSSConstant.EVENT_TYPE)) {
            sortAttr = "eventWord";
        } else if (entryType.equalsIgnoreCase(TKSSConstant.WIKI_TYPE)) {
            sortAttr = "wikiWord";
        } else if (entryType.equalsIgnoreCase(TKSSConstant.CONCEPT_MAP_TYPE)) {
            sortAttr = "conceptWord";
        }
        return sortAttr;
    }

    /**
     * generate MD5 hash text
     * 
     * @param plaintext
     *            plain text
     * @param secreteText
     *            secrete text for protecting data
     * @return hash text
     * @throws NoSuchAlgorithmException
     */
    public static String getMD5HashUserID(String plaintext, String secreteText) throws NoSuchAlgorithmException {
        return getMD5(plaintext + secreteText).substring(0, 25);
    }
    
    /**
     * get MD5
     * 
     * @param input
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String getMD5(String input) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(input.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            String hashtext = bigInt.toString(16);
            // Now we need to zero pad it if you actually want the full 32 chars.
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * write text to a temp file
     * 
     * @param inputText
     * @return file
     */
    public static File writeTextToFile(String inputText) {
        BufferedWriter writer = null;
        File logFile = null;
        try {
            // create a temporary file
            String timeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            logFile = new File(timeLog);

            writer = new BufferedWriter(new FileWriter(logFile));
            writer.write(inputText);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.toString(), e);
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }
        return logFile;
    }

    /**
     * read text file to String
     * 
     * @param file
     *            text file
     * @return string
     * @throws IOException
     */
    public static String readFile(File file) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }

    /**
     * check file exist
     * 
     * @param fileURI
     *            file URI
     * @return true or false
     * @throws URISyntaxException
     * @throws UnsupportedEncodingException
     * 
     */
    public boolean checkFileExist(String fileURI) throws URISyntaxException, UnsupportedEncodingException {
        return (FileUtil.getFile(fileURI)).exists();
    }

    /**
     * delete file in folder if file contain filter string
     * 
     * @param filterStr
     *            filter string
     * @param folderURI
     *            folder URI
     * @throws URISyntaxException
     * @throws UnsupportedEncodingException
     */
    public void deleteFiles(final String filterStr, String folderPath) throws URISyntaxException, UnsupportedEncodingException {
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File fileDir, String name) {
                return (name.contains(filterStr));
            }
        };
        File imageDir = FileUtil.getFile(folderPath);
        File[] images = imageDir.listFiles(filter);
        if (images != null) {
            for (File img : images) {
                img.delete();
            }
        }
    }

    /**
     * delete the files in certain directory by filter
     * 
     * @param filterStr
     *            filter string for files
     * @param dir
     *            directory path
     * @throws URISyntaxException
     * @throws UnsupportedEncodingException
     */
    public void deleteFileByFilter(final String filterStr, String dir, List<String> keepFileList) throws URISyntaxException, UnsupportedEncodingException {
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File fileDir, String name) {
                return (name.contains(filterStr));
            }
        };
        File fileDir = FileUtil.getFile(dir);
        File[] files = fileDir.listFiles(filter);
        if (files != null && files.length > 0) {
            for (File f : files) {
                if (!keepFileList.contains(f.getName())) {
                    // TODO: keep for history
                    //f.delete();
                }
            }
        }
    }

    /**
     * encode string to base64
     * 
     * @param originalText
     * @return base64 text
     * @throws IOException
     */
    public static String encodeBase64(String originalText) throws IOException {
        return compress(originalText);
    }

    /**
     * encode string to base64
     * 
     * @param originalText
     * @return base64 text
     * @throws IOException
     */
    public static String decodeBase64(String base64) throws IOException {
        if (base64.equals("")) return "";
        return decompress(base64);
    }

    /**
     * check string contains only ASCII
     * 
     * @param checkString
     * @return boolean is ASCII
     */
    public static boolean isASCII(String checkString) {
        return asciiEncoder.canEncode(checkString);
    }

    public static String compress(String srcTxt) throws IOException {
        ByteArrayOutputStream rstBao = new ByteArrayOutputStream();
        GZIPOutputStream zos = new GZIPOutputStream(rstBao);
        zos.write(srcTxt.getBytes(TKSSConstant.ENCODING));
        IOUtils.closeQuietly(zos);
        byte[] bytes = rstBao.toByteArray();
        return Base64.encodeBase64String(bytes);
    }

    public static String decompress(String zippedBase64Str) throws IOException {
        String result = null;
        byte[] bytes = Base64.decodeBase64(zippedBase64Str);
        GZIPInputStream zi = null;
        try {
            zi = new GZIPInputStream(new ByteArrayInputStream(bytes));
            StringWriter writer = new StringWriter();
            IOUtils.copy(zi, writer, TKSSConstant.ENCODING);
            result = writer.toString();
        } finally {
            IOUtils.closeQuietly(zi);
        }
        return result;
    }

    public static String compressLZBase64(String input) throws IOException {
        return LZString.compressToEncodedURIComponent(input);
    }
    
    public static String decompressLZBase64(String input) throws IOException {
        return LZString.decompressFromEncodedURIComponent(input);
    }
    
    /**
     * concatenate id
     * 
     * @param ids
     *            id list
     * @return id with delimiter ","
     */
    public static String concatenateId(List<Long> ids) {
        // Concatenate keyword
        StringBuffer keywordBuf = new StringBuffer("");
        boolean start = true;
        for (Long id : ids) {
            if (start) {
                keywordBuf.append(id.toString());
                start = false;
            } else {
                keywordBuf.append(", " + id.toString());
            }
        }
        return keywordBuf.toString();
    }

    /**
     * replace word
     * 
     * @param originalText
     * @param key
     * @param replaceValue
     * @return
     */
    public static String replaceAll(String originalText, String key, String replaceValue){
        if (originalText == null || originalText.length() == 0 || key == null || key.length() == 0 ||
                replaceValue == null) return originalText;
        int fromIndex = 0, idx, len = key.length();
        StringBuffer sb = new StringBuffer();
        boolean found = false;
        while ((idx = originalText.indexOf(key, fromIndex)) != -1) {
            if (!found) found = true;
            sb.append(originalText.substring(fromIndex, idx));
            sb.append(replaceValue);
            fromIndex = idx + len;
        }
        if (!found) return originalText;
        
        if (fromIndex < originalText.length()) {
            sb.append(originalText.substring(fromIndex));
        }
        return sb.toString();
    }
    
    /**
     * create error message 
     * 
     * @param src source message
     * @param replaceTokens
     * @return final message
     */
    public static String createMesssage(String srcMsg, String... replaceTokens) {
        if (replaceTokens == null || replaceTokens.length == 0) {
            return srcMsg;
        }
        for (int i = 0; i < replaceTokens.length; i++) {
            String token = replaceTokens[i];
            if (token == null) {
                token = "";
            }
            srcMsg = replaceAll(srcMsg, "{" + (i + 1) + "}", token);
        }
        return srcMsg;
    }
    
    /**
     * test
     * 
     * @param args
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static void main(String[] args) throws Exception {
        // String DBURL =
        // "jdbc:mysql://localhost/ikos?useUnicode=true&characterEncoding=utf-8";
        // Class.forName("com.mysql.jdbc.Driver");
        // Connection conn = DriverManager.getConnection(DBURL, "root", "****");
        
        String test = "ttteeee11111eeeee";
        System.out.println(test);
        
        String test1 = compressLZBase64(test);
        String test2 = compress(test);
        
        System.out.println(test1);
        System.out.println(test2);
        
        System.out.println(decompressLZBase64(test1));
        System.out.println(decompress(test2));
        
        
        
    }

    public static String getDecodeTitle(String title) {
        if (title != null && title.startsWith(TKSSConstant.LZSTRING_PREFIX)) {
            String temp = title.substring(TKSSConstant.LZSTRING_PREFIX.length(), title.length());
            title = LZString.decompressFromEncodedURIComponent(temp);
        }
        return title;
    }
    
    public static String getEncodeTitle(String title) {
        if (isASCII(title)) {
            return title;
        } else {
            return TKSSConstant.LZSTRING_PREFIX + LZString.compressToEncodedURIComponent(title);
        }
    }

}