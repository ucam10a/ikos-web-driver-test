package com.yung.web.test.ikos;

import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.yung.web.test.App;

import tkss.util.MessageUtil;
import tkss.util.orm.ClassOBJ;
import tkss.util.orm.UserOBJ;

public class ClassTest extends IkosScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, App app) {
        
        try {
        
        	HashMap<String, Object> param = app.getTestParam().getTestParams();
            UserOBJ teacher = (UserOBJ) param.get("teacher");
            
            ClassOBJ test_class = (ClassOBJ) param.get("test_class");
            ClassOBJ test_class2 = (ClassOBJ) param.get("test_class2");
            
            UserOBJ student1 = (UserOBJ) param.get("student1");
            UserOBJ student2 = (UserOBJ) param.get("student2");
            UserOBJ student3 = (UserOBJ) param.get("student3");
            
            login(driver, teacher);
            
            createClass(app, driver, test_class);
            logMessage(app, "create class: " + test_class.getAlias() + " successfully!");
            
            createClass(app, driver, test_class2);
            logMessage(app, "create class: " + test_class2.getAlias() + " successfully!");
            
            logout(driver);
            
            login(driver, student1);
            register(driver, student1, test_class, teacher);
            logMessage(app, "student: " + student1.getUserID() + " register class: " + test_class.getField() + "_" + test_class.getAlias() + " successfully!");
            logout(driver);
            
            login(driver, student2);
            register(driver, student2, test_class, teacher);
            logMessage(app, "student: " + student2.getUserID() + " register class: " + test_class.getField() + "_" + test_class.getAlias() + " successfully!");
            logout(driver);
            
            login(driver, student3);
            register(driver, student3, test_class, teacher);
            logMessage(app, "student: " + student3.getUserID() + " register class: " + test_class.getField() + "_" + test_class.getAlias() + " successfully!");
            logout(driver);
            
        } catch (Exception e) {
            logMessage(app, e.toString());
            e.printStackTrace();
            setSTOP(true);
            takeScreenShot(driver);
            driver.quit();
        }
        
    }
    
    public static void register(WebDriver driver, UserOBJ student1, ClassOBJ classObj, UserOBJ teacher) throws InterruptedException {
        WebElement addClass = driver.findElement(By.xpath("//img[@title='register a class']"));
        addClass.click();
        WebElement inputCode = driver.findElement(By.xpath("//input[@id='inputCode-myPage']"));
        inputCode.sendKeys(classObj.getClassCode());
        WebElement submit = driver.findElement(By.xpath("//input[@id='register-submit-myPage']"));
        waitForAlert(driver, submit, classObj.getField() + "_" + classObj.getAlias(), teacher.getFirstName(), teacher.getLastName());
    }

    private void createClass(App app, WebDriver driver, ClassOBJ classObj) throws InterruptedException {
        
        driver.findElement(By.xpath("//a[@id='showMyClass']")).click();
        waitForElement(driver, "//img[@src='images/add.gif']", 30);
        driver.findElement(By.xpath("//img[@src='images/add.gif']")).click();
        waitForElement(driver, "//input[@id='className-myPage']", 30);
        WebElement input = driver.findElement(By.xpath("//input[@id='className-myPage']"));
        input.sendKeys(classObj.getAlias());
        Thread.sleep(500);
        input = driver.findElement(By.xpath("//select[@id='classField-myPage']//option[@value='" + classObj.getField() + "']"));
        input.click();
        Thread.sleep(500);
        driver.findElement(By.xpath("//input[@id='createClass-submit-myPage']")).click();
        Thread.sleep(1000);
        waitForText(driver, 60, MessageUtil.getMessage(app.getTestLocale(), "success.msg"));
        // get class code
        driver.findElement(By.xpath("//a[@id='showMyClass']")).click();
        waitForElement(driver, "//span[@id='" + classObj.getField() + "_" + classObj.getAlias() + "-code-myPage']", 30);
        String classCode = driver.findElement(By.xpath("//span[@id='" + classObj.getField() + "_" + classObj.getAlias() + "-code-myPage']")).getText();
        classObj.setClassCode(classCode);
        
    }

    @Override
    public void check(WebDriver driver, Properties prop, App app) {
        // TODO Auto-generated method stub
        
    }
    
}