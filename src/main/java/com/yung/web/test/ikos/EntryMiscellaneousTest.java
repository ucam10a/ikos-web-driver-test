package com.yung.web.test.ikos;

import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.yung.web.test.App;

import tkss.util.MessageUtil;
import tkss.util.orm.ClassEntryOBJ;
import tkss.util.orm.ClassOBJ;
import tkss.util.orm.CommentOBJ;
import tkss.util.orm.ConceptOBJ;
import tkss.util.orm.EventOBJ;
import tkss.util.orm.UserOBJ;
import tkss.util.orm.WikiOBJ;

public class EntryMiscellaneousTest extends IkosScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, App app) {
        
        try {
            
        	HashMap<String, Object> params = app.getTestParam().getTestParams();
            UserOBJ student1 = (UserOBJ) params.get("student1");
            EventOBJ event1 = (EventOBJ) params.get("event1");
            UserOBJ student2 = (UserOBJ) params.get("student2");
            WikiOBJ wiki1 = (WikiOBJ) params.get("wiki1");
            UserOBJ student3 = (UserOBJ) params.get("student3");
            ConceptOBJ concept1 = (ConceptOBJ) params.get("concept1");
            
            ClassOBJ test_class = (ClassOBJ) params.get("test_class");
            
            CommentOBJ comment = (CommentOBJ) params.get("comment");
            
            // event
            leftMessage(app, driver, student3, event1, test_class, comment.getComments());
            logMessage(app, "left message " + event1.getEntryClass() + ": " + event1.getEntryTitle() + " successfully!");
            rating(app, driver, student3, event1, test_class, 3);
            logMessage(app, "rate score " + event1.getEntryClass() + ": " + event1.getEntryTitle() + " successfully!");
            
            // wiki
            leftMessage(app, driver, student1, wiki1, test_class, comment.getComments());
            logMessage(app, "left message " + wiki1.getEntryClass() + ": " + wiki1.getEntryTitle() + " successfully!");
            rating(app, driver, student1, wiki1, test_class, 4);
            logMessage(app, "rate score " + wiki1.getEntryClass() + ": " + wiki1.getEntryTitle() + " successfully!");
            
            // concept
            leftMessage(app, driver, student2, concept1, test_class, comment.getComments());
            logMessage(app, "left message " + concept1.getEntryClass() + ": " + concept1.getEntryTitle() + " successfully!");
            rating(app, driver, student2, concept1, test_class, 5);
            logMessage(app, "rate score " + concept1.getEntryClass() + ": " + concept1.getEntryTitle() + " successfully!");
            
        } catch (Exception e) {
            logMessage(app, e.toString());
            e.printStackTrace();
            setSTOP(true);
            takeScreenShot(driver);
            driver.quit();
        }
        
    }

    private void rating(App app, WebDriver driver, UserOBJ student, ClassEntryOBJ entry, ClassOBJ test_class, int rating) throws Exception {
        login(driver, student);
        selectClass(driver, test_class);
        selectEntry(driver, entry, app);
        driver.findElement(By.xpath("//div[@id='tab1Row']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//a[@id='yourRating" + rating + "']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//a[@id='rating-submit']")).click();
        Thread.sleep(2000);
        String text = driver.findElement(By.xpath("//div[@id='rating" + rating + "-cnt']")).getText();
        if (!text.contains("1")) {
            throw new Exception("rating fail!");
        }
        logout(driver);
    }

    private void leftMessage(App app, WebDriver driver, UserOBJ student, ClassEntryOBJ entry, ClassOBJ test_class, String message) throws Exception {
        login(driver, student);
        selectClass(driver, test_class);
        selectEntry(driver, entry, app);
        driver.findElement(By.xpath("//div[@id='tab2Row']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//a[@id='left-comment']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//div[@id='tab2Wait']")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//textarea[@id='leftText-collaborationTab']")).sendKeys(message);
        Thread.sleep(1000);
        String alert = MessageUtil.getMessage(app.getTestLocale(), "leave.comment.success");
        waitForAlert(driver, driver.findElement(By.xpath("//input[@id='messageSubmit-collaborationTab']")), alert);
        String text = driver.findElement(By.xpath("//div[@id='commentDiv']")).getText();
        if (!text.contains(message)) {
            throw new Exception("message: " + message + " not found!");
        }
        logout(driver);
    }

    @Override
    public void check(WebDriver driver, Properties prop, App app) {
        // TODO Auto-generated method stub
        
    }

}