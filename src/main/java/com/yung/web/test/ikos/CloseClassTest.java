package com.yung.web.test.ikos;

import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.yung.web.test.App;

import tkss.util.MessageUtil;
import tkss.util.orm.ClassOBJ;
import tkss.util.orm.UserOBJ;

public class CloseClassTest extends IkosScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, App app) {
        
        try {
            
        	HashMap<String, Object> params = app.getTestParam().getTestParams();
            UserOBJ teacher = (UserOBJ) params.get("teacher");
            
            ClassOBJ test_class = (ClassOBJ) params.get("test_class2");
            
            login(driver, teacher);
            waitForAlert(driver, driver.findElement(By.xpath("//img[@id='" + test_class.getClassID() + "-closeClassImg']")), test_class.getAlias(), MessageUtil.getMessage(app.getTestLocale(), "close.class.confirm1"), MessageUtil.getMessage(app.getTestLocale(), "close.class.confirm2"));
            Thread.sleep(3000);
            List<WebElement> elements = driver.findElements(By.xpath("//td[@class='myClass']"));
            for (WebElement ele : elements) {
                String text = ele.getText();
                if (text.contains(test_class.getAlias())) {
                    throw new Exception("close class fail!");
                }
            }
            logMessage(app, "CloseClassTest test successfully!");
            
        } catch (Exception e) {
            logMessage(app, e.toString());
            e.printStackTrace();
            setSTOP(true);
            takeScreenShot(driver);
            driver.quit();
        }
        
    }

    @Override
    public void check(WebDriver driver, Properties prop, App app) {
        // TODO Auto-generated method stub
        
    }

}