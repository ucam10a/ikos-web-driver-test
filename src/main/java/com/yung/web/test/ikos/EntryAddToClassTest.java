package com.yung.web.test.ikos;

import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.yung.web.test.App;

import tkss.util.MessageUtil;
import tkss.util.orm.ClassEntryOBJ;
import tkss.util.orm.ClassOBJ;
import tkss.util.orm.ConceptOBJ;
import tkss.util.orm.EventOBJ;
import tkss.util.orm.UserOBJ;
import tkss.util.orm.WikiOBJ;

public class EntryAddToClassTest extends IkosScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, App app) {
        
        try {
            
        	HashMap<String, Object> params = app.getTestParam().getTestParams();
            EventOBJ event1 = (EventOBJ) params.get("event1");
            WikiOBJ wiki1 = (WikiOBJ) params.get("wiki1");
            ConceptOBJ concept1 = (ConceptOBJ) params.get("concept1");
            
            UserOBJ teacher = (UserOBJ) params.get("teacher");
            
            ClassOBJ test_class2 = (ClassOBJ) params.get("test_class2");
            
            // event
            addToClass(app, driver, test_class2, event1, teacher);
            logMessage(app, "add entry " + event1.getEntryClass() + ": " + event1.getEntryTitle() + " to class successfully!");
            
            // wiki
            addToClass(app, driver, test_class2, wiki1, teacher);
            logMessage(app, "add entry " + wiki1.getEntryClass() + ": " + wiki1.getEntryTitle() + " to class successfully!");
            
            // concept
            addToClass(app, driver, test_class2, concept1, teacher);
            logMessage(app, "add entry " + concept1.getEntryClass() + ": " + concept1.getEntryTitle() + " to class successfully!");
            
            //confirm
            confirmEntry(app, driver, teacher, test_class2, event1);
            confirmEntry(app, driver, teacher, test_class2, wiki1);
            confirmEntry(app, driver, teacher, test_class2, concept1);
            
        } catch (Exception e) {
            logMessage(app, e.toString());
            e.printStackTrace();
            setSTOP(true);
            takeScreenShot(driver);
            driver.quit();
        }
        
    }

    private void confirmEntry(App app, WebDriver driver, UserOBJ teacher, ClassOBJ test_class, ClassEntryOBJ entry) throws Exception {
        login(driver, teacher);
        selectClass(driver, test_class);
        selectMainEntryPage(driver, entry, app);
        if ("event".equalsIgnoreCase(entry.getEntryClass())) {
            String myEntry = MessageUtil.getMessage(app.getTestLocale(), "my.pic.tag");
            waitForText(driver, 30, myEntry, entry.getEntryTitle());
        } else if ("wiki".equalsIgnoreCase(entry.getEntryClass())) {
            String myEntry = MessageUtil.getMessage(app.getTestLocale(), "my.wiki");
            waitForText(driver, 30, myEntry, entry.getEntryTitle());
        } else if ("concept".equalsIgnoreCase(entry.getEntryClass())) {
            String myEntry = MessageUtil.getMessage(app.getTestLocale(), "my.concept.map");
            waitForText(driver, 30, myEntry, entry.getEntryTitle());
        } else {
            throw new Exception("Unknow type: " + entry.getEntryClass());
        }
        logout(driver);
    }

    private void addToClass(App app, WebDriver driver, ClassOBJ test_class, ClassEntryOBJ entry, UserOBJ teacher) throws Exception {
        login(driver, teacher);
        selectClass(driver, test_class);
        selectMainEntryPage(driver, entry, app);
        clickElement(driver, driver.findElement(By.xpath("//input[@name='isPublic']")));
        Thread.sleep(1000);
        driver.findElement(By.xpath("//input[@id='public-tags']")).sendKeys(entry.getEntryTitle());
        Thread.sleep(1000);
        clickElement(driver, driver.findElement(By.xpath("//input[@id='public-search-submit']")));
        Thread.sleep(1000);
        if ("event".equalsIgnoreCase(entry.getEntryClass())) {
            tabClick(driver, "//div[@id='tab1Wait']");
            Thread.sleep(1000);
            clickElement(driver, driver.findElement(By.xpath("//a[@id='event-" + entry.getEntryTitle() + "-" + test_class.getField() + "-publicSearch']")));
            waitForText(driver, 30, "PicTag title", entry.getEntryTitle());
            driver.findElement(By.xpath("//a[@id='addToClass-eventView']")).click();
        } else if ("wiki".equalsIgnoreCase(entry.getEntryClass())) {
            tabClick(driver, "//div[@id='tab2Wait']");
            Thread.sleep(1000);
            clickElement(driver, driver.findElement(By.xpath("//a[@id='wiki-" + entry.getEntryTitle() + "-" + test_class.getField() + "-publicSearch']")));
            waitForText(driver, 30, "Wiki title", entry.getEntryTitle());
            clickElement(driver, driver.findElement(By.xpath("//a[@id='addToClass-wikiView']")));
        } else if ("concept".equalsIgnoreCase(entry.getEntryClass())) {
            tabClick(driver, "//div[@id='tab3Wait']");
            Thread.sleep(1000);
            clickElement(driver, driver.findElement(By.xpath("//a[@id='concept-" + entry.getEntryTitle() + "-" + test_class.getField() + "-publicSearch']")));
            waitForText(driver, 30, "Concept Map title", entry.getEntryTitle());
            clickElement(driver, driver.findElement(By.xpath("//a[@id='addToClass-conceptView']")));
        } else {
            throw new Exception("Unknow type: " + entry.getEntryClass());
        }
        Thread.sleep(3000);
        clickElement(driver, driver.findElement(By.xpath("//input[@id='" + test_class.getClassID() + "-class']")));
        Thread.sleep(1000);
        clickElement(driver, driver.findElement(By.xpath("//input[@id='addToClassSubmit-class']")));
        // TODO:
        
        
        
        waitForText(driver, 30, "Add to", "successfully", test_class.getAlias());
        logout(driver);
    }

    @Override
    public void check(WebDriver driver, Properties prop, App app) {
        // TODO Auto-generated method stub
        
    }

}