package com.yung.web.test.ikos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yung.web.test.TestParam;

import tkss.util.LZString;
import tkss.util.TypeConverter;
import tkss.util.orm.BookChapterOBJ;
import tkss.util.orm.BookOBJ;
import tkss.util.orm.ClassEntryOBJ;
import tkss.util.orm.ClassOBJ;
import tkss.util.orm.CommentOBJ;
import tkss.util.orm.ConceptOBJ;
import tkss.util.orm.EventOBJ;
import tkss.util.orm.PublicEntryOBJ;
import tkss.util.orm.StatusOBJ;
import tkss.util.orm.UserOBJ;
import tkss.util.orm.WikiOBJ;

/**
 * iKOS test values
 * 
 * @author Yung Long Li
 *
 */
public class TestValue extends TestParam {
    
    private static final Logger logger = LoggerFactory.getLogger(TestValue.class);

    private static final String LZ_CH_EVENT_TITLE = "mjqnEnCMQ";
    private static final String LZ_CH_WIKI1_TITLE = "rW*i-1CMQ";
    private static final String LZ_CH_WIKI2_TITLE = "rW*i-1BMQ";
    private static final String LZ_CH_WIKI3_TITLE = "rW*i-1DMQ";
    private static final String LZ_CH_CONCEPT_MAP_TITLE = "kGWlf9Ro6gjEA";
    private static final String LZ_CH_BOOK_TITLE = "EYexGtA*zI";
    private static final String LZ_CH_TEST = "jR2jMoo";
    private static final String LZ_CH_CONTENT = "uaKk7tI";
    private Locale locale = new Locale("en");
    
    public TestValue() {
    }
    
    public TestValue(Locale locale) {
    	this.locale = locale;
    }
    
    @Override
    public void setLocale(String locale) {
        this.locale = new Locale(locale);
    }
    
    public static ClassEntryOBJ ConvertClassEntry(PublicEntryOBJ publicEntry) {
        ClassEntryOBJ result = TypeConverter.construct(publicEntry, ClassEntryOBJ.class);
        result.setEntryID(publicEntry.getReferenceId());
        return result;
    }

    public void setupTestValue() {
    	if (getTestParams().size() > 0) {
    		return;
    	}
        logger.info("set Locale: " + locale.toString());
        if ("zh".equalsIgnoreCase(locale.toString())) {
            setupTestChValue();
        } else {
            setupTestEnValue();
        }
    }

    /**
     * setup test value
     */
    public void setupTestEnValue() {
        
        HashMap<String, Object> testValues = getTestParams();
        if (testValues.size() == 0) {
            locale = new Locale("en");
            String defaultXml = "<mxGraphModel grid='1' guides='1' tooltips='1' connect='1' fold='1' page='0' pageScale='1' pageWidth='800' pageHeight='600'><root><mxCell id='0'/><mxCell id='1' parent='0'/></root></mxGraphModel>";
            String newXml = "<mxGraphModel grid='1' guides='1' tooltips='1' connect='1' fold='1' page='0' pageScale='1' pageWidth='800' pageHeight='600'><root><mxCell id='0'/><mxCell id='1' parent='0'/><mxCell id='2' value='xyzabc' vertex='1' parent='1'><mxGeometry x='260' y='110' width='120' height='60' as='geometry'/></mxCell></root></mxGraphModel>";
            testValues.put("defaultPass", "123456");
            testValues.put("defaultXml", defaultXml);
            testValues.put("newXml", newXml);

            // student 1
            UserOBJ student1 = new UserOBJ();
            student1.setUserID("bob");
            student1.setAccountType("student");
            student1.setFirstName("bob");
            student1.setLastName("wang");
            student1.setEmail("bob@123.com");
            testValues.put("student1", student1);

            // student 2
            UserOBJ student2 = new UserOBJ();
            student2.setUserID("tom");
            student2.setAccountType("student");
            student2.setFirstName("tom");
            student2.setLastName("ho");
            student2.setEmail("tom@123.com");
            testValues.put("student2", student2);

            // student 3
            UserOBJ student3 = new UserOBJ();
            student3.setUserID("john");
            student3.setAccountType("student");
            student3.setFirstName("john");
            student3.setLastName("lin");
            student3.setEmail("john@123.com");
            testValues.put("student3", student3);
            
            // student 4
            UserOBJ student4 = new UserOBJ();
            student4.setUserID("ken");
            student4.setAccountType("student");
            student4.setFirstName("ken");
            student4.setLastName("lee");
            student4.setEmail("ken@123.com");
            testValues.put("student4", student4);
            
            // student 5
            UserOBJ student5 = new UserOBJ();
            student5.setUserID("alice");
            student5.setAccountType("student");
            student5.setFirstName("alice");
            student5.setLastName("lin");
            student5.setEmail("alice@123.com");
            testValues.put("student5", student5);
            
            // student 6
            UserOBJ student6 = new UserOBJ();
            student6.setUserID("jane");
            student6.setAccountType("student");
            student6.setFirstName("jane");
            student6.setLastName("yen");
            student6.setEmail("jane@123.com");
            testValues.put("student6", student6);
            
            // student 7
            UserOBJ student7 = new UserOBJ();
            student7.setUserID("cindy");
            student7.setAccountType("student");
            student7.setFirstName("cindy");
            student7.setLastName("kuo");
            student7.setEmail("cindy@123.com");
            testValues.put("student7", student7);

            // teacher
            UserOBJ teacher = new UserOBJ();
            teacher.setUserID("mary");
            teacher.setAccountType("teacher");
            teacher.setFirstName("mary");
            teacher.setLastName("chen");
            teacher.setEmail("mary@123.com");
            testValues.put("teacher", teacher);

            // admin
            UserOBJ admin = new UserOBJ();
            admin.setUserID("admin");
            admin.setAccountType("admin");
            admin.setFirstName("admin");
            admin.setLastName("admin");
            admin.setEmail("admin@123.com");
            testValues.put("admin", admin);

            // class
            ClassOBJ test_class = new ClassOBJ();
            test_class.setClassID(1);
            test_class.setClassName("physics_test");
            test_class.setAlias("test");
            test_class.setField("physics");
            test_class.setTeacher("mary");
            test_class.setClassCode("mary10000");
            testValues.put("test_class", test_class);

            ClassOBJ test_class2 = new ClassOBJ();
            test_class2.setClassID(2);
            test_class2.setClassName("physics_001");
            test_class2.setField("physics");
            test_class2.setAlias("001");
            test_class2.setTeacher("mary");
            test_class2.setClassCode("mary20000");
            testValues.put("test_class2", test_class2);

            // event1
            EventOBJ event1 = new EventOBJ();
            event1.setEventWord("event1");
            event1.setEntryID(1);
            event1.setXml(newXml);
            event1.setBgImage("");
            event1.setDescription("");
            event1.setLeader(student1.getUserID());
            event1.setTeam("1_event");
            event1.setKeyword(event1.getEntryTitle());
            testValues.put("event1", event1);
            ArrayList<String> eventTeam = new ArrayList<String>();
            eventTeam.add(student1.getUserID());
            eventTeam.add(student2.getUserID());
            testValues.put("eventTeam", eventTeam);

            // delete event
            EventOBJ deleteEvent = new EventOBJ();
            deleteEvent.setEventWord("deleteEvent");
            deleteEvent.setEntryID(2);
            deleteEvent.setXml(newXml);
            deleteEvent.setBgImage("");
            deleteEvent.setDescription("");
            deleteEvent.setLeader(student1.getUserID());
            deleteEvent.setTeam("2_event");
            deleteEvent.setKeyword(deleteEvent.getEntryTitle());
            testValues.put("deleteEvent", deleteEvent);

            // wiki1
            WikiOBJ wiki1 = new WikiOBJ();
            wiki1.setWikiWord("wiki1");
            wiki1.setEntryID(1);
            wiki1.setLeader(student2.getUserID());
            wiki1.setTeam("1_wiki");
            wiki1.setKeyword(wiki1.getEntryTitle());
            testValues.put("wiki1", wiki1);
            ArrayList<String> wikiTeam = new ArrayList<String>();
            wikiTeam.add(student2.getUserID());
            wikiTeam.add(student3.getUserID());
            testValues.put("wikiTeam", wikiTeam);

            // delete wiki
            WikiOBJ deleteWiki = new WikiOBJ();
            deleteWiki.setWikiWord("deleteWiki");
            deleteWiki.setEntryID(2);
            deleteWiki.setLeader(student2.getUserID());
            deleteWiki.setTeam("2_wiki");
            deleteWiki.setKeyword(deleteWiki.getEntryTitle());
            testValues.put("deleteWiki", deleteWiki);

            // concept1
            ConceptOBJ concept1 = new ConceptOBJ();
            concept1.setConceptWord("concept1");
            concept1.setEntryID(1);
            concept1.setLeader(student3.getUserID());
            concept1.setTeam("1_concept");
            concept1.setKeyword(concept1.getEntryTitle());
            concept1.setXml(newXml);
            testValues.put("concept1", concept1);
            ArrayList<String> conceptTeam = new ArrayList<String>();
            conceptTeam.add(student3.getUserID());
            conceptTeam.add(student1.getUserID());
            testValues.put("conceptTeam", conceptTeam);

            // delete concept
            ConceptOBJ deleteConcept = new ConceptOBJ();
            deleteConcept.setConceptWord("deleteConcept");
            deleteConcept.setEntryID(2);
            deleteConcept.setLeader(student3.getUserID());
            deleteConcept.setTeam("2_concept");
            deleteConcept.setKeyword(deleteConcept.getEntryTitle());
            deleteConcept.setXml(newXml);
            testValues.put("deleteConcept", deleteConcept);

            StatusOBJ status = new StatusOBJ();
            event1.setStatus(status);
            wiki1.setStatus(status);
            concept1.setStatus(status);

            testValues.put("currentClass", -1);
            testValues.put("httpBg", "http://localhost:8080/TKSS/orbit-images/headPic1.jpg");

            testValues.put("local-search-word", "wiki1");
            testValues.put("public-search-word", "wiki1");

            // wiki2 for search test
            WikiOBJ wiki2 = new WikiOBJ();
            wiki2.setWikiWord("wiki2");
            wiki2.setEntryID(5);
            wiki2.setLeader(teacher.getUserID());
            wiki2.setTeam("5_wiki");
            wiki2.setKeyword(wiki1.getEntryTitle() + ", " + wiki2.getEntryTitle());
            testValues.put("wiki2", wiki2);
            
            // wiki3 for license test
            WikiOBJ wiki3 = new WikiOBJ();
            wiki3.setWikiWord("wiki3");
            wiki3.setEntryID(6);
            wiki3.setLeader(teacher.getUserID());
            wiki3.setTeam("6_wiki");
            wiki3.setKeyword(wiki3.getEntryTitle());
            testValues.put("wiki3", wiki3);
            
            // book1
            BookOBJ book1 = new BookOBJ();
            book1.setBookWord("book" + System.currentTimeMillis());
            book1.setEntryID(1);
            book1.setLeader(student1.getUserID());
            book1.setTeam("1_book");
            BookChapterOBJ chapter1 = new BookChapterOBJ();
            chapter1.setBookID(book1.getEntryID());
            chapter1.setChapterID(1);
            chapter1.setType(event1.getEntryClass());
            chapter1.setTitle(event1.getEntryTitle());
            chapter1.setEntryID(event1.getEntryID());
            BookChapterOBJ chapter2 = new BookChapterOBJ();
            chapter2.setBookID(book1.getEntryID());
            chapter2.setChapterID(2);
            chapter2.setType(wiki1.getEntryClass());
            chapter2.setTitle(wiki1.getEntryTitle());
            chapter2.setEntryID(wiki1.getEntryID());
            BookChapterOBJ chapter3 = new BookChapterOBJ();
            chapter3.setBookID(book1.getEntryID());
            chapter3.setChapterID(3);
            chapter3.setType(concept1.getEntryClass());
            chapter3.setTitle(concept1.getEntryTitle());
            chapter3.setEntryID(concept1.getEntryID());
            List<BookChapterOBJ> chapters = new ArrayList<BookChapterOBJ>();
            chapters.add(chapter1);
            chapters.add(chapter2);
            chapters.add(chapter3);
            book1.setBookChapters(chapters);
            testValues.put("book1", book1);
            
            CommentOBJ comment = new CommentOBJ();
            comment.setComments("@qwert");
            testValues.put("comment", comment);
            
            
        }
    }

    /**
     * setup test value
     */
    public void setupTestChValue() {
        HashMap<String, Object> testValues = getTestParams();
        if (testValues.size() == 0) {
            locale = new Locale("zh");
            String defaultXml = "<mxGraphModel grid='1' guides='1' tooltips='1' connect='1' fold='1' page='0' pageScale='1' pageWidth='800' pageHeight='600'><root><mxCell id='0'/><mxCell id='1' parent='0'/></root></mxGraphModel>";
            String newXml = "<mxGraphModel grid='1' guides='1' tooltips='1' connect='1' fold='1' page='0' pageScale='1' pageWidth='1600' pageHeight='1200'><root><mxCell id='0'/><mxCell id='1' parent='0'/></root></mxGraphModel>";
            testValues.put("defaultPass", "123456");
            testValues.put("defaultXml", defaultXml);
            testValues.put("newXml", newXml);

            // student 1
            UserOBJ student1 = new UserOBJ();
            student1.setUserID("bob");
            student1.setAccountType("student");
            student1.setFirstName("bob");
            student1.setLastName("wang");
            student1.setEmail("bob@123.com");
            testValues.put("student1", student1);

            // student 2
            UserOBJ student2 = new UserOBJ();
            student2.setUserID("tom");
            student2.setAccountType("student");
            student2.setFirstName("tom");
            student2.setLastName("ho");
            student2.setEmail("tom@123.com");
            testValues.put("student2", student2);

            // student 3
            UserOBJ student3 = new UserOBJ();
            student3.setUserID("john");
            student3.setAccountType("student");
            student3.setFirstName("john");
            student3.setLastName("lin");
            student3.setEmail("john@123.com");
            testValues.put("student3", student3);

            // student 4
            UserOBJ student4 = new UserOBJ();
            student4.setUserID("ken");
            student4.setAccountType("student");
            student4.setFirstName("ken");
            student4.setLastName("lee");
            student4.setEmail("ken@123.com");
            testValues.put("student4", student4);
            
            // student 5
            UserOBJ student5 = new UserOBJ();
            student5.setUserID("alice");
            student5.setAccountType("student");
            student5.setFirstName("alice");
            student5.setLastName("lin");
            student5.setEmail("alice@123.com");
            testValues.put("student5", student5);
            
            // student 6
            UserOBJ student6 = new UserOBJ();
            student6.setUserID("jane");
            student6.setAccountType("student");
            student6.setFirstName("jane");
            student6.setLastName("yen");
            student6.setEmail("jane@123.com");
            testValues.put("student6", student6);
            
            // student 7
            UserOBJ student7 = new UserOBJ();
            student7.setUserID("cindy");
            student7.setAccountType("student");
            student7.setFirstName("cindy");
            student7.setLastName("kuo");
            student7.setEmail("cindy@123.com");
            testValues.put("student7", student7);
            
            // teacher
            UserOBJ teacher = new UserOBJ();
            teacher.setUserID("mary");
            teacher.setAccountType("teacher");
            teacher.setFirstName("mary");
            teacher.setLastName("chen");
            teacher.setEmail("mary@123.com");
            testValues.put("teacher", teacher);

            // admin
            UserOBJ admin = new UserOBJ();
            admin.setUserID("admin");
            admin.setAccountType("admin");
            admin.setFirstName("admin");
            admin.setLastName("admin");
            admin.setEmail("admin@123.com");
            testValues.put("admin", admin);

            // class
            ClassOBJ test_class = new ClassOBJ();
            test_class.setClassID(1);
            test_class.setClassName("physics_test");
            test_class.setAlias("test");
            test_class.setField("physics");
            test_class.setTeacher("mary");
            test_class.setClassCode("mary10000");
            testValues.put("test_class", test_class);

            ClassOBJ test_class2 = new ClassOBJ();
            test_class2.setClassID(2);
            test_class2.setClassName("physics_001");
            test_class2.setField("physics");
            test_class2.setAlias("001");
            test_class2.setTeacher("mary");
            test_class2.setClassCode("mary20000");
            testValues.put("test_class2", test_class2);

            // event1
            EventOBJ event1 = new EventOBJ();
            event1.setEventWord(LZString.decode(LZ_CH_EVENT_TITLE));
            event1.setEntryID(1);
            event1.setXml(newXml);
            event1.setBgImage("");
            event1.setDescription(LZString.decode(LZ_CH_TEST));
            event1.setLeader(student1.getUserID());
            event1.setTeam("1_event");
            event1.setKeyword(event1.getEntryTitle());
            testValues.put("event1", event1);
            ArrayList<String> eventTeam = new ArrayList<String>();
            eventTeam.add(student1.getUserID());
            eventTeam.add(student2.getUserID());
            testValues.put("eventTeam", eventTeam);

            // wiki1
            WikiOBJ wiki1 = new WikiOBJ();
            wiki1.setWikiWord(LZString.decode(LZ_CH_WIKI1_TITLE));
            wiki1.setEntryID(1);
            wiki1.setLeader(student2.getUserID());
            wiki1.setTeam("1_wiki");
            wiki1.setKeyword(wiki1.getEntryTitle());
            wiki1.setText(LZString.decode(LZ_CH_CONTENT));
            testValues.put("wiki1", wiki1);
            ArrayList<String> wikiTeam = new ArrayList<String>();
            wikiTeam.add(student2.getUserID());
            wikiTeam.add(student3.getUserID());
            testValues.put("wikiTeam", wikiTeam);

            // concept1
            ConceptOBJ concept1 = new ConceptOBJ();
            concept1.setConceptWord(LZString.decode(LZ_CH_CONCEPT_MAP_TITLE));
            concept1.setEntryID(1);
            concept1.setLeader(student3.getUserID());
            concept1.setTeam("1_concept");
            concept1.setKeyword(concept1.getEntryTitle());
            concept1.setXml(newXml);
            testValues.put("concept1", concept1);
            ArrayList<String> conceptTeam = new ArrayList<String>();
            conceptTeam.add(student3.getUserID());
            conceptTeam.add(student1.getUserID());
            testValues.put("conceptTeam", conceptTeam);

            StatusOBJ status = new StatusOBJ();
            event1.setStatus(status);
            wiki1.setStatus(status);
            concept1.setStatus(status);

            testValues.put("currentClass", -1);
            testValues.put("httpBg", "http://localhost:8080/TKSS/orbit-images/headPic1.jpg");

            testValues.put("local-search-word", LZString.decode(LZ_CH_WIKI1_TITLE));
            testValues.put("public-search-word", LZString.decode(LZ_CH_WIKI1_TITLE));

            // wiki2 for search test
            WikiOBJ wiki2 = new WikiOBJ();
            wiki2.setWikiWord(LZString.decode(LZ_CH_WIKI2_TITLE));
            wiki2.setEntryID(1);
            wiki2.setLeader(teacher.getUserID());
            wiki2.setTeam("2_wiki");
            wiki2.setKeyword(wiki1.getEntryTitle() + ", " + wiki2.getEntryTitle());
            testValues.put("wiki2", wiki2);
            
            // wiki3 for license test
            WikiOBJ wiki3 = new WikiOBJ();
            wiki3.setWikiWord(LZString.decode(LZ_CH_WIKI3_TITLE));
            wiki3.setEntryID(6);
            wiki3.setLeader(teacher.getUserID());
            wiki3.setTeam("6_wiki");
            wiki3.setKeyword(wiki3.getEntryTitle());
            testValues.put("wiki3", wiki3);
            
            // book1
            BookOBJ book1 = new BookOBJ();
            book1.setBookWord(LZString.decode(LZ_CH_BOOK_TITLE));
            book1.setEntryID(1);
            book1.setLeader(student1.getUserID());
            book1.setTeam("1_book");
            BookChapterOBJ chapter1 = new BookChapterOBJ();
            chapter1.setBookID(book1.getEntryID());
            chapter1.setChapterID(1);
            chapter1.setType(event1.getEntryClass());
            chapter1.setTitle(event1.getEntryTitle());
            chapter1.setEntryID(event1.getEntryID());
            BookChapterOBJ chapter2 = new BookChapterOBJ();
            chapter2.setBookID(book1.getEntryID());
            chapter2.setChapterID(2);
            chapter2.setType(wiki1.getEntryClass());
            chapter2.setTitle(wiki1.getEntryTitle());
            chapter2.setEntryID(wiki1.getEntryID());
            BookChapterOBJ chapter3 = new BookChapterOBJ();
            chapter3.setBookID(book1.getEntryID());
            chapter3.setChapterID(3);
            chapter3.setType(concept1.getEntryClass());
            chapter3.setTitle(concept1.getEntryTitle());
            chapter3.setEntryID(concept1.getEntryID());
            List<BookChapterOBJ> chapters = new ArrayList<BookChapterOBJ>();
            chapters.add(chapter1);
            chapters.add(chapter2);
            chapters.add(chapter3);
            book1.setBookChapters(chapters);
            testValues.put("book1", book1);
            
            CommentOBJ comment = new CommentOBJ();
            comment.setComments(LZString.decode(LZ_CH_TEST));
            testValues.put("comment", comment);
            
        }
    }

    /**
     * get test value
     * 
     * @return test value map
     */
    public HashMap<String, Object> getTestValue() {
        setupTestValue();
        return getTestParams();
    }

}