package com.yung.web.test.ikos;

import java.util.Locale;

import com.yung.web.test.SwingAutoTestApp;

public class CombineTest {

    public static void main(String args[]) throws Exception {
        
        SwingAutoTestApp app = new SwingAutoTestApp(
                new CheckServer()
                ,new InitDatabase()
                ,new AccountTest()
                ,new ClassTest()
                ,new EntryTest()
                ,new EntryPublicationTest()
                ,new EntryMiscellaneousTest()
                /*,new EntryUpdateTest()*/
                ,new EntryAddToPublicDatabaseTest()
                /*,new EntryAddToClassTest()*/
                ,new BookTest()
                ,new ProgressTest()
                ,new SearchTest()
                ,new ClassGraphTest()
                ,new DisplayMessageTest()
                ,new CloseClassTest()
                ,new LicenseTest()
        );
        app.setTestLocale(new Locale("ch"));
        app.showUI();
        
    }
    
}