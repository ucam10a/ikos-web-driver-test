package com.yung.web.test.ikos;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.yung.web.test.App;
import com.yung.web.test.AutoTestApp;

import tkss.util.orm.ClassOBJ;
import tkss.util.orm.UserOBJ;

public class ClassGraphTest extends IkosScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, App app) {
        
        try {
            
        	HashMap<String, Object> param = app.getTestParam().getTestParams();
            UserOBJ teacher = (UserOBJ) param.get("teacher");
            
            ClassOBJ test_class = (ClassOBJ) param.get("test_class");
            
            login(driver, teacher);
            selectClass(driver, test_class);
            clickElement(driver, driver.findElement(By.xpath("//a[@id='showClassGraph']")));
            Thread.sleep(5000);
            String html = getElementHtml(driver, "//div[@id='holder']");
            if (!html.contains("<path")) {
                throw new Exception("ClassGraphTest fail");
            }
            logout(driver);
            logMessage(app, "ClassGraphTest test successfully!");
            
        } catch (Exception e) {
            logMessage(app, e.toString());
            e.printStackTrace();
            setSTOP(true);
            takeScreenShot(driver);
            driver.quit();
        }
        
    }

    @Override
    public void check(WebDriver driver, Properties prop, App app) {
        // TODO Auto-generated method stub
        
    }

    public void takeScreenShot(WebDriver driver) {
        if (!AutoTestApp.DRIVER_NAME.equals("phantomjs.exe")) {
            return;
        }
        System.out.println("Taking screenshot now");
        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        System.out.println("File:" + srcFile);
        try {
            FileUtils.copyFile(srcFile, new File(SCREEN_SHOT));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
    
}