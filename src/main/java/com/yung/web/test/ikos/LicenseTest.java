package com.yung.web.test.ikos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.yung.web.test.App;

import tkss.util.MessageUtil;
import tkss.util.orm.ClassOBJ;
import tkss.util.orm.UserOBJ;

public class LicenseTest extends EntryTest {

    @Override
    public void execute(WebDriver driver, Properties prop, App app) {
        
        try {
            
        	HashMap<String, Object> params = app.getTestParam().getTestParams();
            UserOBJ admin = (UserOBJ) params.get("admin");
            UserOBJ teacher = (UserOBJ) params.get("teacher");
            ClassOBJ test_class = (ClassOBJ) params.get("test_class");
            
            // setup class code
            login(driver, teacher);
            driver.findElement(By.xpath("//a[@id='showMyClass']")).click();
            waitForElement(driver, "//span[@id='" + test_class.getField() + "_" + test_class.getAlias() + "-code-myPage']", 30);
            String classCode = driver.findElement(By.xpath("//span[@id='" + test_class.getField() + "_" + test_class.getAlias() + "-code-myPage']")).getText();
            test_class.setClassCode(classCode);
            logout(driver);
            
            UserOBJ student4 = (UserOBJ) params.get("student4");
            UserOBJ student5 = (UserOBJ) params.get("student5");
            UserOBJ student6 = (UserOBJ) params.get("student6");
            UserOBJ student7 = (UserOBJ) params.get("student7");
            
            createStudent(driver, student4, app);
            createStudent(driver, student5, app);
            createStudent(driver, student6, app);
            createStudent(driver, student7, app);
            
            login(driver, student4);
            register(driver, student4, test_class, teacher);
            logout(driver);
            
            login(driver, student5);
            register(driver, student5, test_class, teacher);
            logout(driver);
            
            login(driver, student6);
            boolean registerLock = false;
            register(driver, student6, test_class, teacher);
            Thread.sleep(3000);
            
            String text = driver.findElement(By.xpath("//body")).getText();
            if (text.contains(MessageUtil.getMessage(app.getTestLocale(), "error.msg"))) {
                registerLock = true;
            } else {
                registerLock = false;
            }
            
            if (registerLock != true) {
                throw new Exception("register not lock!");
            }
            logout(driver);
            
            // teacher import license
            String licenseCode = getLicenseCode(driver, admin);
            login(driver, teacher);
            WebElement settingEle = driver.findElement(By.xpath("//a[@id='changeSetting-link']"));
            settingEle.click();
            Thread.sleep(1000);
            WebElement licenseTabEle = driver.findElement(By.xpath("//div[@id='profileTab3Wait']"));
            licenseTabEle.click();
            Thread.sleep(1000);
            driver.findElement(By.xpath("//input[@id='licenseCode']")).sendKeys(licenseCode);
            Thread.sleep(1000);
            WebElement importLicenseBtn = driver.findElement(By.xpath("//input[@id='importLicense-btn']"));
            waitForAlert(driver, importLicenseBtn, MessageUtil.getMessage(app.getTestLocale(), "msg.import.license.success"));
            logout(driver);
            
            // student register
            login(driver, student6);
            register(driver, student6, test_class, teacher);
            logout(driver);
            
            login(driver, student7);
            registerLock = false;
            register(driver, student7, test_class, teacher);
            Thread.sleep(3000);
            
            text = driver.findElement(By.xpath("//body")).getText();
            if (text.contains(MessageUtil.getMessage(app.getTestLocale(), "error.msg"))) {
                registerLock = true;
            } else {
                registerLock = false;
            }
            if (registerLock != true) {
                throw new Exception("register not lock!");
            }
            logout(driver);
            
            logMessage(app, "LicenseTest test successfully!");
            
        } catch (Exception e) {
            logMessage(app, e.toString());
            e.printStackTrace();
            setSTOP(true);
            takeScreenShot(driver);
            driver.quit();
        }
        
    }

    private void createStudent(WebDriver driver, UserOBJ student, App app) throws InterruptedException {
        AccountTest.createStudent(driver, student, app);
    }

    private void register(WebDriver driver, UserOBJ student, ClassOBJ test_class, UserOBJ teacher) throws InterruptedException {
        ClassTest.register(driver, student, test_class, teacher);
        Thread.sleep(1000);
    }

    private String getLicenseCode(WebDriver driver, UserOBJ admin) throws Exception {
        String code = "";
        login(driver, admin);
        WebElement accountEle = driver.findElement(By.xpath("//a[@id='becomeUser']"));
        accountEle.click();
        Thread.sleep(1000);
        WebElement licenseEle = driver.findElement(By.xpath("//a[@id='license-link']"));
        licenseEle.click();
        Thread.sleep(1000);
        // go to license tab
        List<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        Thread.sleep(1000);
        WebElement showLicenseEle = driver.findElement(By.xpath("//input[@id='showLicense-btn']"));
        showLicenseEle.click();
        Thread.sleep(5000);
        WebElement iFrame = driver.findElement(By.xpath("//iframe[1]"));
        driver.switchTo().frame(iFrame);
        WebElement tdEle = driver.findElement(By.xpath("//div[@class='ht_master handsontable']//table[@class='htCore']//tr[position()=2]//td[position()=7]"));
        code = tdEle.getText();
        driver.switchTo().defaultContent();
        driver.close();
        // go to origin tab
        driver.switchTo().window(tabs.get(0));
        logout(driver);
        return code;
    }
    
}