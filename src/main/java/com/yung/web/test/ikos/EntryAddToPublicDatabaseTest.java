package com.yung.web.test.ikos;

import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.yung.web.test.App;

import tkss.util.MessageUtil;
import tkss.util.TKSSUtil;
import tkss.util.orm.ClassEntryOBJ;
import tkss.util.orm.ClassOBJ;
import tkss.util.orm.ConceptOBJ;
import tkss.util.orm.EventOBJ;
import tkss.util.orm.UserOBJ;
import tkss.util.orm.WikiOBJ;

public class EntryAddToPublicDatabaseTest extends IkosScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, App app) {
        
        try {
            
        	HashMap<String, Object> params = app.getTestParam().getTestParams();
            EventOBJ event1 = (EventOBJ) params.get("event1");
            WikiOBJ wiki1 = (WikiOBJ) params.get("wiki1");
            ConceptOBJ concept1 = (ConceptOBJ) params.get("concept1");
            
            UserOBJ teacher = (UserOBJ) params.get("teacher");
            UserOBJ admin = (UserOBJ) params.get("admin");
            
            ClassOBJ test_class = (ClassOBJ) params.get("test_class");
            
            // event
            teacherSubmit(app, driver, event1, test_class, teacher);
            adminApprove(app, driver, event1, test_class, admin);
            logMessage(app, "submit entry " + event1.getEntryClass() + ": " + event1.getEntryTitle() + " to public database successfully!");
            
            // wiki
            teacherSubmit(app, driver, wiki1, test_class, teacher);
            adminApprove(app, driver, wiki1, test_class, admin);
            logMessage(app, "submit entry " + wiki1.getEntryClass() + ": " + wiki1.getEntryTitle() + " to public database successfully!");
            
            // concept
            teacherSubmit(app, driver, concept1, test_class, teacher);
            adminApprove(app, driver, concept1, test_class, admin);
            logMessage(app, "submit entry " + concept1.getEntryClass() + ": " + concept1.getEntryTitle() + " to public database successfully!");
            
        } catch (Exception e) {
            logMessage(app, e.toString());
            e.printStackTrace();
            setSTOP(true);
            takeScreenShot(driver);
            driver.quit();
        }
        
    }

    private void adminApprove(App app, WebDriver driver, ClassEntryOBJ entry, ClassOBJ test_class, UserOBJ admin) throws Exception {
        login(driver, admin);
        selectAdminMainPage(driver, entry);
        clickElement(driver, driver.findElement(By.xpath("//a[@id='entrylist-" + entry.getEntryID() + "']")));
        if ("event".equalsIgnoreCase(entry.getEntryClass())) {
            String entryTitle = MessageUtil.getMessage(app.getTestLocale(), "pic.tag.title");
            waitForText(driver, 30, entryTitle, entry.getEntryTitle());
        } else if ("wiki".equalsIgnoreCase(entry.getEntryClass())) {
            String entryTitle = MessageUtil.getMessage(app.getTestLocale(), "wiki.title");
            waitForText(driver, 30, entryTitle, entry.getEntryTitle());
        } else if ("concept".equalsIgnoreCase(entry.getEntryClass())) {
            String entryTitle = MessageUtil.getMessage(app.getTestLocale(), "concept.map.title");
            waitForText(driver, 30, entryTitle, entry.getEntryTitle());
        } else {
            throw new Exception("Unknow type: " + entry.getEntryClass());
        }
        Thread.sleep(1000);
        clickElement(driver, driver.findElement(By.xpath("//a[@id='accept-checkBottom']")));
        Thread.sleep(1000);
        String text = driver.findElement(By.xpath("//div[@id='center_view']")).getText();
        String srcMsg = MessageUtil.getMessage(app.getTestLocale(), "msg.successfully.public.added");
        String showMsg = TKSSUtil.createMesssage(srcMsg, entry.getEntryTitle());
        if (!text.contains(showMsg)) {
            throw new Exception("approve to public database fail!");
        }
        logout(driver);
    }

    private void teacherSubmit(App app, WebDriver driver, ClassEntryOBJ entry, ClassOBJ test_class, UserOBJ teacher) throws Exception {
        login(driver, teacher);
        selectClass(driver, test_class);
        selectEntry(driver, entry, app);
        clickElement(driver, driver.findElement(By.xpath("//div[@id='tab3Row']")));
        Thread.sleep(1000);
        clickElement(driver, driver.findElement(By.xpath("//a[@id='applyToPublcDatabase-bottomTab']")));
        Thread.sleep(1000);
        String text = driver.findElement(By.xpath("//div[@id='center_view']")).getText();
        String srcMsg = MessageUtil.getMessage(app.getTestLocale(), "msg.successfully.submitted");
        String showMsg = TKSSUtil.createMesssage(srcMsg, entry.getEntryTitle());
        if (!text.contains(showMsg)) {
            throw new Exception("submit to public database fail!");
        }
        logout(driver);
    }

    @Override
    public void check(WebDriver driver, Properties prop, App app) {
        // TODO Auto-generated method stub
        
    }

}