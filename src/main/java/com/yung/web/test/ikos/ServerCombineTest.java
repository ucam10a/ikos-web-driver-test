package com.yung.web.test.ikos;

import java.util.Locale;

import com.yung.web.test.App;
import com.yung.web.test.AppLogger;
import com.yung.web.test.ServerTestApp;

public class ServerCombineTest {

    private static final AppLogger logger = AppLogger.getLogger(ServerCombineTest.class);
    
    public static void main(String[] args) throws Exception {
        
        IkosScenario.HOME_URL = "http://192.168.2.107:8080/TKSS";
        
        String localeArg = "en";
        String logNo = "";
        if (args != null) {
            if (args.length > 0) {
                logNo = args[0];
            }
            if (args.length > 1) {
                localeArg = args[1];
            }
        }
        
        logger.info("localeArg: " + localeArg);
        logger.info("logNo: " + logNo);
        logger.info("headless: " + App.getServerTestAppHeadless());
        logger.info("logDir: " + App.getCurrentLogDir(logNo));
        logger.info("remoteURL: " + App.getServerTestAppRemoteURL());
        
        ServerTestApp app = new ServerTestApp(
                new CheckServer()
                ,new InitDatabase()
                ,new AccountTest()
                ,new ClassTest()
                ,new EntryTest()
                ,new EntryPublicationTest()
                ,new EntryMiscellaneousTest()
                /*,new EntryUpdateTest()*/
                ,new EntryAddToPublicDatabaseTest()
                /*,new EntryAddToClassTest()*/
                ,new BookTest()
                ,new ProgressTest()
                ,new SearchTest()
                ,new ClassGraphTest()
                ,new DisplayMessageTest()
                ,new CloseClassTest()
                ,new LicenseTest()
        );
        
        TestValue testParam = new TestValue(new Locale(localeArg));
        testParam.setupTestValue();
        app.setTestParam(testParam);
        app.configEnv(localeArg, logNo);
        
        try {
            app.run();
        } finally {
            app.closeDriver();
        }
        
    }

}
