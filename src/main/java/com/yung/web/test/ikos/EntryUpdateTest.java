package com.yung.web.test.ikos;

import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.yung.web.test.App;

import tkss.util.MessageUtil;
import tkss.util.TKSSUtil;
import tkss.util.orm.ClassEntryOBJ;
import tkss.util.orm.ClassOBJ;
import tkss.util.orm.ConceptOBJ;
import tkss.util.orm.EventOBJ;
import tkss.util.orm.UserOBJ;
import tkss.util.orm.WikiOBJ;

public class EntryUpdateTest extends IkosScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, App app) {
        
        try {
            
        	HashMap<String, Object> params = app.getTestParam().getTestParams();
            UserOBJ student1 = (UserOBJ) params.get("student1");
            EventOBJ event1 = (EventOBJ) params.get("event1");
            UserOBJ student2 = (UserOBJ) params.get("student2");
            WikiOBJ wiki1 = (WikiOBJ) params.get("wiki1");
            UserOBJ student3 = (UserOBJ) params.get("student3");
            ConceptOBJ concept1 = (ConceptOBJ) params.get("concept1");
            
            ClassOBJ test_class = (ClassOBJ) params.get("test_class");
            
            // event
            updateEntry(app, driver, student2, event1, test_class);
            acceptUpdate(app, driver, student1, event1, test_class);
            logMessage(app, "update entry " + event1.getEntryClass() + ": " + event1.getEntryTitle() + " successfully!");
            
            // wiki
            updateEntry(app, driver, student3, wiki1, test_class);
            acceptUpdate(app, driver, student2, wiki1, test_class);
            logMessage(app, "update entry " + wiki1.getEntryClass() + ": " + wiki1.getEntryTitle() + " successfully!");
            
            // concept
            updateEntry(app, driver, student1, concept1, test_class);
            acceptUpdate(app, driver, student3, concept1, test_class);
            logMessage(app, "update entry " + concept1.getEntryClass() + ": " + concept1.getEntryTitle() + " successfully!");
            
        } catch (Exception e) {
            logMessage(app, e.toString());
            e.printStackTrace();
            setSTOP(true);
            takeScreenShot(driver);
            driver.quit();
        }
        
    }

    private void acceptUpdate(App app, WebDriver driver, UserOBJ student, ClassEntryOBJ entry, ClassOBJ test_class) throws Exception {
        checkCurrentView(app, driver, student, entry, test_class);
        // accept update
        login(driver, student);
        selectClass(driver, test_class);
        selectMainEntryPage(driver, entry, app);
        driver.findElement(By.xpath("//a[@id='" + MessageUtil.getMessage(app.getTestLocale(), "go.to.new.update") + "-" + entry.getEntryID() + "-userEntryList']")).click();
        Thread.sleep(3000);
        if ("event".equalsIgnoreCase(entry.getEntryClass())) {
            String html = getElementHtml(driver, "//div[@id='geDiagramContainerDiv']");
            if (!html.contains("<ellipse")) {
                throw new Exception("update fail!");
            }
        } else if ("wiki".equalsIgnoreCase(entry.getEntryClass())) {
            String text = driver.findElement(By.xpath("//div[@class='content']")).getText();
            int idx = text.indexOf("!@#$%");
            if (idx < 0) {
                throw new Exception("update fail!");
            }
        } else if ("concept".equalsIgnoreCase(entry.getEntryClass())) {
            String html = getElementHtml(driver, "//div[@id='geDiagramContainerDiv']");
            if (!html.contains("<ellipse")) {
                throw new Exception("update fail!");
            }
        }
        String srcMsg = MessageUtil.getMessage(app.getTestLocale(), "msg.update.approved");
        String alert = TKSSUtil.createMesssage(srcMsg, getEntryTypeName(entry.getEntryClass(), app), entry.getEntryTitle());
        waitForAlert(driver, driver.findElement(By.xpath("//a[@id='accept-checkBottom']")), alert);
        logout(driver);
        checkUpdateView(app, driver, student, entry, test_class);
    }
    
    private void checkUpdateView(App app, WebDriver driver, UserOBJ student, ClassEntryOBJ entry, ClassOBJ test_class) throws Exception {
        login(driver, student);
        selectClass(driver, test_class);
        selectEntry(driver, entry, app);
        // check current view
        if ("event".equalsIgnoreCase(entry.getEntryClass())) {
            String html = getElementHtml(driver, "//div[@id='geDiagramContainerDiv']");
            if (!html.contains("<ellipse")) {
                throw new Exception("update fail! update view should change after first author accept!");
            }
        } else if ("wiki".equalsIgnoreCase(entry.getEntryClass())) {
            String text = driver.findElement(By.xpath("//div[@id='mainContent']")).getText();
            int idx = text.indexOf("!@#$%");
            if (idx < 0) {
                throw new Exception("update fail! update view should change after first author accept!");
            }
        } else if ("concept".equalsIgnoreCase(entry.getEntryClass())) {
            String html = getElementHtml(driver, "//div[@id='geDiagramContainerDiv']");
            if (!html.contains("<ellipse")) {
                throw new Exception("update fail! update view should change after first author accept!");
            }
        } 
        logout(driver);
    }
    
    private void checkCurrentView(App app, WebDriver driver, UserOBJ student, ClassEntryOBJ entry, ClassOBJ test_class) throws Exception {
        login(driver, student);
        selectClass(driver, test_class);
        selectEntry(driver, entry, app);
        // check current view
        if ("event".equalsIgnoreCase(entry.getEntryClass())) {
            String html = getElementHtml(driver, "//div[@id='geDiagramContainerDiv']");
            if (html.contains("<ellipse")) {
                throw new Exception("update fail! current view should not change before first author accept!");
            }
        } else if ("wiki".equalsIgnoreCase(entry.getEntryClass())) {
            String text = driver.findElement(By.xpath("//div[@id='mainContent']")).getText();
            if (text.contains(entry.getEntryTitle() + "!@#$%")) {
                throw new Exception("update fail! current view should not change before first author accept!");
            }
        } else if ("concept".equalsIgnoreCase(entry.getEntryClass())) {
            String html = getElementHtml(driver, "//div[@id='geDiagramContainerDiv']");
            if (html.contains("<ellipse")) {
                throw new Exception("update fail! current view should not change before first author accept!");
            }
        } 
        logout(driver);
    }
    
    private void updateEntry(App app, WebDriver driver, UserOBJ student, ClassEntryOBJ entry, ClassOBJ test_class) throws Exception {
        login(driver, student);
        selectClass(driver, test_class);
        selectEntry(driver, entry, app);
        driver.findElement(By.xpath("//input[@id='modify']")).click();
        Thread.sleep(3000);
        if ("event".equalsIgnoreCase(entry.getEntryClass())) {
            insertCircle(driver);
        } else if ("wiki".equalsIgnoreCase(entry.getEntryClass())) {
            editWiki(driver, "!@#$%");
        } else if ("concept".equalsIgnoreCase(entry.getEntryClass())) {
            insertCircle(driver);
        } else {
            throw new Exception("Unknow type: " + entry.getEntryClass());
        }
        Thread.sleep(2000);
        String alert = MessageUtil.getMessage(app.getTestLocale(), "update.success");
        if ("event".equalsIgnoreCase(entry.getEntryClass())) {
            waitForAlert(driver, driver.findElement(By.xpath("//a[@id='upperSave-eventEdit']")), alert);
            driver.findElement(By.xpath("//a[@id='upperLeave-eventEdit']")).click();
        } else if ("wiki".equalsIgnoreCase(entry.getEntryClass())) {
            waitForAlert(driver, driver.findElement(By.xpath("//a[@id='upperSave-wikiEdit']")), alert);
            driver.findElement(By.xpath("//a[@id='upperLeave-wikiEdit']")).click();
        } else if ("concept".equalsIgnoreCase(entry.getEntryClass())) {
            waitForAlert(driver, driver.findElement(By.xpath("//a[@id='upperSave-conceptEdit']")), alert);
            driver.findElement(By.xpath("//a[@id='upperLeave-conceptEdit']")).click();
        }
        Thread.sleep(2000);
        logout(driver);
    }

    @Override
    public void check(WebDriver driver, Properties prop, App app) {
        // TODO Auto-generated method stub
        
    }

}