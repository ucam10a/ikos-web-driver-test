package com.yung.web.test.ikos;

import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.yung.web.test.App;

import tkss.util.MessageUtil;
import tkss.util.TKSSUtil;
import tkss.util.orm.ClassEntryOBJ;
import tkss.util.orm.ClassOBJ;
import tkss.util.orm.ConceptOBJ;
import tkss.util.orm.EventOBJ;
import tkss.util.orm.UserOBJ;
import tkss.util.orm.WikiOBJ;

public class EntryPublicationTest extends IkosScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, App app) {
        
        try {
            
        	HashMap<String, Object> params = app.getTestParam().getTestParams();
            UserOBJ student1 = (UserOBJ) params.get("student1");
            EventOBJ event1 = (EventOBJ) params.get("event1");
            UserOBJ student2 = (UserOBJ) params.get("student2");
            WikiOBJ wiki1 = (WikiOBJ) params.get("wiki1");
            UserOBJ student3 = (UserOBJ) params.get("student3");
            ConceptOBJ concept1 = (ConceptOBJ) params.get("concept1");
            
            UserOBJ teacher = (UserOBJ) params.get("teacher");
            
            ClassOBJ test_class = (ClassOBJ) params.get("test_class");
            
            // event
            applyToOpen(app, driver, teacher, student1, event1, test_class);
            logMessage(app, "apply to open " + event1.getEntryClass() + ": " + event1.getEntryTitle() + " successfully!");
            acceptOpen(app, driver, teacher, event1, test_class);
            logMessage(app, "accept to open " + event1.getEntryClass() + ": " + event1.getEntryTitle() + " successfully!");
            switchStatus(app, driver, teacher, event1, test_class);
            switchStatus(app, driver, teacher, event1, test_class);
            logMessage(app, "switch status " + event1.getEntryClass() + ": " + event1.getEntryTitle() + " successfully!");
            
            // wiki
            applyToOpen(app, driver, teacher, student2, wiki1, test_class);
            logMessage(app, "apply to open " + wiki1.getEntryClass() + ": " + wiki1.getEntryTitle() + " successfully!");
            acceptOpen(app, driver, teacher, wiki1, test_class);
            logMessage(app, "accept to open " + wiki1.getEntryClass() + ": " + wiki1.getEntryTitle() + " successfully!");
            switchStatus(app, driver, teacher, wiki1, test_class);
            switchStatus(app, driver, teacher, wiki1, test_class);
            logMessage(app, "switch status " + wiki1.getEntryClass() + ": " + wiki1.getEntryTitle() + " successfully!");
            
            // concept
            applyToOpen(app, driver, teacher, student3, concept1, test_class);
            logMessage(app, "apply to open " + concept1.getEntryClass() + ": " + concept1.getEntryTitle() + " successfully!");
            acceptOpen(app, driver, teacher, concept1, test_class);
            logMessage(app, "accept to open " + concept1.getEntryClass() + ": " + concept1.getEntryTitle() + " successfully!");
            switchStatus(app, driver, teacher, concept1, test_class);
            switchStatus(app, driver, teacher, concept1, test_class);
            logMessage(app, "switch status " + concept1.getEntryClass() + ": " + concept1.getEntryTitle() + " successfully!");
            
        } catch (Exception e) {
            logMessage(app, e.toString());
            e.printStackTrace();
            setSTOP(true);
            takeScreenShot(driver);
            driver.quit();
        }
    
    }

    private void acceptOpen(App app, WebDriver driver, UserOBJ teacher, ClassEntryOBJ entry, ClassOBJ test_class) throws Exception {
        login(driver, teacher);
        selectClass(driver, test_class);
        selectMainEntryPage(driver, entry, app);
        driver.findElement(By.xpath("//a[@id='" + MessageUtil.getMessage(app.getTestLocale(), "go.to.open.applying") + "-" + entry.getEntryID() + "-userEntryList']")).click();
        waitForText(driver, 30, MessageUtil.getMessage(app.getTestLocale(), "then.decide.open"));
        Thread.sleep(1000);
        String srcMsg = MessageUtil.getMessage(app.getTestLocale(), "msg.open.to.class");
        String alert = TKSSUtil.createMesssage(srcMsg, getEntryTypeName(entry.getEntryClass(), app), entry.getEntryTitle());
        waitForAlert(driver, driver.findElement(By.xpath("//a[@id='accept-checkBottom']")), alert);
        Thread.sleep(1000);
        logout(driver);
    }

    private void switchStatus(App app, WebDriver driver, UserOBJ teacher, ClassEntryOBJ entry, ClassOBJ test_class) throws Exception {
        login(driver, teacher);
        selectClass(driver, test_class);
        selectMainEntryPage(driver, entry, app);
        String text = driver.findElement(By.xpath("//span[@id='entryStatus-" + entry.getEntryID() + "-userEntryList']")).getText();
        String srcMsg1 = MessageUtil.getMessage(app.getTestLocale(), "switch.entry");
        String srcMsg2 = MessageUtil.getMessage(app.getTestLocale(), "public.status");
        waitForAlert(driver, driver.findElement(By.xpath("//a[@id='switchStatus-" + entry.getEntryID() + "-userEntryList']")), srcMsg1, entry.getEntryTitle(), srcMsg2);
        Thread.sleep(3000);
        if (MessageUtil.getMessage(app.getTestLocale(), "status.open.to.class").equalsIgnoreCase(text)) {
            text = driver.findElement(By.xpath("//span[@id='entryStatus-" + entry.getEntryID() + "-userEntryList']")).getText();
            if (!MessageUtil.getMessage(app.getTestLocale(), "status.not.open.to.class").equalsIgnoreCase(text)) {
                throw new Exception("status not change");
            }
        } else {
            text = driver.findElement(By.xpath("//span[@id='entryStatus-" + entry.getEntryID() + "-userEntryList']")).getText();
            if (!MessageUtil.getMessage(app.getTestLocale(), "status.open.to.class").equalsIgnoreCase(text)) {
                throw new Exception("status not change");
            }
        }
        logout(driver);
    }

    private void applyToOpen(App app, WebDriver driver, UserOBJ teacher, UserOBJ student, ClassEntryOBJ entry, ClassOBJ test_class) throws Exception {
        login(driver, student);
        selectClass(driver, test_class);
        selectEntry(driver, entry, app);
        clickElement(driver, driver.findElement(By.xpath("//div[@id='tab3Row']")));
        Thread.sleep(1000);
        String srcMsg = MessageUtil.getMessage(app.getTestLocale(), "msg.apply.open.to.class");
        String alert = TKSSUtil.createMesssage(srcMsg, getEntryTypeName(entry.getEntryClass(), app), entry.getEntryTitle());
        waitForAlert(driver, driver.findElement(By.xpath("//a[@id='applyOpen-submit']")), alert);
        logout(driver);
    }

    @Override
    public void check(WebDriver driver, Properties prop, App app) {
        // TODO Auto-generated method stub
        
    }

}