package com.yung.web.test.ikos;

import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.yung.web.test.App;

import tkss.util.MessageUtil;
import tkss.util.orm.ClassOBJ;
import tkss.util.orm.UserOBJ;
import tkss.util.orm.WikiOBJ;

public class SearchTest extends IkosScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, App app) {
        
        try {
            
        	HashMap<String, Object> params = app.getTestParam().getTestParams();
            //WikiOBJ wiki1 = (WikiOBJ) params.get("wiki1");
            WikiOBJ wiki2 = (WikiOBJ) params.get("wiki2");
            
            UserOBJ teacher = (UserOBJ) params.get("teacher");
            
            ClassOBJ test_class = (ClassOBJ) params.get("test_class");
            
            // create wiki2
            login(driver, teacher);
            selectClass(driver, test_class);
            selectMainEntryPage(driver, wiki2, app);
            WebElement add = driver.findElement(By.xpath("//table[@id='myEntryList']//tbody//tr//td//h2//a"));
            add.click();
            Thread.sleep(1000);
            Alert alert = driver.switchTo().alert();
            alert.sendKeys(wiki2.getEntryTitle());
            alert.accept();
            Thread.sleep(1000);
            // add keyword
            String[] keywords = wiki2.getKeyword().split(",");
            String keywordText = driver.findElement(By.xpath("//div[@id='keywords']")).getText();
            for (String keyword : keywords) {
                keyword = keyword.trim();
                if (!keywordText.contains(keyword)) {
                    driver.findElement(By.xpath("//input[@id='keywordNode']")).clear();
                    driver.findElement(By.xpath("//input[@id='keywordNode']")).sendKeys(keyword);
                    Thread.sleep(1000);
                    clickElement(driver, driver.findElement(By.xpath("//u[@id='addKeyword-wikiEdit']")));
                    Thread.sleep(1000);
                }
            }
            String msg = MessageUtil.getMessage(app.getTestLocale(), "update.success");
            waitForAlert(driver, driver.findElement(By.xpath("//a[@id='upperSave-wikiEdit']")), msg);
            driver.findElement(By.xpath("//a[@id='upperLeave-wikiEdit']")).click();
            Thread.sleep(3000);
            logout(driver);
            insertEntryID(app, driver, teacher, wiki2, test_class);
            login(driver, teacher);
            selectClass(driver, test_class);
            selectMainEntryPage(driver, wiki2, app);
            String srcMsg1 = MessageUtil.getMessage(app.getTestLocale(), "switch.entry");
            String srcMsg2 = MessageUtil.getMessage(app.getTestLocale(), "public.status");
            waitForAlert(driver, driver.findElement(By.xpath("//a[@id='switchStatus-" + wiki2.getEntryID() + "-userEntryList']")), srcMsg1, wiki2.getEntryTitle(), srcMsg2);
            Thread.sleep(3000);
            
            // check search
            //driver.findElement(By.xpath("//input[@id='local-tags']")).sendKeys(wiki1.getEntryTitle());
            //Thread.sleep(1000);
            //clickElement(driver, driver.findElement(By.xpath("//input[@id='local-search-submit']")));
            //Thread.sleep(3000);
            //tabClick(driver, "//div[@id='tab2Wait']");
            //String text = driver.findElement(By.xpath("//div[@id='tabContent2']")).getText();
            //if (!text.contains(wiki1.getEntryTitle()) || !text.contains(wiki2.getEntryTitle())) {
            //    throw new Exception("SearchTest fail");
            //}
            //logMessage(app, "SearchTest test successfully!");
            
        } catch (Exception e) {
            logMessage(app, e.toString());
            e.printStackTrace();
            setSTOP(true);
            takeScreenShot(driver);
            driver.quit();
        }
        
    }

    @Override
    public void check(WebDriver driver, Properties prop, App app) {
        // TODO Auto-generated method stub
        
    }

}