package com.yung.web.test.ikos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.yung.web.test.App;

import tkss.util.MessageUtil;
import tkss.util.orm.BookChapterOBJ;
import tkss.util.orm.BookOBJ;
import tkss.util.orm.ClassEntryOBJ;
import tkss.util.orm.ClassOBJ;
import tkss.util.orm.UserOBJ;
import tkss.util.orm.WikiOBJ;

public class BookTest extends IkosScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, App app) {
        
        try {
            
        	HashMap<String, Object> param = app.getTestParam().getTestParams();
            UserOBJ student1 = (UserOBJ) param.get("student1");
            UserOBJ student2 = (UserOBJ) param.get("student2");
            BookOBJ book1 = (BookOBJ) param.get("book1");
            ClassOBJ test_class = (ClassOBJ) param.get("test_class");
            WikiOBJ wiki1 = (WikiOBJ) param.get("wiki1");
            
            // join co-edit for wiki2
            inviteUser(app, driver, student2, wiki1, test_class, student1);
            
            // create book
            createBook(driver, student1, test_class, book1);
            logMessage(app, "create book successfully!");
            
            // add book entry
            editBook(driver, student1, test_class, book1);
            logMessage(app, "edit book successfully!");
            
            // view book
            viewBook(driver, student2, test_class, book1);
            logMessage(app, "view book successfully!");
            
            logout(driver);
            
        } catch (Exception e) {
            logMessage(app, e.toString());
            e.printStackTrace();
            setSTOP(true);
            takeScreenShot(driver);
            driver.quit();
        }
        
    }

    private void inviteUser(App app, WebDriver driver, UserOBJ author, ClassEntryOBJ entry, ClassOBJ test_class, UserOBJ coEditor) throws Exception {
        login(driver, author);
        selectClass(driver, test_class);
        selectEntry(driver, entry, app);
        clickElement(driver, driver.findElement(By.xpath("//div[@id='tab3Row']")));
        Thread.sleep(1000);
        driver.findElement(By.xpath("//a[@id='invite-person']")).click();
        Thread.sleep(1000);
        String checked = driver.findElement(By.xpath("//input[@id='" + coEditor.getUserID() + "-user']")).getAttribute("checked");
        if (!"true".equals(checked)) {
            clickElement(driver, driver.findElement(By.xpath("//input[@id='" + coEditor.getUserID() + "-user']")));
        }
        Thread.sleep(1000);
        String alert = MessageUtil.getMessage(app.getTestLocale(), "msg.edit.group.update");
        waitForAlert(driver, driver.findElement(By.xpath("//input[@id='invitePersonSubmit-class']")), alert);
        logout(driver);
    }
    
    private void viewBook(WebDriver driver, UserOBJ student1, ClassOBJ classObj, BookOBJ book1) throws Exception {
        login(driver, student1);
        selectClass(driver, classObj);
        Thread.sleep(1000);
        WebElement tag = driver.findElement(By.xpath("//a[@id='showMyBook']"));
        tag.click();
        Thread.sleep(1000);
        clickElement(driver, driver.findElement(By.xpath("//a[text() = '" + book1.getBookWord() + "']")));
        Thread.sleep(1000);
        clickElement(driver, driver.findElement(By.xpath("//img[@title='view book']")));
        Thread.sleep(5000);
        List<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        // go to book tab
        driver.switchTo().window(tabs.get(1));
        waitForElement(driver, "//div[@page='1']", 30);
        driver.close();
        // go to origin tab
        driver.switchTo().window(tabs.get(0));
        logout(driver);
    }

    private void editBook(WebDriver driver, UserOBJ student1, ClassOBJ classObj, BookOBJ book1) throws Exception {
        login(driver, student1);
        selectClass(driver, classObj);
        Thread.sleep(1000);
        WebElement tag = driver.findElement(By.xpath("//a[@id='showMyBook']"));
        tag.click();
        Thread.sleep(1000);
        clickElement(driver, driver.findElement(By.xpath("//a[text() = '" + book1.getBookWord() + "']")));
        Thread.sleep(1000);
        for (BookChapterOBJ chapter : book1.getBookChapters()) {
            if ("event".equals(chapter.getType())) {
                driver.findElement(By.xpath("//a[@id='addPicChapter']")).click();
                Thread.sleep(1000);
                if (driver.findElement(By.xpath("//div[@id='picTab1Wait']")).isDisplayed()) {
                    driver.findElement(By.xpath("//div[@id='picTab1Wait']")).click();
                    Thread.sleep(1000);
                }
                clickElement(driver, driver.findElement(By.xpath("//input[@id='" + chapter.getEntryID() + "-event']")));
                Thread.sleep(1000);
                clickElement(driver, driver.findElement(By.xpath("//input[@id='getChapterEntrySubmit-event']")));
                Thread.sleep(1000);
                waitForText(driver, 60, chapter.getTitle());
            } else if ("wiki".equals(chapter.getType())) {
                driver.findElement(By.xpath("//a[@id='addWikiChapter']")).click();
                Thread.sleep(1000);
                clickElement(driver, driver.findElement(By.xpath("//input[@id='" + chapter.getEntryID() + "-wiki']")));
                Thread.sleep(1000);
                clickElement(driver, driver.findElement(By.xpath("//input[@id='getChapterEntrySubmit-wiki']")));
                Thread.sleep(1000);
                waitForText(driver, 60, chapter.getTitle());
            } else if ("concept".equals(chapter.getType())) {
                driver.findElement(By.xpath("//a[@id='addPicChapter']")).click();
                Thread.sleep(1000);
                if (driver.findElement(By.xpath("//div[@id='picTab2Wait']")).isDisplayed()) {
                    clickElement(driver, driver.findElement(By.xpath("//div[@id='picTab2Wait']")));
                    Thread.sleep(1000);
                }
                clickElement(driver, driver.findElement(By.xpath("//input[@id='" + chapter.getEntryID() + "-concept']")));
                Thread.sleep(1000);
                clickElement(driver, driver.findElement(By.xpath("//input[@id='getChapterEntrySubmit-concept']")));
                Thread.sleep(1000);
                waitForText(driver, 60, chapter.getTitle());
            }
        }
        logout(driver);
    }

    private void createBook(WebDriver driver, UserOBJ student1, ClassOBJ classObj, BookOBJ book1) throws Exception {
        login(driver, student1);
        selectClass(driver, classObj);
        Thread.sleep(1000);
        WebElement tag = driver.findElement(By.xpath("//a[@id='showMyBook']"));
        tag.click();
        Thread.sleep(1000);
        WebElement add = driver.findElement(By.xpath("//table[@id='myEntryList']//tbody//tr//td//h2//a"));
        add.click();
        Thread.sleep(1000);
        Alert alert = driver.switchTo().alert();
        alert.sendKeys(book1.getEntryTitle());
        alert.accept();
        Thread.sleep(1000);
        waitForText(driver, 60, book1.getBookWord());
        logout(driver);
    }

    @Override
    public void check(WebDriver driver, Properties prop, App app) {
        // TODO Auto-generated method stub
        
    }

}