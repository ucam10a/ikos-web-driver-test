package com.yung.web.test.ikos;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.yung.web.test.App;

public class InitDatabase extends IkosScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, App app) {
        try {
            
            goToPage(driver, HOME_URL + "/initDB", "//body");
            String text = driver.findElement(By.xpath("//body")).getText();
            if (!text.contains("Database is initialized successfully")) {
                throw new Exception("InitDatabase fail!");
            }
            
        } catch (Exception e) {
            logMessage(app, e.toString());
            e.printStackTrace();
            setSTOP(true);
            takeScreenShot(driver);
            driver.quit();
        }
    }

    @Override
    public void check(WebDriver driver, Properties prop, App app) {
        // TODO Auto-generated method stub
        
    }

}