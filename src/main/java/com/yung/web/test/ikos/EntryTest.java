package com.yung.web.test.ikos;

import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.yung.web.test.App;

import tkss.util.MessageUtil;
import tkss.util.orm.ClassEntryOBJ;
import tkss.util.orm.ClassOBJ;
import tkss.util.orm.ConceptOBJ;
import tkss.util.orm.EventOBJ;
import tkss.util.orm.UserOBJ;
import tkss.util.orm.WikiOBJ;

public class EntryTest extends IkosScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, App app) {
        
        try {
            
        	HashMap<String, Object> params = app.getTestParam().getTestParams();
            UserOBJ student1 = (UserOBJ) params.get("student1");
            EventOBJ event1 = (EventOBJ) params.get("event1");
            UserOBJ student2 = (UserOBJ) params.get("student2");
            WikiOBJ wiki1 = (WikiOBJ) params.get("wiki1");
            UserOBJ student3 = (UserOBJ) params.get("student3");
            ConceptOBJ concept1 = (ConceptOBJ) params.get("concept1");
            
            ClassOBJ test_class = (ClassOBJ) params.get("test_class");
            
            login(driver, student1);
            createEntry(app, driver, test_class, event1);
            logout(driver);
            insertEntryID(app, driver, student1, event1, test_class);
            logMessage(app, "create " + event1.getEntryClass() + ": " + event1.getEntryTitle() + " successfully!");
            login(driver, student1);
            inviteUser(app, driver, student1, event1, test_class, student2);
            logout(driver);
            logMessage(app, "invite user " + event1.getEntryClass() + ": " + event1.getEntryTitle() + " successfully!");
            
            login(driver, student2);
            createEntry(app, driver, test_class, wiki1);
            logout(driver);
            insertEntryID(app, driver, student2, wiki1, test_class);
            logMessage(app, "create " + wiki1.getEntryClass() + ": " + wiki1.getEntryTitle() + " successfully!");
            login(driver, student2);
            inviteUser(app, driver, student2, wiki1, test_class, student3);
            logout(driver);
            logMessage(app, "invite user " + wiki1.getEntryClass() + ": " + wiki1.getEntryTitle() + " successfully!");
            
            login(driver, student3);
            createEntry(app, driver, test_class, concept1);
            logout(driver);
            insertEntryID(app, driver, student3, concept1, test_class);
            logMessage(app, "create " + concept1.getEntryClass() + ": " + concept1.getEntryTitle() + " successfully!");
            login(driver, student3);
            inviteUser(app, driver, student3, concept1, test_class, student1);
            logout(driver);
            logMessage(app, "invite user " + concept1.getEntryClass() + ": " + concept1.getEntryTitle() + " successfully!");
            
        } catch (Exception e) {
            logMessage(app, e.toString());
            e.printStackTrace();
            setSTOP(true);
            takeScreenShot(driver);
            driver.quit();
        }
        
    }
    
    private void inviteUser(App app, WebDriver driver, UserOBJ author, ClassEntryOBJ entry, ClassOBJ test_class, UserOBJ coEditor) throws Exception {
        selectClass(driver, test_class);
        selectEntry(driver, entry, app);
        clickElement(driver, driver.findElement(By.xpath("//div[@id='tab3Row']")));
        Thread.sleep(1000);
        clickElement(driver, driver.findElement(By.xpath("//a[@id='invite-person']")));
        Thread.sleep(1000);
        clickElement(driver, driver.findElement(By.xpath("//input[@id='" + coEditor.getUserID() + "-user']")));
        Thread.sleep(1000);
        waitForAlert(driver, driver.findElement(By.xpath("//input[@id='invitePersonSubmit-class']")), MessageUtil.getMessage(app.getTestLocale(), "msg.edit.group.update"));
    }

    protected void createEntry(App app, WebDriver driver, ClassOBJ classObj, ClassEntryOBJ entry) throws InterruptedException {
        selectClass(driver, classObj);
        Thread.sleep(1000);
        WebElement tag = null;
        if (entry.getEntryClass().equalsIgnoreCase("event")) {
            tag = driver.findElement(By.xpath("//a[@id='showMyEvent']"));
        } else if (entry.getEntryClass().equalsIgnoreCase("wiki")) {
            tag = driver.findElement(By.xpath("//a[@id='showMyWiki']"));
        } else if (entry.getEntryClass().equalsIgnoreCase("concept")) {
            tag = driver.findElement(By.xpath("//a[@id='showMyConcept']"));
        }
        tag.click();
        Thread.sleep(1000);
        WebElement add = driver.findElement(By.xpath("//table[@id='myEntryList']//tbody//tr//td//h2//a"));
        add.click();
        Thread.sleep(1000);
        Alert alert = driver.switchTo().alert();
        alert.sendKeys(entry.getEntryTitle());
        alert.accept();
        Thread.sleep(1000);
        if (entry.getEntryClass().equalsIgnoreCase("event")) {
            try {
                alert = driver.switchTo().alert();
                alert.accept();
                driver.switchTo().defaultContent();
            } catch (Exception e) {
                //e.printStackTrace();
            }
            Thread.sleep(1000);
            try {
                Alert imgAlert = driver.switchTo().alert();
                imgAlert.accept();
            } catch (Exception e) {
                //e.printStackTrace();
            }
            driver.findElement(By.xpath("//a[@id='EventToolBar']")).click();
            Thread.sleep(1000);
            driver.findElement(By.xpath("//a[@id='insertBgimage']")).click();
            Thread.sleep(1000);
            driver.findElement(By.xpath("//div[@id='tab2Wait']")).click();
            Thread.sleep(1000);
            WebElement httpBg = driver.findElement(By.xpath("//input[@id='httpBg']"));
            httpBg.sendKeys(HOME_URL + "/orbit-images/headPic1.jpg");
            driver.findElement(By.xpath("//input[@id='httpBg-submit']")).click();
            Thread.sleep(1000);
            insertSquare(driver);
            waitForAlert(driver, driver.findElement(By.xpath("//a[@id='upperSave-eventEdit']")), MessageUtil.getMessage(app.getTestLocale(), "update.success"));
            driver.findElement(By.xpath("//a[@id='upperLeave-eventEdit']")).click();
        } else if (entry.getEntryClass().equalsIgnoreCase("wiki")) {
            editWiki(driver, entry.getEntryTitle());
            String[] keywords = entry.getKeyword().split(",");
            String keywordText = driver.findElement(By.xpath("//div[@id='keywords']")).getText();
            for (String keyword : keywords) {
                keyword = keyword.trim();
                if (!keywordText.contains(keyword)) {
                    driver.findElement(By.xpath("//input[@id='keywordNode']")).clear();
                    driver.findElement(By.xpath("//input[@id='keywordNode']")).sendKeys(keyword);
                    Thread.sleep(1000);
                    driver.findElement(By.xpath("//ui[@id='addKeyword-wikiEdit']")).click();
                    Thread.sleep(1000);
                }
            }
            waitForAlert(driver, driver.findElement(By.xpath("//a[@id='upperSave-wikiEdit']")), MessageUtil.getMessage(app.getTestLocale(), "update.success"));
            driver.findElement(By.xpath("//a[@id='upperLeave-wikiEdit']")).click();
        } else if (entry.getEntryClass().equalsIgnoreCase("concept")) {
            insertSquare(driver);
            waitForAlert(driver, driver.findElement(By.xpath("//a[@id='upperSave-conceptEdit']")), MessageUtil.getMessage(app.getTestLocale(), "update.success"));
            driver.findElement(By.xpath("//a[@id='upperLeave-conceptEdit']")).click();
        }
        Thread.sleep(3000);
    }

    @Override
    public void check(WebDriver driver, Properties prop, App app) {
        // TODO Auto-generated method stub
        
    }

}