package com.yung.web.test.ikos;

import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.yung.web.test.App;

import tkss.util.MessageUtil;
import tkss.util.orm.ClassOBJ;
import tkss.util.orm.ConceptOBJ;
import tkss.util.orm.EventOBJ;
import tkss.util.orm.UserOBJ;
import tkss.util.orm.WikiOBJ;

public class ProgressTest extends IkosScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, App app) {
        
        try {
            
        	HashMap<String, Object> params = app.getTestParam().getTestParams();
            UserOBJ student1 = (UserOBJ) params.get("student1");
            EventOBJ event1 = (EventOBJ) params.get("event1");
            UserOBJ student2 = (UserOBJ) params.get("student2");
            WikiOBJ wiki1 = (WikiOBJ) params.get("wiki1");
            UserOBJ student3 = (UserOBJ) params.get("student3");
            ConceptOBJ concept1 = (ConceptOBJ) params.get("concept1");
            
            UserOBJ teacher = (UserOBJ) params.get("teacher");
            
            ClassOBJ test_class = (ClassOBJ) params.get("test_class");
            
            String text = null;
            login(driver, teacher);
            selectClass(driver, test_class);
            clickElement(driver, driver.findElement(By.xpath("//a[@id='showProgress']")));
            Thread.sleep(3000);
            
            // student
            tabClick(driver, "//div[@id='tab1Wait']");
            String studentInfo = MessageUtil.getMessage(app.getTestLocale(), "student.information");
            waitForText(driver, 30, studentInfo);
            text = driver.findElement(By.xpath("//div[@id='tabContent1']")).getText();
            if (!text.contains(student1.getUserID())) {
                throw new Exception("student: " + student1.getUserID() + " not exist!");
            }
            if (!text.contains(student2.getUserID())) {
                throw new Exception("student: " + student2.getUserID() + " not exist!");
            }
            if (!text.contains(student3.getUserID())) {
                throw new Exception("student: " + student3.getUserID() + " not exist!");
            }
            
            // event
            tabClick(driver, "//div[@id='tab2Wait']");
            String picTagInfo = MessageUtil.getMessage(app.getTestLocale(), "pic.tag.information");
            waitForText(driver, 30, picTagInfo);
            text = driver.findElement(By.xpath("//div[@id='tabContent2']")).getText();
            if (!text.contains(event1.getEntryTitle())) {
                throw new Exception("event: " + event1.getEntryTitle() + " not exist!");
            }
            
            // wiki
            tabClick(driver, "//div[@id='tab3Wait']");
            String wikiInfo = MessageUtil.getMessage(app.getTestLocale(), "wiki.information");
            waitForText(driver, 30, wikiInfo);
            text = driver.findElement(By.xpath("//div[@id='tabContent3']")).getText();
            if (!text.contains(wiki1.getEntryTitle())) {
                throw new Exception("wiki: " + wiki1.getEntryTitle() + " not exist!");
            }
            
            // concept
            tabClick(driver, "//div[@id='tab4Wait']");
            String conceptInfo = MessageUtil.getMessage(app.getTestLocale(), "concept.map.information");
            waitForText(driver, 30, conceptInfo);
            text = driver.findElement(By.xpath("//div[@id='tabContent4']")).getText();
            if (!text.contains(concept1.getEntryTitle())) {
                throw new Exception("concept: " + concept1.getEntryTitle() + " not exist!");
            }
            logMessage(app, "ProgressTest test successfully!");
            
        } catch (Exception e) {
            logMessage(app, e.toString());
            e.printStackTrace();
            setSTOP(true);
            takeScreenShot(driver);
            driver.quit();
        }
        
    }

    @Override
    public void check(WebDriver driver, Properties prop, App app) {
        // TODO Auto-generated method stub
        
    }

}