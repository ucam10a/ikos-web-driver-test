package com.yung.web.test.ikos;

import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.yung.web.test.App;

import tkss.util.MessageUtil;
import tkss.util.orm.UserOBJ;

public class AccountTest extends IkosScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, App app) {
        
        try {
            
        	HashMap<String, Object> param = app.getTestParam().getTestParams();
            UserOBJ student1 = (UserOBJ) param.get("student1");
            UserOBJ student2 = (UserOBJ) param.get("student2");
            UserOBJ student3 = (UserOBJ) param.get("student3");
            UserOBJ teacher = (UserOBJ) param.get("teacher");
            
            createStudent(driver, student1, app);
            app.logMessage("create student1 successfully!");
            
            createStudent(driver, student2, app);
            app.logMessage("create student2 successfully!");
            
            createStudent(driver, student3, app);
            app.logMessage("create student3 successfully!");
            
            createTeacher(driver, teacher, app);
            app.logMessage("create teacher successfully!");
            
            logout(driver);
            
        } catch (Exception e) {
            app.logMessage(e.toString());
            e.printStackTrace();
            setSTOP(true);
            takeScreenShot(driver);
            driver.quit();
        }
        
    }
    
    private void createTeacher(WebDriver driver, UserOBJ student1, App app) throws InterruptedException {
        goToPage(driver, HOME_URL + "/createAccount.action?accountType=teacher", "//input[@id='username-create-" + student1.getAccountType() + "']");
        
        WebElement input = driver.findElement(By.xpath("//input[@id='username-create-" + student1.getAccountType() + "']"));
        input.sendKeys(student1.getUserID());
        Thread.sleep(500);
        
        input = driver.findElement(By.xpath("//input[@id='password-create-" + student1.getAccountType() + "']"));
        input.sendKeys(DEFAULT_PASS);
        Thread.sleep(500);
        
        input = driver.findElement(By.xpath("//input[@id='confirm-create-" + student1.getAccountType() + "']"));
        input.sendKeys(DEFAULT_PASS);
        Thread.sleep(500);
        
        input = driver.findElement(By.xpath("//input[@id='email-create-" + student1.getAccountType() + "']"));
        input.sendKeys(student1.getEmail());
        Thread.sleep(500);
        
        input = driver.findElement(By.xpath("//input[@id='firstName-create-" + student1.getAccountType() + "']"));
        input.sendKeys(student1.getFirstName());
        Thread.sleep(500);
        
        input = driver.findElement(By.xpath("//input[@id='lastName-create-" + student1.getAccountType() + "']"));
        input.sendKeys(student1.getLastName());
        Thread.sleep(500);
        
        driver.findElement(By.xpath("//input[@id='submit-create-" + student1.getAccountType() + "']")).click();
        Thread.sleep(1000);
        String msg = MessageUtil.getMessage(app.getTestLocale(), "success.msg");
        waitForText(driver, 60, msg);
    }

    public static void createStudent(WebDriver driver, UserOBJ student1, App app) throws InterruptedException {
        goToPage(driver, HOME_URL + "/createAccount.action?accountType=student", "//input[@id='username-create-" + student1.getAccountType() + "']");
        
        WebElement input = driver.findElement(By.xpath("//input[@id='username-create-" + student1.getAccountType() + "']"));
        input.sendKeys(student1.getUserID());
        Thread.sleep(500);
        
        input = driver.findElement(By.xpath("//input[@id='password-create-" + student1.getAccountType() + "']"));
        input.sendKeys(DEFAULT_PASS);
        Thread.sleep(500);
        
        input = driver.findElement(By.xpath("//input[@id='confirm-create-" + student1.getAccountType() + "']"));
        input.sendKeys(DEFAULT_PASS);
        Thread.sleep(500);
        
        input = driver.findElement(By.xpath("//input[@id='email-create-" + student1.getAccountType() + "']"));
        input.sendKeys(student1.getEmail());
        Thread.sleep(500);
        
        input = driver.findElement(By.xpath("//input[@id='firstName-create-" + student1.getAccountType() + "']"));
        input.sendKeys(student1.getFirstName());
        Thread.sleep(500);
        
        input = driver.findElement(By.xpath("//input[@id='lastName-create-" + student1.getAccountType() + "']"));
        input.sendKeys(student1.getLastName());
        Thread.sleep(500);
        
        driver.findElement(By.xpath("//input[@id='submit-create-" + student1.getAccountType() + "']")).click();
        Thread.sleep(1000);
        String msg = MessageUtil.getMessage(app.getTestLocale(), "success.msg");
        waitForText(driver, 60, msg);
    }

    public void check(WebDriver driver, Properties prop, App app) {
        // TODO Auto-generated method stub
        
    }

}