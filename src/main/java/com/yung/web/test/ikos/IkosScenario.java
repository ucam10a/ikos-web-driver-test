package com.yung.web.test.ikos;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import tkss.util.MessageUtil;
import tkss.util.TKSSConstant;
import tkss.util.orm.ClassEntryOBJ;
import tkss.util.orm.ClassOBJ;
import tkss.util.orm.UserOBJ;

import com.yung.web.test.App;
import com.yung.web.test.AutoTestApp;
import com.yung.web.test.CheckScenario;
import com.yung.web.test.Scenario;

public abstract class IkosScenario extends Scenario implements CheckScenario {
    
    public static String HOME_URL = "http://127.0.0.1:8080/TKSS";
    
    public static String DEFAULT_PASS = "123456";
    
    public static String SCREEN_SHOT = "E:/test/screen_shot.png";
    
    public abstract void execute(WebDriver driver, Properties prop, App app);
    
    public void run(WebDriver driver, Properties prop, App app) {
        try {
            execute(driver, prop, app);
            logout(driver);
        } catch (Exception e) {
            app.logMessage(e.toString());
            e.printStackTrace();
            setSTOP(true);
            driver.quit();
        }
    }
    
    public void login(WebDriver driver, UserOBJ user) throws Exception {
        goToPage(driver, HOME_URL, "//div[@id='details']");
        WebElement idInput = driver.findElement(By.xpath("//input[@id='userID-right']"));
        idInput.sendKeys(user.getUserID());
        Thread.sleep(1000);
        WebElement passInput = driver.findElement(By.xpath("//input[@id='password-right']"));
        passInput.sendKeys(DEFAULT_PASS);
        Thread.sleep(1000);
        WebElement submit = driver.findElement(By.xpath("//input[@id='submit-right']"));
        submit.click();
        Thread.sleep(3000);
        waitForText(driver, 30, user.getUserID());
        
    }
    
    public void logout(WebDriver driver) throws Exception {
        goToPage(driver, HOME_URL + "/logout.action", "//input[@id='userID-right']");
    }
    
    public static void waitForText(WebDriver driver, int seconds, final String... msgs) {
        if (isSTOP()) throw new RuntimeException("Cancel process");
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        ExpectedCondition<Boolean> condition = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                String text = d.findElement(By.xpath("//body")).getText();
                for (String msg : msgs) {
                    if (!text.contains(msg)) {
                        //System.out.println("msg: " + msg + ", not contains text: " + text);
                        return false;
                    }
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                return true;
            }
        };
        wait.until(condition);
    }
    
    public static void waitForAlert(WebDriver driver, WebElement submit, final String... texts) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        ExpectedCondition<Boolean> condition = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                Alert alert = d.switchTo().alert();
                String msg = alert.getText();
                msg = msg.toLowerCase();
                for (String text : texts) {
                    if (!msg.contains(text.toLowerCase())) {
                        //System.out.println("msg: " + msg + ", not contains text: " + text);
                        d.switchTo().defaultContent();
                        return false;
                    }
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                alert.accept();
                d.switchTo().defaultContent();
                return true;
            }
        };
        clickElement(driver, submit);
        wait.until(condition);
    }
    
    public static void clickElement(WebDriver driver, WebElement element) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].click()", element);
    }
    
    public void selectClass(WebDriver driver, ClassOBJ classObj) throws InterruptedException {
        String currentClass = driver.findElement(By.xpath("//span[@id='currentClass-myPage']")).getText();
        if (currentClass == null || !currentClass.equals(classObj.getField() + "_" + classObj.getAlias())) {
            WebElement selectClass = driver.findElement(By.xpath("//input[@id='" + classObj.getClassID() + "-myPage']"));
            selectClass.click();
            waitForText(driver, 30, classObj.getField() + "_" + classObj.getAlias());
        }
        Thread.sleep(1000);
    }
    
    public void insertSquare(WebDriver driver) throws InterruptedException {
        driver.findElement(By.xpath("//a[@id='TagToolBar']")).click();
        driver.findElement(By.xpath("//div[@id='geTagToolBarDiv']//a[@class='geItem'][1]")).click();
        Thread.sleep(1000);
    }
    
    public void insertCircle(WebDriver driver) throws InterruptedException {
        driver.findElement(By.xpath("//a[@id='TagToolBar']")).click();
        driver.findElement(By.xpath("//div[@id='geTagToolBarDiv']//a[@class='geItem'][3]")).click();
        Thread.sleep(1000);
    }
    
    public void editWiki(WebDriver driver, String text) throws InterruptedException {
        driver.switchTo().defaultContent();
        Thread.sleep(2000);
        WebElement iFrame = driver.findElement(By.xpath("//iframe[1]"));
        driver.switchTo().frame(iFrame);
        WebElement body = driver.findElement(By.xpath("//html//body"));
        clickElement(driver, body);
        Thread.sleep(1000);
        body.sendKeys(text);
        Thread.sleep(1000);
        driver.switchTo().defaultContent();
    }
    
    public void selectEntry(WebDriver driver, ClassEntryOBJ entry, App app) throws Exception {
        selectMainEntryPage(driver, entry, app);
        driver.findElement(By.xpath("//a[@id='entrylist-" + entry.getEntryID() + "']")).click();
        if ("event".equalsIgnoreCase(entry.getEntryClass())) {
            waitForText(driver, 30, MessageUtil.getMessage(app.getTestLocale(), "pic.tag.title"), entry.getEntryTitle());
        } else if ("wiki".equalsIgnoreCase(entry.getEntryClass())) {
            waitForText(driver, 30, MessageUtil.getMessage(app.getTestLocale(), "wiki.title"), entry.getEntryTitle());
        } else if ("concept".equalsIgnoreCase(entry.getEntryClass())) {
            waitForText(driver, 30, MessageUtil.getMessage(app.getTestLocale(), "concept.map.title"), entry.getEntryTitle());
        } else {
            throw new Exception("Unknow type: " + entry.getEntryClass());
        }
        Thread.sleep(1000);
    }
    
    public void selectMainEntryPage(WebDriver driver, ClassEntryOBJ entry, App app) throws Exception {
        if ("event".equalsIgnoreCase(entry.getEntryClass())) {
            driver.findElement(By.xpath("//a[@id='showMyEvent']")).click();
            waitForText(driver, 30, MessageUtil.getMessage(app.getTestLocale(), "my.pic.tag"), MessageUtil.getMessage(app.getTestLocale(), "pic.tag.title"));
        } else if ("wiki".equalsIgnoreCase(entry.getEntryClass())) {
            driver.findElement(By.xpath("//a[@id='showMyWiki']")).click();
            waitForText(driver, 30, MessageUtil.getMessage(app.getTestLocale(), "my.wiki"), MessageUtil.getMessage(app.getTestLocale(), "wiki.title"));
        } else if ("concept".equalsIgnoreCase(entry.getEntryClass())) {
            driver.findElement(By.xpath("//a[@id='showMyConcept']")).click();
            waitForText(driver, 30, MessageUtil.getMessage(app.getTestLocale(), "my.concept.map"), MessageUtil.getMessage(app.getTestLocale(), "concept.map.title"));
        } else {
            throw new Exception("Unknow type: " + entry.getEntryClass());
        }
    }
    
    public void selectAdminMainPage(WebDriver driver, ClassEntryOBJ entry) throws Exception {
        if ("event".equalsIgnoreCase(entry.getEntryClass())) {
            driver.findElement(By.xpath("//a[@id='showMyEvent']")).click();
            waitForText(driver, 30, "Applying event Entry");
        } else if ("wiki".equalsIgnoreCase(entry.getEntryClass())) {
            driver.findElement(By.xpath("//a[@id='showMyWiki']")).click();
            waitForText(driver, 30, "Applying wiki Entry");
        } else if ("concept".equalsIgnoreCase(entry.getEntryClass())) {
            driver.findElement(By.xpath("//a[@id='showMyConcept']")).click();
            waitForText(driver, 30, "Applying concept Entry");
        } else {
            throw new Exception("Unknow type: " + entry.getEntryClass());
        }
    }
    
    public String getElementHtml(WebDriver driver, String xpath) {
        WebElement ele = driver.findElement(By.xpath(xpath));
        String html = ele.getAttribute("innerHTML");
        return html;
    }
    
    public void tabClick(WebDriver driver, String xpath) {
        if (driver.findElement(By.xpath(xpath)).isDisplayed()) {
            driver.findElement(By.xpath(xpath)).click();
        }
    }
    
    public void insertEntryID(App app, WebDriver driver, UserOBJ author, ClassEntryOBJ entry, ClassOBJ test_class) throws Exception {
        login(driver, author);
        selectClass(driver, test_class);
        selectMainEntryPage(driver, entry, app);
        List<WebElement> elements = driver.findElements(By.xpath("//table[@id='myEntryList']//td[@class='myEntry']//a"));
        for (WebElement element : elements) {
            String title = element.getText().trim();
            if (title.equals(entry.getEntryTitle())) {
                String id = element.getAttribute("id");
                if (id.startsWith("entrylist-")) {
                    String entryIDStr = id.substring("entrylist-".length());
                    int entryID = Integer.valueOf(entryIDStr);
                    entry.setEntryID(entryID);
                    app.getTestParam().getTestParams().put(entry.getEntryTitle(), entry);
                    System.out.println("set entry: " + entry.getEntryTitle() + " id: " + entryID);
                    break;
                }
            }
        }
        logout(driver);
    }
    
    public String getEntryTypeName(String type, App app) {
        String entryTypeName = "";
        if (type.equalsIgnoreCase(TKSSConstant.EVENT_TYPE)) {
            entryTypeName = MessageUtil.getMessage(app.getTestLocale(), "pic.tag");
        } else if (type.equalsIgnoreCase(TKSSConstant.WIKI_TYPE)) {
            entryTypeName = MessageUtil.getMessage(app.getTestLocale(), "wiki");
        } else if (type.equalsIgnoreCase(TKSSConstant.CONCEPT_MAP_TYPE)) {
            entryTypeName = MessageUtil.getMessage(app.getTestLocale(), "concept.map");
        } else if (type.equalsIgnoreCase(TKSSConstant.BOOK_TYPE)) {
            entryTypeName = MessageUtil.getMessage(app.getTestLocale(), "book");
        }
        return entryTypeName;
    }
    
    public void takeScreenShot(WebDriver driver) {
        if (!AutoTestApp.DRIVER_NAME.equals("phantomjs.exe")) {
            return;
        }
        System.out.println("Taking screenshot now");
        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        System.out.println("File:" + srcFile);
        try {
            FileUtils.copyFile(srcFile, new File(SCREEN_SHOT));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
    
}