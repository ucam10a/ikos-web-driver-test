package com.yung.web.test.ikos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.yung.web.test.App;

import tkss.util.MessageUtil;
import tkss.util.orm.ConceptOBJ;
import tkss.util.orm.EventOBJ;
import tkss.util.orm.UserOBJ;

public class DisplayMessageTest extends IkosScenario {

    @Override
    public void execute(WebDriver driver, Properties prop, App app) {
        
        try {
            
        	HashMap<String, Object> params = app.getTestParam().getTestParams();
            EventOBJ event1 = (EventOBJ) params.get("event1");
            ConceptOBJ concept1 = (ConceptOBJ) params.get("concept1");
            
            UserOBJ student1 = (UserOBJ) params.get("student1");
            UserOBJ student2 = (UserOBJ) params.get("student2");
            UserOBJ student3 = (UserOBJ) params.get("student3");
            UserOBJ teacher = (UserOBJ) params.get("teacher");
            
            String teacherName = getUserName(teacher);
            String student1Name = getUserName(student1);
            String student2Name = getUserName(student2);
            String student3Name = getUserName(student3);
            String of = MessageUtil.getMessage(app.getTestLocale(), "of");
            
            List<String> displayMessages = new ArrayList<String>();
            displayMessages.add(teacherName + " " + MessageUtil.getMessage(app.getTestLocale(), "change.status.on") + " " + student1Name + of + " " + event1.getEventWord());
            displayMessages.add(teacherName + " " + MessageUtil.getMessage(app.getTestLocale(), "approved.open.to.class.on") + " " + student1Name + of + " " + event1.getEventWord());
            displayMessages.add(teacherName + " " + MessageUtil.getMessage(app.getTestLocale(), "change.status.on") + " " + student3Name + of + " " + concept1.getConceptWord());
            displayMessages.add(teacherName + " " + MessageUtil.getMessage(app.getTestLocale(), "approved.open.to.class.on") + " " + student3Name + of + " " + concept1.getConceptWord());
            displayMessages.add(student3Name + " " + MessageUtil.getMessage(app.getTestLocale(), "comment.on") + " " + student1Name + of + " " + event1.getEventWord());
            displayMessages.add(student3Name + " " + MessageUtil.getMessage(app.getTestLocale(), "rated.on") + " " + student1Name + of + " " + event1.getEventWord());
            displayMessages.add(student2Name + " " + MessageUtil.getMessage(app.getTestLocale(), "comment.on") + " " + student3Name + of + " " + concept1.getConceptWord());
            displayMessages.add(student2Name + " " + MessageUtil.getMessage(app.getTestLocale(), "rated.on") + " " + student3Name + of + " " + concept1.getConceptWord());
            //displayMessages.add(student3Name + " " + MessageUtil.getMessage(app.getTestLocale(), "approved.update.on") + " " + concept1.getConceptWord());
            
            login(driver, student1);
            Thread.sleep(5000);
            String text = driver.findElement(By.xpath("//div[@id='messageWindow']")).getText();
            for (String msg : displayMessages) {
                if (!text.contains(msg)) {
                    throw new Exception("message: " + msg + " not found!");
                }
            }
            
            logMessage(app, "DisplayMessageTest test successfully!");
            
        } catch (Exception e) {
            logMessage(app, e.toString());
            e.printStackTrace();
            setSTOP(true);
            takeScreenShot(driver);
            driver.quit();
        }
        
    }

    private String getUserName(UserOBJ user) {
        return user.getFirstName() + " " + user.getLastName();
    }

    @Override
    public void check(WebDriver driver, Properties prop, App app) {
        // TODO Auto-generated method stub
        
    }

}