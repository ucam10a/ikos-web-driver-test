package com.yung.web.test.complex;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.yung.web.test.App;

public class InitDatabase extends MyScenario {

    public void run(WebDriver driver, Properties prop, App app) {
        try {
            
            goToPage(driver, HOME_URL + "/initDB", "//body");
            String text = driver.findElement(By.xpath("//body")).getText();
            if (!text.contains("Database is initialized successfully")) {
                throw new Exception("InitDatabase fail!");
            }
            
        } catch (Exception e) {
            app.logMessage(ERROR_PREFIX + e.toString());
            setSTOP(true);
            throw new RuntimeException(e);
        }
    }
    
    public void check(WebDriver driver, Properties prop, App log) {
        // TODO Auto-generated method stub
        
    }

}