package com.yung.web.test.complex;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.yung.web.test.App;
import com.yung.web.test.AppLogger;
import com.yung.web.test.CheckScenario;
import com.yung.web.test.ComplexScenario;
import com.yung.web.test.EnrichScenario;

public class OutllokSendMap extends ComplexScenario implements CheckScenario {
    
    private static final AppLogger logger = AppLogger.getLogger(OutllokSendMap.class);
    
    MSLogin msLogin = new MSLogin();
    OutllokSend outlookSend = new OutllokSend();
    
    @Override
    public void check(WebDriver driver, Properties prop, App app) {
        List<String> errorMsgs = new ArrayList<String>();
        for (String logMsg : app.getLogList()) {
            if (logMsg.contains(ERROR_PREFIX)) {
                errorMsgs.add(logMsg);
            }
        }
        StringBuilder sb = new StringBuilder();
        String result = "Success";
        if (errorMsgs.size() > 0) {
            result = "Fail";
        }
        sb.append("All Test finished! " + result + NEW_LINE);
        if (errorMsgs.size() > 0) {
            sb.append("Error Messages: " + NEW_LINE);
            for (String errorMsg : errorMsgs) {
                sb.append(errorMsg + NEW_LINE);
            }
        }
        app.logMessage(sb.toString());
    }

    public void run(WebDriver driver, Properties prop, App app) {
        try {
            
            goToPage(driver, "https://outlook.live.com/mail/0/inbox", "//body");
            
            if (!EnrichScenario.checkElementExist(driver, "//button[@type='button']//span[contains(text(), 'New message')]", 10)) {
                msLogin.execute(driver, prop, app);
            }
            outlookSend.execute(driver, prop, app);
            
        } catch (Exception e) {
            logger.error(e.toString(), e);
            File outFile = new File("fail.jpg");
            logger.info(outFile.getAbsolutePath());
            MyScenario.takeScreenShot(driver, outFile);
            app.logMessage(ERROR_PREFIX + e.toString());
            setSTOP(true);
            driver.quit();
        }
    }

}