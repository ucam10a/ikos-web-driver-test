package com.yung.web.test.complex;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.yung.web.test.AppLogger;
import com.yung.web.test.EnrichScenario;

public abstract class MyScenario extends EnrichScenario {
    
    private static final AppLogger logger = AppLogger.getLogger(MyScenario.class);

    public static String HOME_URL = "http://127.0.0.1:8080/TKSS";
    
    public static String DEFAULT_PASS = "123456";

    public static void takeScreenShot(WebDriver driver) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH-mm-ss");
        File outFile = new File(sdf.format(new Date()) + ".jpg");
        logger.info("snapshot to: " + outFile.getAbsolutePath());
        takeScreenShot(driver, outFile);
    }
    
    public static void takeScreenShot(WebDriver driver, File outFile) {
        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(srcFile, outFile);
        } catch (IOException e) {
            logger.error(e.toString(), e);
        }
    }
    
}
