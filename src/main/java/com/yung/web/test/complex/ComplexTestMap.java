package com.yung.web.test.complex;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.yung.web.test.App;
import com.yung.web.test.AppLogger;
import com.yung.web.test.CheckScenario;
import com.yung.web.test.ComplexScenario;

import tkss.util.orm.UserOBJ;

public class ComplexTestMap extends ComplexScenario implements CheckScenario {
    
    private static final AppLogger logger = AppLogger.getLogger(ComplexTestMap.class);
    
    CheckServer checkServer = new CheckServer();
    
    InitDatabase initDB = new InitDatabase();
    
    CreateAccount acctTest = new CreateAccount();
    
    Logout logout = new Logout();
    
    public ComplexTestMap() {
        MyScenario.HOME_URL = "http://192.168.2.105:8080/TKSS";
    }
    
    private List<UserOBJ> getStudents(App app) {
        List<UserOBJ> students = new ArrayList<UserOBJ>();
        HashMap<String, Object> params = app.getTestParam().getTestParams();
        UserOBJ student1 = (UserOBJ) params.get("student1");
        UserOBJ student2 = (UserOBJ) params.get("student2");
        UserOBJ student3 = (UserOBJ) params.get("student3");
        students.add(student1);
        students.add(student2);
        students.add(student3);
        
        for (int i = 0; i < 1; i++) {
            UserOBJ studentError = new UserOBJ();
            studentError.setUserID("err" + i);
            studentError.setAccountType("student");
            studentError.setFirstName("err" + i);
            studentError.setLastName("err" + i);
            studentError.setEmail("err" + i);
            students.add(studentError);
        }
        
        return students;
    }
    
    @Override
    public void check(WebDriver driver, Properties prop, App app) {
        List<String> errorMsgs = new ArrayList<String>();
        for (String logMsg : app.getLogList()) {
            if (logMsg.contains(ERROR_PREFIX)) {
                errorMsgs.add(logMsg);
            }
        }
        StringBuilder sb = new StringBuilder();
        String result = "Success";
        if (errorMsgs.size() > 0) {
            result = "Fail";
        }
        sb.append("All Test finished! " + result + NEW_LINE);
        if (errorMsgs.size() > 0) {
            sb.append("Error Messages: " + NEW_LINE);
            for (String errorMsg : errorMsgs) {
                sb.append(errorMsg + NEW_LINE);
            }
        }
        app.logMessage(sb.toString());
    }

    public void run(WebDriver driver, Properties prop, App app) {
        try {
            
            prop.put(TEST_SOURCE_OBJECT_KEY, this);
            checkServer.execute(driver, prop, app);
            initDB.execute(driver, prop, app);
            
            for (UserOBJ student : getStudents(app)) {
                app.getTestParam().setTestParameter(student);
                acctTest.execute(driver, prop, app);
                logout.execute(driver, prop, app);
            }
            
        } catch (Exception e) {
            logger.error(e.toString(), e);
            File outFile = new File("out.jpg");
            logger.info(outFile.getAbsolutePath());
            MyScenario.takeScreenShot(driver, outFile);
            app.logMessage(ERROR_PREFIX + e.toString());
            setSTOP(true);
            driver.quit();
        }
    }

}