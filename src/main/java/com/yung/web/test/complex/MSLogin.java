package com.yung.web.test.complex;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.yung.web.test.App;

public class MSLogin extends MyScenario {

    public void run(WebDriver driver, Properties prop, App app) {
        
        app.logMessage("start to login");
        
        sleep(1);
        WebElement signIn = waitAndGetElement(driver, "//a[@data-task='signin']", 30);
        signIn.click();
        sleep(3);
        
        WebElement emailInput = waitAndGetElement(driver, "//input[@type='email' and @name='loginfmt']", 30);
        emailInput.sendKeys("ucam10a@hotmail.com");
        sleep(2);
        
        app.logMessage("login ucam10a@hotmail.com");
        
        WebElement submit = waitAndGetElement(driver, "//input[@type='submit']", 30);
        submit.click();
        sleep(2);
      
        WebElement pass = waitAndGetElement(driver, "//input[@type='password']", 30);
        pass.sendKeys("Mts600t$");
        sleep(2);
        
        WebElement login = waitAndGetElement(driver, "//input[@type='submit']", 30);
        login.click();
        
        app.logMessage("login process done");
        
    }
    
}