package com.yung.web.test.complex;

import java.util.Locale;

import com.yung.web.test.SwingAutoTestApp;
import com.yung.web.test.ikos.TestValue;

public class SwingComplexTest {

    public static void main(String args[]) throws Exception {
        
        String localeParam = "en";
        if (args != null) {
            if (args.length > 0) {
                localeParam = args[0];
            }
        }
        
        SwingAutoTestApp app = new SwingAutoTestApp(
                //new ComplexTestMap()
                new OutllokSendMap()
        );
        app.setTestLocale(new Locale(localeParam));
        TestValue testParam = new TestValue(new Locale(localeParam));
        testParam.setupTestValue();
        app.setTestParam(testParam);
        app.setTestLocale(new Locale(localeParam));
        app.showUI();
        
    }
    
}