package com.yung.web.test.complex;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.yung.web.test.App;
import com.yung.web.test.AppLogger;

import tkss.util.MessageUtil;
import tkss.util.orm.UserOBJ;

public class CreateAccount extends MyScenario {

    private static final AppLogger logger = AppLogger.getLogger(CreateAccount.class);
    
    public static void createStudent(WebDriver driver, UserOBJ student1, App app) throws InterruptedException {
        goToPage(driver, HOME_URL + "/createAccount.action?accountType=student", "//input[@id='username-create-" + student1.getAccountType() + "']");
        
        WebElement input = driver.findElement(By.xpath("//input[@id='username-create-" + student1.getAccountType() + "']"));
        input.sendKeys(student1.getUserID());
        Thread.sleep(500);
        
        input = driver.findElement(By.xpath("//input[@id='password-create-" + student1.getAccountType() + "']"));
        input.sendKeys(DEFAULT_PASS);
        Thread.sleep(500);
        
        input = driver.findElement(By.xpath("//input[@id='confirm-create-" + student1.getAccountType() + "']"));
        input.sendKeys(DEFAULT_PASS);
        Thread.sleep(500);
        
        input = driver.findElement(By.xpath("//input[@id='email-create-" + student1.getAccountType() + "']"));
        input.sendKeys(student1.getEmail());
        Thread.sleep(500);
        
        input = driver.findElement(By.xpath("//input[@id='firstName-create-" + student1.getAccountType() + "']"));
        input.sendKeys(student1.getFirstName());
        Thread.sleep(500);
        
        input = driver.findElement(By.xpath("//input[@id='lastName-create-" + student1.getAccountType() + "']"));
        input.sendKeys(student1.getLastName());
        Thread.sleep(500);
        
        driver.findElement(By.xpath("//input[@id='submit-create-" + student1.getAccountType() + "']")).click();
        Thread.sleep(1000);
        String msg = MessageUtil.getMessage(app.getTestLocale(), "success.msg");
        waitForText(driver, 10, msg);
    }

    public void login(WebDriver driver, UserOBJ user) throws Exception {
        goToPage(driver, HOME_URL, "//div[@id='details']");
        WebElement idInput = driver.findElement(By.xpath("//input[@id='userID-right']"));
        idInput.sendKeys(user.getUserID());
        Thread.sleep(1000);
        WebElement passInput = driver.findElement(By.xpath("//input[@id='password-right']"));
        passInput.sendKeys(DEFAULT_PASS);
        Thread.sleep(1000);
        WebElement submit = driver.findElement(By.xpath("//input[@id='submit-right']"));
        submit.click();
        Thread.sleep(3000);
        waitForText(driver, 30, user.getUserID());
        
    }
    
    public void check(WebDriver driver, Properties prop, App app) {
        
        try {
            
            UserOBJ student = (UserOBJ) app.getTestParam().getTestParameter();
            login(driver, student);
            app.logMessage("check student: " + student.getUserID() + " login successfully!");
                
        } catch (Exception e) {
            app.logMessage(ERROR_PREFIX + e.toString());
            logger.error(e.toString(), e);
        }
        
    }

    public void run(WebDriver driver, Properties prop, App app) {
        try {
            
            UserOBJ student = (UserOBJ) app.getTestParam().getTestParameter();
            createStudent(driver, student, app);
            app.logMessage("create student: " + student.getUserID() + " successfully!");
            
        } catch (Exception e) {
            app.logMessage(ERROR_PREFIX + e.toString());
            logger.error(e.toString(), e);
            takeScreenShot(driver);
        }
    }
    
}