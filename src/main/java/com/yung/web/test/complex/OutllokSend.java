package com.yung.web.test.complex;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.yung.web.test.App;

public class OutllokSend extends MyScenario {

    public void run(WebDriver driver, Properties prop, App app) {
        
        WebElement editMail = waitAndGetElement(driver, "//button[@type='button']//span[contains(text(), 'New message')]", 30);
        editMail.click();
        sleep(3);
        
        WebElement to = waitAndGetElement(driver, "//div[@role='presentation']//input[@role='combobox']", 30);
        to.sendKeys("ucam10a@gmail.com");
        sleep(3);
    
        WebElement subject = waitAndGetElement(driver, "//input[@aria-label='Add a subject']", 30);
        subject.sendKeys("second test");
        sleep(3);
        
        WebElement body = waitAndGetElement(driver, "//div[@dir='ltr']//div", 30);
        body.sendKeys("second test");
        sleep(3);
        
        WebElement send = waitAndGetElement(driver, "//button[@aria-label='Send']", 30);
        send.click();
        sleep(10);
        
        app.logMessage("send email to ucam10a@gmail.com");
        
    }
    
}