package com.yung.web.test.complex;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.yung.web.test.App;

public class Logout  extends MyScenario {

    public void logout(WebDriver driver) throws Exception {
        goToPage(driver, HOME_URL + "/logout.action", "//input[@id='userID-right']");
    }
    
    public void run(WebDriver driver, Properties prop, App app) {
        try {
            logout(driver);
        } catch (Exception e) {
            app.logMessage(e.toString());
            e.printStackTrace();
            setSTOP(true);
            driver.quit();
        }
    }

    public void check(WebDriver driver, Properties prop, App app) {
        // TODO Auto-generated method stub
        
    }
    
}
