package test;

import java.util.HashMap;
import java.util.Map;

import com.yung.web.test.EnrichScenario;

public class Test {

    public static void main(String[] args) {
        Map<String, String> equalMap = new HashMap<String, String>();
        equalMap.put("id", "abc");
        equalMap.put("data-id", "123");
        Map<String, String> containMap = new HashMap<String, String>();
        containMap.put("text()", "xyz");
        EnrichScenario.waitAndGetElement(null, "div", equalMap, containMap, 10);
        
    }

}
